import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';

import { SocialLoginModule, AuthServiceConfig } from "angularx-social-login";
import { GoogleLoginProvider, FacebookLoginProvider } from "angularx-social-login";
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './register/register.component';
 
import { ReactiveFormsModule, FormsModule  } from '@angular/forms';
import { HeaderComponent } from './header/header.component';
import {MenubarModule} from 'primeng/menubar';
import {MenuItem} from 'primeng/api';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HttpClientModule } from '@angular/common/http';
import {CalendarModule} from 'primeng/calendar';
import { AutoformComponent } from './autoform/autoform.component';


let loginConfig = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider("187868511743-hpgfqb13bmg6ikdkbe8slc3k3p7ulumj.apps.googleusercontent.com")
  },
  {
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider("271302570583190")
  }
]);

export function provideLoginConfig() {
  return loginConfig;
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    RegisterComponent,
    HeaderComponent,
    AutoformComponent
  ],
  imports: [
    BrowserAnimationsModule,
    
    CalendarModule,
    BrowserModule,
    AppRoutingModule,
    SocialLoginModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    MenubarModule
  ],
  providers: [
    {
      provide: AuthServiceConfig,
      useFactory: provideLoginConfig,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
