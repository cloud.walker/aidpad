import { Component, OnInit, NgModule } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@NgModule({
  imports: [
    // import HttpClientModule after BrowserModule.
    HttpClient,
  ],
  declarations: [
    RegisterComponent,
  ],
  bootstrap: [ RegisterComponent ]
})

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})

export class RegisterComponent implements OnInit {
  
  selectedRole = ''
  nume = ''
  mail = '';
  
  constructor(private http: HttpClient) { 

  }

  roles = ['Organization', 'Persoana Fizica', 'Grupuri de initiativa'];
  scopuri = ['Administrator', 'Finantator', 'Evaluator'];
  asd = "dsad";
  altscop = false;

  ngOnInit(): void {
  }

  
  headers = { 'Authorization': 'Bearer my-token', 'My-Custom-Header': 'foobar' }

  log(){
    
  }

  register(){
    let body={
      nume: this.nume,
      role: this.selectedRole,
      mail: this.mail
    }
    console.log(body)
    this.http.post<any>("http://localhost:8080/aidpad/api/register", body).subscribe(response => {
      console.log(response);
    })

  }
}
