package com.aidpad.componenttests;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.aidpad.AidpadApplication;

@SpringBootApplication
public class AidpadComponentTests {

	public static void main(String[] args) {
		SpringApplication.run(AidpadApplication.class, args);
	}
}
