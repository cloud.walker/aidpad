package com.aidpad.componenttests;

import static org.hamcrest.CoreMatchers.endsWith;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.aidpad.AidpadApplication;
import com.aidpad.api.model.PersonDetailsTO;
import com.aidpad.api.model.UserTO;
import com.aidpad.api.model.UserTOLinks;
import com.aidpad.api.model.UserValueTO;
import com.aidpad.persistence.entities.PersonDetailsEntity;
import com.aidpad.persistence.entities.RoleEntity;
import com.aidpad.persistence.entities.UserEntity;
import com.aidpad.persistence.entities.UserRoleEntity;
import com.aidpad.persistence.repositories.GroupMembersRepository;
import com.aidpad.persistence.repositories.OrganizationDetailsRepository;
import com.aidpad.persistence.repositories.PersonDetailsRepository;
import com.aidpad.persistence.repositories.RolesRepository;
import com.aidpad.persistence.repositories.UserRolesRepository;
import com.aidpad.persistence.repositories.UsersRepository;
import com.aidpad.users.AuthenticationType;
import com.aidpad.users.UserType;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest(classes = AidpadApplication.class)
@EnableAutoConfiguration(exclude = DataSourceAutoConfiguration.class)
public class AidpadUsersTest {

	private static final String TEST_USER_1 = "users.component.tests@example.com";

	@Autowired
	private ObjectMapper mapper;
	
	@Autowired 
	private WebApplicationContext context;
	
	private static MockMvc mockMvc;

	@MockBean
	private UsersRepository usersRepository;

	@MockBean
	private RolesRepository rolesRepository;
	
	@MockBean
	private UserRolesRepository userRolesRepository;
	
	@MockBean
	private GroupMembersRepository groupMembersRepository; 

	@MockBean
	private PersonDetailsRepository personDetailsRepository;
	
	@MockBean
	private OrganizationDetailsRepository organizationDetailsRepository;

	@BeforeEach
	public void makeMockMvc() {
		mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
	}
	
	@Test
	void contextLoads() {
	}
	
	@Test
	public void getUserByUuidReturnsUser() throws Exception {
		String uuid = UUID.randomUUID().toString();
		UserEntity userEntity = new UserEntity(uuid, AuthenticationType.GOOGLE.name(), UserType.PERSON.name(), TEST_USER_1, false, true);
		when((usersRepository.findByUuid(uuid))).thenReturn(userEntity);
		
		String userUri = "/aidpad/api/user/" + uuid;
		
		mockMvc.perform(get(userUri))
			.andDo(print()) // this is how you debug these tests
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.user.name", equalTo(TEST_USER_1)))
			.andExpect(jsonPath("$.links.self", equalTo(userUri)));
	}
	
	@Test
	public void postUserCreatesUser() throws Exception {
		String uuid = UUID.randomUUID().toString();
		UserEntity userEntity = new UserEntity(uuid, AuthenticationType.GOOGLE.name(), UserType.PERSON.name(), TEST_USER_1, false, true);
		UserEntity[] savedUserEntity = new UserEntity[1]; 
		
		String[] roleUuids = new String[] {
				UUID.randomUUID().toString(),
				UUID.randomUUID().toString()
		};
		List<UserRoleEntity> userRoles = new ArrayList<>();
		for (String roleUuid : roleUuids) {
			RoleEntity roleEntity = new RoleEntity(roleUuid, "test-role-" + roleUuid, null);
			when(rolesRepository.findByUuid(roleUuid)).thenReturn(roleEntity);
			userRoles.add(new UserRoleEntity(roleEntity, userEntity));
		}
		
		UserTO userTO = new UserTO();
		UserValueTO userValueTO = new UserValueTO();
		userValueTO.setAuthType(AuthenticationType.GOOGLE.toString());
		userValueTO.setEnabled(false);
		userValueTO.setLoginName(TEST_USER_1);
		userValueTO.setMustChangePassword(true);
		userValueTO.setName(TEST_USER_1);
		userValueTO.setUserType(UserType.PERSON.name());
		userValueTO.setUuid(uuid);
		
		userValueTO.setPersonDetails(new PersonDetailsTO());
		userValueTO.getPersonDetails().setName("test-person-name");
		userValueTO.getPersonDetails().setEmail("test-person-email@example.com");

		when((usersRepository.findByUuid(uuid))).thenAnswer(new Answer<UserEntity>() {
			@Override
			public UserEntity answer(InvocationOnMock invocation) throws Throwable {
				if (uuid.equals(invocation.getArgument(0))) {
					return savedUserEntity[0]; 
				}
				return null;
			}
		});
		when((usersRepository.saveAndFlush(any()))).thenAnswer(new Answer<UserEntity>() {
			@Override
			public UserEntity answer(InvocationOnMock invocation) throws Throwable {
				savedUserEntity[0] = invocation.getArgument(0);
				savedUserEntity[0].setRoles(new HashSet<>());
				savedUserEntity[0].getRoles().addAll(userRoles);
				return savedUserEntity[0];
			}
		});
		
		when(personDetailsRepository.save(any(PersonDetailsEntity.class))).thenAnswer(new Answer<PersonDetailsEntity>() {
			@Override
			public PersonDetailsEntity answer(InvocationOnMock invocation) throws Throwable {
				return invocation.getArgument(0);
			}
		});
		
		UserTOLinks userLinks = new UserTOLinks();
		ArrayList<String> rolesLinks = new ArrayList<>();
		userLinks.setRoles(rolesLinks);
		for (String roleUuid : roleUuids) {
			rolesLinks.add("/aidpad/api/role/" + roleUuid);
		}
		userTO.setLinks(userLinks);
		
		userTO.setUser(userValueTO);
		
		String userUri = "/aidpad/api/user/" + uuid;
		mockMvc.perform(post("/aidpad/api/user/")
					.accept(MediaType.APPLICATION_JSON_VALUE)
					.contentType(MediaType.APPLICATION_JSON_VALUE)
					.content(mapper.writeValueAsString(userTO)))
			.andDo(print()) // this is how you debug these tests
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.user.name", equalTo(TEST_USER_1)))
			.andExpect(jsonPath("$.links.self", equalTo(userUri)))
			.andExpect(jsonPath("$.links.roles", hasItems(
					endsWith(roleUuids[0]), 
					endsWith(roleUuids[1]))));
	}

	/*
	 * post user creates new user
	 * delete user deletes user
	 * put user updates user
	 * get list with no parameters returns list with no prev link, default count and next
	 * get list with start and count returns list with prev and next and same next as request
	 * get list with start and count beyond end leaves out next 
	 * get list with all parameters passes values to query
	 * */
	
}
