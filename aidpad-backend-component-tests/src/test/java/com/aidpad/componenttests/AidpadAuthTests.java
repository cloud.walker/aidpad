package com.aidpad.componenttests;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.aidpad.AidpadApplication;
import com.aidpad.api.model.LoginRequest;
import com.aidpad.api.model.RegisterRequest;
import com.aidpad.api.model.RegisterRequestLinks;
import com.aidpad.api.model.RegisterRequestTO;
import com.aidpad.persistence.entities.RoleEntity;
import com.aidpad.persistence.entities.UserEntity;
import com.aidpad.persistence.entities.UserRoleEntity;
import com.aidpad.persistence.repositories.GroupMembersRepository;
import com.aidpad.persistence.repositories.OrganizationDetailsRepository;
import com.aidpad.persistence.repositories.PersonDetailsRepository;
import com.aidpad.persistence.repositories.RolesRepository;
import com.aidpad.persistence.repositories.UserRolesRepository;
import com.aidpad.persistence.repositories.UsersRepository;
import com.aidpad.users.AuthenticationType;
import com.aidpad.users.UserType;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest(classes = AidpadApplication.class)
@EnableAutoConfiguration(exclude = DataSourceAutoConfiguration.class)
public class AidpadAuthTests {

	@Autowired
	private ObjectMapper mapper;

	@Autowired
	private WebApplicationContext context;

	private static MockMvc mockMvc;

	@MockBean
	private RolesRepository rolesRepository;

	@MockBean
	private UsersRepository usersRepository;
	
	@MockBean
	private GroupMembersRepository groupMembersRepository; 
	
	@MockBean
	private PersonDetailsRepository personDetailsRepository;
	
	@MockBean
	private OrganizationDetailsRepository organizationDetailsRepository;
	
	@MockBean
	private UserRolesRepository userRolesRepository;
	
	@BeforeEach
	public void makeMockMvc() {
		mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
	}

	@Test
	void contextLoads() {
	}

	@Test
	public void loginWhenUserIsDisabled() throws Exception {
		UserEntity user = new UserEntity(UUID.randomUUID().toString(), "local", "person", "user 1", false, true);
		LoginRequest loginRequest = makeLoginRequest(user.getLoginName(), "password");
		when(usersRepository.findByLoginName(user.getLoginName())).thenReturn(user);
		mockMvc.perform(post("/aidpad/api/login").contentType(MediaType.APPLICATION_JSON_VALUE)
				.accept(MediaType.APPLICATION_JSON_VALUE).content(mapper.writeValueAsString(loginRequest)))
				.andExpect(status().isInternalServerError());
	}

	@Test
	public void loginWhenUserIsNotPresent() throws Exception {
		UserEntity user = new UserEntity(UUID.randomUUID().toString(), "local", "person", "user 1", false, true);
		LoginRequest loginRequest = makeLoginRequest("user 2", "password");
		when(usersRepository.findByLoginName(user.getLoginName())).thenReturn(user);
		mockMvc.perform(post("/aidpad/api/login").contentType(MediaType.APPLICATION_JSON_VALUE)
				.accept(MediaType.APPLICATION_JSON_VALUE).content(mapper.writeValueAsString(loginRequest)))
				.andExpect(status().isNotFound());

	}

	@Test
	public void loginWhenUserAndPasswordAreGood() throws Exception {
		Set<UserRoleEntity> userRoles = new HashSet<UserRoleEntity>();
		UserEntity user = new UserEntity(UUID.randomUUID().toString(), "local", "person",
				"$2a$10$pEzVtsED7FV9bzVpOclqteprISPZmF5W1M1ErUxO8UhjkRz0U2kJm", "user 1", true, false, null, null, null, userRoles);
		user.setId(1l);
		LoginRequest loginRequest = makeLoginRequest("user 1", "password");
		when(usersRepository.findByLoginName(user.getLoginName())).thenReturn(user);
		mockMvc.perform(post("/aidpad/api/login").contentType(MediaType.APPLICATION_JSON_VALUE)
				.accept(MediaType.APPLICATION_JSON_VALUE).content(mapper.writeValueAsString(loginRequest)))
				.andExpect(status().isOk());

	}

	@Test
	public void registerPersonSaysOK() throws Exception {
		String uuid = UUID.randomUUID().toString();
		UserEntity userEntity = new UserEntity(uuid, AuthenticationType.PASSWORD.name(), UserType.PERSON.name(),"user 9",false, true);
		
		String[] roleUuids = new String[] {
				UUID.randomUUID().toString(),
				UUID.randomUUID().toString()
		};
		List<UserRoleEntity> userRoles = new ArrayList<>();
		for (String roleUuid : roleUuids) {
			RoleEntity roleEntity = new RoleEntity(roleUuid, "test-role-" + roleUuid, null);
			when(rolesRepository.findByUuid(roleUuid)).thenReturn(roleEntity);
			userRoles.add(new UserRoleEntity(roleEntity, userEntity));
		}
		RegisterRequest registerRequest = makeRegisterRequest("user 9", AuthenticationType.PASSWORD.name(), UserType.PERSON.name(),uuid, roleUuids);
		mockMvc.perform(post("/aidpad/api/register").contentType(MediaType.APPLICATION_JSON_VALUE)
				.accept(MediaType.TEXT_PLAIN).content(mapper.writeValueAsString(registerRequest)))
				.andExpect(status().isOk());
	}

	@Test
	public void registerOrganization() throws Exception {
	}

	@Test
	public void registerInitiativeGroup() throws Exception {
	}

	private LoginRequest makeLoginRequest(String name, String password) {
		LoginRequest loginRequest = new LoginRequest();
		loginRequest.setLoginName(name);
		loginRequest.setPassword(password);
		return loginRequest;
	}
	
	private RegisterRequest makeRegisterRequest(String name, String authType, String userType, String uuid, String[] roleUuids) {
		RegisterRequest registerRequest = new RegisterRequest();
		RegisterRequestTO registerRequestTO = new RegisterRequestTO();
		registerRequestTO.setLoginName(name);
		registerRequestTO.setUserType(userType);
		registerRequestTO.setUuid(uuid);
		registerRequest.setRequest(registerRequestTO);
		RegisterRequestLinks registerRequestLinks = new RegisterRequestLinks();
		ArrayList<String> rolesLinks = new ArrayList<>();
		registerRequestLinks.setRoles(rolesLinks);
		for (String roleUuid : roleUuids) {
			rolesLinks.add("/aidpad/api/role/" + roleUuid);
		}
		registerRequest.setLinks(registerRequestLinks);
		return registerRequest;
	}
	
}
