package com.aidpad.componenttests;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
// This is from where to import result handlers
// import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.aidpad.AidpadApplication;
import com.aidpad.api.model.RoleTO;
import com.aidpad.api.model.RoleTOLinks;
import com.aidpad.api.model.RoleValueTO;
import com.aidpad.persistence.entities.RoleEntity;
import com.aidpad.persistence.repositories.GroupMembersRepository;
import com.aidpad.persistence.repositories.OrganizationDetailsRepository;
import com.aidpad.persistence.repositories.PersonDetailsRepository;
import com.aidpad.persistence.repositories.RolesRepository;
import com.aidpad.persistence.repositories.UserRolesRepository;
import com.aidpad.persistence.repositories.UsersRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest(classes = AidpadApplication.class)
@EnableAutoConfiguration(exclude = DataSourceAutoConfiguration.class)
class AidpadRolesTest {

	@Autowired
	private ObjectMapper mapper;
	
	@Autowired 
	private WebApplicationContext context;
	
	private static MockMvc mockMvc;

	@MockBean
	private RolesRepository rolesRepository;

	@MockBean
	private UsersRepository usersRepository;
	
	@MockBean
	private UserRolesRepository userRolesRepository;

	@MockBean
	private GroupMembersRepository groupMembersRepository; 

	@MockBean
	private PersonDetailsRepository personDetailsRepository;
	
	@MockBean
	private OrganizationDetailsRepository organizationDetailsRepository;
	
	@BeforeEach
	public void makeMockMvc() {
		mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
	}
	
	@Test
	void contextLoads() {
	}
	
	@Test
	public void getRolesReturnsAllRoles() throws Exception {
		List<RoleEntity> allRoles = new ArrayList<>();
		allRoles.add(new RoleEntity(UUID.randomUUID().toString(), "role 1", null));
		allRoles.add(new RoleEntity(UUID.randomUUID().toString(), "role 2", null));
		allRoles.add(new RoleEntity(UUID.randomUUID().toString(), "role 3", null));
		when(rolesRepository.findAll()).thenReturn(allRoles);
		
		mockMvc.perform(get("/aidpad/api/roles"))
//			.andDo(print()) // this is how you debug these tests
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.roles", hasSize(3)))
			.andExpect(jsonPath("$.roles[*].role.name", containsInAnyOrder("role 1", "role 2", "role 3")));
	}
	
	@Test
	public void getRolesWhenNoRolesReturnsEmptyList() throws Exception {
		when(rolesRepository.findAll()).thenReturn(Collections.emptyList());
		mockMvc.perform(get("/aidpad/api/roles"))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.roles", hasSize(0)));
	}

	@Test
	public void getRoleByUuidReturnsRole() throws Exception {
		RoleEntity role = new RoleEntity(UUID.randomUUID().toString(), "role 1", null);
		when(rolesRepository.findByUuid(role.getUuid())).thenReturn(role);
		
		mockMvc.perform(get("/aidpad/api/role/" + role.getUuid()))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.role.name", equalTo(role.getName())));
	}
	
	@Test
	public void updateRoleChangesName()  throws Exception {
		RoleEntity role = new RoleEntity(UUID.randomUUID().toString(), "role 1", null);
		when(rolesRepository.findByUuid(role.getUuid())).thenReturn(role);
		when(rolesRepository.save(any())).thenAnswer(new Answer<RoleEntity>() {
			@Override
			public RoleEntity answer(InvocationOnMock invocation) throws Throwable {
				return invocation.getArgument(0);
			}
		});
		
		RoleTO roleTO = makeRoleTO(role.getUuid(), "changed name");
		
		mockMvc.perform(put(roleTO.getLinks().getSelf())
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.accept(MediaType.APPLICATION_JSON_VALUE)
				.content(mapper.writeValueAsString(roleTO)))
			.andExpect(status().isNoContent());
		
		ArgumentCaptor<RoleEntity> savedRole = ArgumentCaptor.forClass(RoleEntity.class) ;
		verify(rolesRepository).save(savedRole.capture());
		assertThat(savedRole.getValue(), is(role));
		assertThat(savedRole.getValue().getName(), equalTo("changed name"));
	}
	
	@Test
	public void updateNonExistingRoleSaysNotFound()  throws Exception {
		RoleTO roleTO = makeRoleTO(UUID.randomUUID().toString(), "test role");
		
		mockMvc.perform(put("/aidpad/api/role/" + UUID.randomUUID().toString())
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.accept(MediaType.APPLICATION_JSON_VALUE)
				.content(mapper.writeValueAsString(roleTO)))
			.andExpect(status().isBadRequest());
		
		verify(rolesRepository, times(0)).save(any());
	}

	@Test
	public void updateRoleWithDifferentUuidInUrlSaysBadRequest()  throws Exception {
		RoleTO roleTO = makeRoleTO(UUID.randomUUID().toString(), "test role");
		
		mockMvc.perform(put(roleTO.getLinks().getSelf())
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.accept(MediaType.APPLICATION_JSON_VALUE)
				.content(mapper.writeValueAsString(roleTO)))
			.andExpect(status().isNotFound());
		
		verify(rolesRepository, times(0)).save(any());
	}

	@Test
	public void createRoleSavesNewRole() throws JsonProcessingException, Exception {
		when(rolesRepository.save(any())).thenAnswer(new Answer<RoleEntity>() {
			@Override
			public RoleEntity answer(InvocationOnMock invocation) throws Throwable {
				return invocation.getArgument(0);
			}
		});
		
		RoleTO roleTO = makeRoleTO(UUID.randomUUID().toString(), "test role");
		
		mockMvc.perform(post("/aidpad/api/role")
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.accept(MediaType.APPLICATION_JSON_VALUE)
				.content(mapper.writeValueAsString(roleTO)))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.role.name", equalTo(roleTO.getRole().getName())))
			.andExpect(jsonPath("$.role.uuid", equalTo(roleTO.getRole().getUuid())));
		
		ArgumentCaptor<RoleEntity> roleCaptor = ArgumentCaptor.forClass(RoleEntity.class); 
		verify(rolesRepository).save(roleCaptor.capture());
		assertThat(roleCaptor.getValue().getUuid(), equalTo(roleTO.getRole().getUuid()));
		assertThat(roleCaptor.getValue().getName(), equalTo(roleTO.getRole().getName()));
	}
	
	@Test
	public void createRoleWhenRoleExistsSaysBadRequest() throws JsonProcessingException, Exception {
		RoleEntity role = new RoleEntity(UUID.randomUUID().toString(), "test role", null);
		when(rolesRepository.findByUuid(role.getUuid())).thenReturn(role);
		
		RoleTO roleTO = makeRoleTO(role.getUuid(), role.getName());
		mockMvc.perform(post("/aidpad/api/role")
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.accept(MediaType.APPLICATION_JSON_VALUE)
				.content(mapper.writeValueAsString(roleTO)))
			.andExpect(status().isBadRequest());
	}
	
	@Test
	public void createRoleWithNoUuidSaysBadRequest() throws JsonProcessingException, Exception {
		RoleTO roleTO = makeRoleTO("", "test role");
		mockMvc.perform(post("/aidpad/api/role")
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.accept(MediaType.APPLICATION_JSON_VALUE)
				.content(mapper.writeValueAsString(roleTO)))
			.andExpect(status().isBadRequest());
	}

	@Test
	public void createRoleWithNoNameSaysBadRequest() throws JsonProcessingException, Exception {
		RoleTO roleTO = makeRoleTO(UUID.randomUUID().toString(), "");
		mockMvc.perform(post("/aidpad/api/role")
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.accept(MediaType.APPLICATION_JSON_VALUE)
				.content(mapper.writeValueAsString(roleTO)))
			.andExpect(status().isBadRequest());
	}
	
	private RoleTO makeRoleTO(String uuid, String name) {
		RoleTO roleTO = new RoleTO();
		roleTO.setRole(new RoleValueTO());
		roleTO.getRole().setUuid(uuid);
		roleTO.getRole().setName(name);
		roleTO.setLinks(new RoleTOLinks());
		roleTO.getLinks().setSelf("/aidpad/api/role/" + uuid);
		return roleTO;
	}
	
}
