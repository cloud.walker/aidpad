# AidPad backend component tests

These tests load the whole application context, letting spring do its wiring magic, but mock the database, leave out the actual web server and . Then they run requests through the spring dispatcher servlet as if they would ve dispatched by a servlet container, and controllers and other components interact as in the real application, except that they hit mocks instead of real repositories.

This allows testing all things in the application, except for the interaction with the database. Since no servlet container and no database is involved, these tests are still pretty cheap and fast, and require no external dependencies. The only thing making them somewhat slower than unit tests is the fact that spring fires up the entire application context once per each test class.

