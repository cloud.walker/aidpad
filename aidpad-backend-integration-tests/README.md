# Integration tests

These tests are meant to test all things that the app uses to interact with the outside world.

In our case, this means two kinds of components:
* The spring security parts which interact with Google and Facebook
* Repositories used to access the database

## Spring security parts

I have no idea. We're not yet there. 
{::comment} TODO: Simina should chime in here. {:/comment}
<!-- TODO: Simina should chime in here. -->

## Repository tests

These tests need to test two things:
* That the entity a repo is in charge of can be saved/created
* That all custom methods defined on repos execute correctly.

In general, these tests are not difficult to write, save for the part of creating test data plus cleanup. Two techniques can be used to make this easier:

* Some data is always created by migrations. For example the initial user of an app, if kept in the DB, permissions and roles, entities used in more complex tests as test data etc. Use it here too.
* Annotate your ests `@Rollback`, and delete all entities from the repo at the very beginning. Then start with a clean slate.   

