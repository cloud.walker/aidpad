package com.aidpad.integrationtests.persistence;

import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.aidpad.AidpadApplication;
import com.aidpad.persistence.entities.RoleEntity;
import com.aidpad.persistence.entities.UserEntity;
import com.aidpad.persistence.entities.UserRoleEntity;
import com.aidpad.persistence.repositories.RolesRepository;
import com.aidpad.persistence.repositories.UserRolesRepository;
import com.aidpad.persistence.repositories.UsersRepository;
import com.aidpad.roles.Roles;
import com.aidpad.users.AuthenticationType;
import com.aidpad.users.UserType;

@SpringBootTest(classes = AidpadApplication.class)
@RunWith(SpringJUnit4ClassRunner.class)
class UsersRepositoryIntegrationTest {

	// look into the database migrations, where test users are created, for where
	// this values come from
	private static final String GOOGLE_PERSON_UUID = "7c5ed8f8-9ced-11ea-b6e0-20cf305ca7c0";
	private static final String GOOGLE_GROUP_UUID = "35f5a3c0-9cec-11ea-9b5d-20cf305ca7c0";
	private static final String GOOGLE_PERSON_LOGIN_NAME = "test4@gmail.com";
	private static final String FACEBOOK_PERSON_UUID = "3653a39e-9cec-11ea-bf97-20cf305ca7c0";
	private static final String UNAMEPWD_ORGANIZATION_UUID = "02886fe0-9cec-11ea-a061-20cf305ca7c0";
	private static final String BENEFICIARY_ROLE_UUID = "12e609f2-9946-11ea-b6da-dcce74dcef1b";

	@Autowired
	private UsersRepository usersRepository;

	@Autowired
	private RolesRepository rolesRepository;

	@Autowired
	private UserRolesRepository userRolesRepository;

	@Test
	void contextLoads() {
	}

	@Test
	void canFindUserByLoginName() {
		UserEntity byLoginName = usersRepository.findByLoginName(GOOGLE_PERSON_LOGIN_NAME);
		assertThat(byLoginName.getUuid(), equalTo(GOOGLE_PERSON_UUID));
	}

	@Transactional
	@Rollback
	@Test
	public void canCreateUser() {
		UserEntity newUser = new UserEntity();
		newUser.setLoginName("component.test@example.com");
		newUser.setUuid(UUID.randomUUID().toString());
		newUser.setAuthType(AuthenticationType.GOOGLE.name());
		newUser.setUserType(UserType.PERSON.name());
		newUser.setEnabled(false);
		newUser.setMustChangePassword(true);
		newUser.setPasswordHash("test-hash");

		UserEntity savedUser = usersRepository.save(newUser);

		assertThat(savedUser.getLoginName(), equalTo(newUser.getLoginName()));
		assertThat(savedUser.getLoginName(), equalTo(newUser.getLoginName()));
		assertThat(savedUser.getAuthType(), equalTo(newUser.getAuthType()));
		assertThat(savedUser.getUserType(), equalTo(newUser.getUserType()));
		assertThat(savedUser.isEnabled(), equalTo(newUser.isEnabled()));
		assertThat(savedUser.isMustChangePassword(), equalTo(newUser.isMustChangePassword()));
		assertThat(savedUser.getPasswordHash(), equalTo(newUser.getPasswordHash()));
	}
	
	@Transactional
	@Rollback
	@Test
	public void canAttachRolesToUser() {
		UserEntity newUser = new UserEntity();
		newUser.setLoginName("component.test@example.com");
		newUser.setUuid(UUID.randomUUID().toString());
		newUser.setAuthType(AuthenticationType.GOOGLE.name());
		newUser.setUserType(UserType.PERSON.name());
		newUser.setEnabled(false);
		newUser.setMustChangePassword(true);
		newUser.setPasswordHash("test-hash");

		Set<RoleEntity> allRoles = new HashSet<>();
		rolesRepository.findAll().forEach(allRoles::add);

		UserEntity savedUser = usersRepository.save(newUser);

		allRoles.stream()
				.filter(role -> role.getName().equals(Roles.ADMINISTRATOR.name())
						|| role.getName().equals(Roles.FINANCIER.name()))
				.forEach(role -> {
					// This is stupid. But I didn't find any other way. Because the relation is many
					// to many, i.e.
					// no way to model it as an aggregation, it seems there is no way to force an
					// automated
					// cascaded update.
					savedUser.getRoles().add(userRolesRepository.save(new UserRoleEntity(role, savedUser)));
				});
		// make sure user gets completely reloaded upon re-query
		usersRepository.flush();
		UserEntity reloadedUser = usersRepository.findById(savedUser.getId()).get();

		assertThat(reloadedUser.getRoles(), hasSize(2));
		assertThat(reloadedUser.getRoles().stream().map(userRole -> userRole.getRole().getName())
				.collect(Collectors.toList()), containsInAnyOrder(Roles.ADMINISTRATOR.name(), Roles.FINANCIER.name()));
	}

	@Transactional
	@Rollback
	@Test
	public void canSaveUserWithRoles() {
		UserEntity newUser = new UserEntity();
		newUser.setLoginName("component.test@example.com");
		newUser.setUuid(UUID.randomUUID().toString());
		newUser.setAuthType(AuthenticationType.GOOGLE.name());
		newUser.setUserType(UserType.PERSON.name());
		newUser.setEnabled(false);
		newUser.setMustChangePassword(true);
		newUser.setPasswordHash("test-hash");

		Set<RoleEntity> allRoles = new HashSet<>();
		rolesRepository.findAll().forEach(allRoles::add);

		UserEntity savedUser = usersRepository.save(newUser);

		allRoles.stream().filter(role -> role.getName().equals(Roles.ADMINISTRATOR.name())
				|| role.getName().equals(Roles.FINANCIER.name())).forEach(role -> {
					savedUser.getRoles().add(userRolesRepository.save(new UserRoleEntity(role, savedUser)));
				});
		// make sure user gets completely reloaded
		usersRepository.flush();
		UserEntity reloadedUser = usersRepository.findById(savedUser.getId()).get();

		// make sure re-saved user doesn't just get reloaded from the cache
		usersRepository.flush();
		UserEntity resavedUser = usersRepository.save(reloadedUser);

		assertThat(resavedUser.getRoles(), hasSize(2));
		assertThat(resavedUser.getRoles().stream().map(userRole -> userRole.getRole().getName())
				.collect(Collectors.toList()), containsInAnyOrder(Roles.ADMINISTRATOR.name(), Roles.FINANCIER.name()));
	}

	@Transactional
	@Rollback
	@Test
	public void findsUsersWithStartUuidAndCountCorrectly() {
		List<UserEntity> allUsers = usersRepository.findAll().stream()
				.sorted((user1, user2) -> user1.getUuid().compareTo(user2.getUuid())).collect(Collectors.toList());

		String startUuid = allUsers.get(1).getUuid();

		List<UserEntity> pagedUsers = usersRepository.findPaged(startUuid, 2, null, null, null, null, null);

		assertThat(pagedUsers, hasSize(2));
		assertThat(pagedUsers.get(0).getUuid(), equalTo(startUuid));
		assertThat(pagedUsers.get(1).getUuid(), equalTo(allUsers.get(2).getUuid()));
	}

	@Transactional
	@Rollback
	@Test
	public void findsUsersWithAuthType() {
		List<String> googleAuthTypeAsList = new ArrayList<String>();
		googleAuthTypeAsList.add(AuthenticationType.GOOGLE.name());
		List<String> googleUserUuids = usersRepository
				.findPaged(null, 1000, null, null, googleAuthTypeAsList, null, null).stream().map(UserEntity::getUuid)
				.collect(Collectors.toList());
		assertThat(googleUserUuids, hasItems(GOOGLE_GROUP_UUID, GOOGLE_PERSON_UUID));
	}

	@Transactional
	@Rollback
	@Test
	public void findsUsersWithUserType() {
		List<String> personUserTypeAsList = new ArrayList<String>();
		personUserTypeAsList.add(UserType.PERSON.name());
		List<String> googleUserUuids = usersRepository
				.findPaged(null, 1000, null, personUserTypeAsList, null, null, null).stream().map(UserEntity::getUuid)
				.collect(Collectors.toList());
		assertThat(googleUserUuids, hasItems(FACEBOOK_PERSON_UUID, GOOGLE_PERSON_UUID));
	}

	@Transactional
	@Rollback
	@Test
	public void findsUsersWithRoles() {
		RoleEntity beneficiary = rolesRepository.findByUuid(BENEFICIARY_ROLE_UUID);

		List<Long> beneficiaryRoleIdAsList = new ArrayList<Long>();
		beneficiaryRoleIdAsList.add(beneficiary.getId());
		List<String> googleUserUuids = usersRepository
				.findPaged(null, 1000, beneficiaryRoleIdAsList, null, null, null, null).stream()
				.map(UserEntity::getUuid).collect(Collectors.toList());
		assertThat(googleUserUuids, hasItems(GOOGLE_GROUP_UUID, UNAMEPWD_ORGANIZATION_UUID));
	}

	@Transactional
	@Rollback
	@Test
	public void findsUsersByEnabledValue() {
		List<String> enabledUserUuids = usersRepository.findPaged(null, 1000, null, null, null, true, null).stream()
				.map(UserEntity::getUuid).collect(Collectors.toList());
		assertThat(enabledUserUuids, not(hasItem(GOOGLE_GROUP_UUID)));
		assertThat(enabledUserUuids, not(hasItem(GOOGLE_PERSON_UUID)));
		assertThat(enabledUserUuids, not(hasItem(FACEBOOK_PERSON_UUID)));
		assertThat(enabledUserUuids, not(hasItem(UNAMEPWD_ORGANIZATION_UUID)));

		List<String> disabledUserUuids = usersRepository.findPaged(null, 1000, null, null, null, false, null).stream()
				.map(UserEntity::getUuid).collect(Collectors.toList());
		assertThat(disabledUserUuids, hasItems(
				GOOGLE_GROUP_UUID, GOOGLE_PERSON_UUID, FACEBOOK_PERSON_UUID, UNAMEPWD_ORGANIZATION_UUID));
	}
	
	@Transactional
	@Rollback
	@Test
	public void findsUsersByMustChangedPasswordValue() {
		List<String> mustChangePasswordUserUuids = usersRepository.findPaged(null, 1000, null, null, null, null, false).stream()
				.map(UserEntity::getUuid).collect(Collectors.toList());
		assertThat(mustChangePasswordUserUuids, not(hasItem(GOOGLE_GROUP_UUID)));
		assertThat(mustChangePasswordUserUuids, not(hasItem(GOOGLE_PERSON_UUID)));
		assertThat(mustChangePasswordUserUuids, not(hasItem(FACEBOOK_PERSON_UUID)));
		assertThat(mustChangePasswordUserUuids, not(hasItem(UNAMEPWD_ORGANIZATION_UUID)));

		List<String> haveValidPasswordUserUuids = usersRepository.findPaged(null, 1000, null, null, null, null, true).stream()
				.map(UserEntity::getUuid).collect(Collectors.toList());
		assertThat(haveValidPasswordUserUuids, hasItems(
				GOOGLE_GROUP_UUID, GOOGLE_PERSON_UUID, FACEBOOK_PERSON_UUID, UNAMEPWD_ORGANIZATION_UUID));
	}
	
	@Transactional
	@Rollback
	@Test
	public void findsUuidForPreviousLink() {
		List<String> mustChangePasswordUserUuids = usersRepository.findPaged(null, 1000, null, null, null, null, true).stream()
				.map(UserEntity::getUuid).collect(Collectors.toList());
		
		assertThat(mustChangePasswordUserUuids.size(), greaterThan(4));
		
		final int PAGE_SIZE = 2;
		String startUuid = mustChangePasswordUserUuids.get(PAGE_SIZE + 1);
		
		List<String> prevPageUuids = usersRepository.findUidsReversed(startUuid, 2, null, null, null, null, true);
		
		assertThat(prevPageUuids, hasSize(PAGE_SIZE));
		
		assertThat(prevPageUuids.get(1), equalTo(mustChangePasswordUserUuids.get(0)));
		assertThat(prevPageUuids.get(0), equalTo(mustChangePasswordUserUuids.get(1)));
	}
	
	@Transactional
	@Rollback
	@Test
	public void findsNextUuidCorrectly() {
		List<String> mustChangePasswordUserUuids = usersRepository.findPaged(null, 1000, null, null, null, null, true).stream()
				.map(UserEntity::getUuid).collect(Collectors.toList());
		
		assertThat(mustChangePasswordUserUuids.size(), greaterThan(4));
		
		String nextUuid =  usersRepository.findNextUuid(mustChangePasswordUserUuids.get(1), null, null, null, null, true);
		
		assertThat(nextUuid, equalTo(mustChangePasswordUserUuids.get(2)));
	}
	
	@Transactional
	@Rollback
	@Test
	public void findsEmptyRevPageWhenAlreadyAtStart() {
		List<String> mustChangePasswordUserUuids = usersRepository.findPaged(null, 1000, null, null, null, null, true).stream()
				.map(UserEntity::getUuid).collect(Collectors.toList());
		
		assertThat(mustChangePasswordUserUuids.size(), greaterThan(4));
		
		final int PAGE_SIZE = 3;
		String startUuid = mustChangePasswordUserUuids.get(0);
		
		List<String> prevPageUuids = usersRepository.findUidsReversed(startUuid, PAGE_SIZE, null, null, null, null, true);
		
		assertThat(prevPageUuids, hasSize(0));
	}
	
}
