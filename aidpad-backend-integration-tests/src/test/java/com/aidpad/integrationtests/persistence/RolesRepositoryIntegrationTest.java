package com.aidpad.integrationtests.persistence;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.aidpad.AidpadApplication;
import com.aidpad.persistence.entities.RoleEntity;
import com.aidpad.persistence.repositories.RolesRepository;

@SpringBootTest(classes = AidpadApplication.class)
@RunWith(SpringJUnit4ClassRunner.class)
class RolesRepositoryIntegrationTest {

	@Autowired
	private RolesRepository rolesRepository;
	
	@Test
	void contextLoads() {
	}
	
	@Test
	public void canFindRoleByUuid() {
		List<RoleEntity> allRoles = new ArrayList<>();
		rolesRepository.findAll().forEach(allRoles::add);;
		RoleEntity adminRole = allRoles.stream().filter(role -> role.getName().equals("ADMINISTRATOR")).findFirst().get();
		
		RoleEntity retrieved = rolesRepository.findByUuid(adminRole.getUuid());
		assertThat(retrieved.getId(), equalTo(adminRole.getId()));
		assertThat(retrieved.getName(), equalTo(adminRole.getName()));
		assertThat(retrieved.getUuid(), equalTo(adminRole.getUuid()));
	}

	@Transactional
	@Rollback
	@Test
	public void canCreateRole() {
		rolesRepository.deleteAll();
		RoleEntity newRole = new RoleEntity(UUID.randomUUID().toString(), "test role", null);
		RoleEntity savedRole = rolesRepository.save(newRole);
		
		assertThat(savedRole.getUuid(), equalTo(newRole.getUuid()));
		assertThat(savedRole.getName(), equalTo(newRole.getName()));
	}
}
