package com.aidpad.integrationtests;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.aidpad.AidpadApplication;

@SpringBootApplication
public class AidpadIntegrationTests {

	public static void main(String[] args) {
		SpringApplication.run(AidpadApplication.class, args);
	}
}
