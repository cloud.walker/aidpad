# OpenApi specification

The [OpenAPi spec](https://swagger.io/specification/) documents a way to specify APIs in a language independent way.

Used together with code generators for various languages, it can be used to do API-first development.

For aidpad, we chose this way to go. This directory contains the aidpad API spec. Both client and server side code is generated from it.

Server-side, the code is generated using the openapi generator maven plugin. The solution largely just copies what's done [here](https://github.com/thombergs/code-examples/tree/master/spring-boot/spring-boot-openapi-impl).

When the aidpad backend is running, an executable documentation of the API is available at /swagger-ui.html, using the swagger UI.

Since the client only consumes the API, there's no server-like code to be generated for the client.

## Organization of the API definitions

One single file containing all transfer objects and endpoint definitions would be unwieldy, therefore the API definition is split into separate files for each kind of resource we use.

[This tutorial](https://apihandyman.io/writing-openapi-swagger-specification-tutorial-part-8-splitting-specification-file/) documents how to split a large openapi definition into multiple files.

The structure to use is:
* one directory for each resource
* inside that directory, an api file and a model file
* the api file contains all endpoint definitions, save the paths
* the model definitions contains schemas for all request and response bodies
* the amin api file - [aidpad.yaml](aidpad.yaml) - just aggregates all of those files

## A note about build warnings

When the code is generated, you might receive some harmless warnings that some models already exist. This is caused by models being referenced both from the api file, the main [aidpad.yaml](aidpad.yaml) and potentially from models themselves. It does not, however, impact code generation.

## Example

The first resource to have its API defined were roles. Therefore, the roles definitions should be used as an example.
