#Using mybatis migrations from maven

It's basically the same as using the [standalone tool](http://mybatis.org/migrations/), only it runs using the [mybatis maven plugin](http://mybatis.org/migrations-maven-plugin/new-mojo.html).

Configuration and script files and environments (see the documentation) are all the same, only calling the tool is different.

The basic form of all commands is:

    mvn -Dmigration.path=migrations migration:<command>
    
where `<command>` is what you'd pass to the command line version too.

For convenience, the very few most frequently used commands are described below.

#Most frequently used commands


## Create new migration
    
    mvn -Dmigration.path=migrations migration:new -Dmigration.description="<description for the new migration>"
    
This will create a new migration script in the `/migrations/scripts` directory. After creation, you have to add sql for perfoming the migration and undoing it into that script. The script contains markers and comments to guide you.  

Take great care to keep the SQL you enter into the migrations scripts executable in one transaction. MariaDB and MySQL are not capable of executing more than one DDL statement in one transaction, therefore, if you have to modify the schema of the database, create multiple scripts, one for each DDL operation.

## Update database to latest version

    mvn -Dmigration.path=migrations migration:up

You can check that the migration was performed properly by looking into the table CHANGELOG inside the database.

## Roll back just the last applied script

    mvn -Dmigration.path=migrations migration:down

Same way to check as above. The reason the up and down operations are asymmetrical is that this turned out to be most convenient - you usually do up everywhere, in production or elsewhere, whereas you use down mostly while working locally on one new migration script, so you really only want down to undo just the latest migration most of the time. 
 
## Upgrade/downgrade to a particular version


    mvn -Dmigration.path=migrations -Dmigration.version=<particular version> migration:version
 
This will upgrade or downgrade the database, by rolling the do or undo parts of the various scripts, to the particular version you specify. The version is the timestamp part of the script file name which you want to be the last applied to the database. 

Updating to a particular version is useful when deploying in production. The latest stable build artifact might not be the most recent one. Therefore, when deploying it, you don't want the database it uses to be updated to the very latest version, but to the particular version that was in place at the time the artifact was built. 


