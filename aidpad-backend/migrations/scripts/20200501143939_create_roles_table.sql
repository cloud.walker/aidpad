-- // Create roles table 
-- Migration SQL that makes the change goes here.
CREATE TABLE roles (
	id BIGINT auto_increment NOT NULL,
	uuid varchar(37) NOT NULL,
	name varchar(100) NULL,
	CONSTRAINT PK_roles PRIMARY KEY (id),
	CONSTRAINT UN_roles_uuid UNIQUE KEY (uuid),
	CONSTRAINT UN_roles_name UNIQUE KEY (name)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8mb4
COLLATE=utf8mb4_general_ci;

-- //@UNDO
-- SQL to undo the change goes here.
DROP TABLE roles;
