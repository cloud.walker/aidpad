-- // Assign roles to test users - drop temp procedure possibly surviving some failed transaction
DROP PROCEDURE IF EXISTS tmp;

-- //@UNDO
-- SQL to undo the change goes here.
-- @DELIMITER |
DROP PROCEDURE IF EXISTS tmp;
