-- // Create users table
-- Migration SQL that makes the change goes here.
CREATE TABLE users (
	id BIGINT auto_increment NOT NULL,
	uuid varchar(37) NOT NULL,
	auth_type varchar(25) NOT NULL,
	user_type varchar(25) NOT NULL,
	password_hash varchar(100) NULL,
	login_name varchar(100) NOT NULL,
	enabled BOOL DEFAULT false NOT NULL,
	must_change_password BOOL DEFAULT true NOT NULL,
	CONSTRAINT users_PK PRIMARY KEY (id),
	CONSTRAINT users_login_name UNIQUE KEY (login_name)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8mb4
COLLATE=utf8mb4_general_ci;

-- //@UNDO
-- SQL to undo the change goes here.
DROP TABLE users; 

