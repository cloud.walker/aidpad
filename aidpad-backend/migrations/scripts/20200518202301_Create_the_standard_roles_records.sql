-- // Create the standard roles records
-- Migration SQL that makes the change goes here.
INSERT INTO roles
    (uuid, name)
VALUES
    ('8bebb158-9945-11ea-b6da-dcce74dcef1b', "ADMINISTRATOR"),
    ('f80a11f8-9945-11ea-b6da-dcce74dcef1b', "FINANCIER"),
    ('0a8d77f3-9946-11ea-b6da-dcce74dcef1b', "ASSESSOR"),
    ('12e609f2-9946-11ea-b6da-dcce74dcef1b', "BENEFICIARY");

-- //@UNDO
-- SQL to undo the change goes here.
DELETE FROM roles
WHERE
   uuid = '8bebb158-9945-11ea-b6da-dcce74dcef1b'
   OR uuid = 'f80a11f8-9945-11ea-b6da-dcce74dcef1b'
   OR uuid = '0a8d77f3-9946-11ea-b6da-dcce74dcef1b'
   OR uuid = '12e609f2-9946-11ea-b6da-dcce74dcef1b';

