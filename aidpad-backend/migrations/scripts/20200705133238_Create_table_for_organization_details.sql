-- // Create table for organization details
-- Migration SQL that makes the change goes here.
CREATE TABLE organization_details (
	id BIGINT auto_increment NOT NULL,
	user_id BIGINT NOT NULL,
	name varchar(100) NOT NULL,
	fiscal_identification_code varchar(25) NOT NULL,
	contact_email varchar(50) NOT NULL,
	web_site varchar(50) NULL,
	facebook_page varchar(100) NULL,
	CONSTRAINT organization_details_PK PRIMARY KEY (id),
	CONSTRAINT organization_details_FK FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE ON UPDATE CASCADE
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8mb4
COLLATE=utf8mb4_general_ci;

-- //@UNDO
-- SQL to undo the change goes here.
DROP TABLE organization_details;
