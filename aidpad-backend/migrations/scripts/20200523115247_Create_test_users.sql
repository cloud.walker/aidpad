-- // Create test users
-- Migration SQL that makes the change goes here.
INSERT INTO users
	(uuid, auth_type, user_type, password_hash, login_name, enabled, must_change_password)
VALUES
	('02886fe0-9cec-11ea-a061-20cf305ca7c0', 'PASSWORD', 'ORGANISATION', '', 'test1@example.org', 0, 1),
	('35f5a3c0-9cec-11ea-9b5d-20cf305ca7c0', 'GOOGLE', 'GROUP', '', 'test2@gmail.com', 0, 1),
	('3653a39e-9cec-11ea-bf97-20cf305ca7c0', 'FACEBOOK', 'PERSON', '', 'facebook\\test3', 0, 1),
	('7c5ed8f8-9ced-11ea-b6e0-20cf305ca7c0', 'GOOGLE', 'PERSON', '', 'test4@gmail.com', 0, 1);

-- //@UNDO
-- SQL to undo the change goes here.
DELETE FROM users 
WHERE
	uuid = '02886fe0-9cec-11ea-a061-20cf305ca7c0'
	OR uuid = '35f5a3c0-9cec-11ea-9b5d-20cf305ca7c0'
	OR uuid = '3653a39e-9cec-11ea-bf97-20cf305ca7c0'
	OR uuid = '7c5ed8f8-9ced-11ea-b6e0-20cf305ca7c0';
