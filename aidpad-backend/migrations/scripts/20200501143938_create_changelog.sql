-- // Create Changelog
-- Be sure that ID and DESCRIPTION fields exist in
-- BigInteger and String compatible fields respectively.
CREATE TABLE ${changelog} (
	ID BIGINT auto_increment NOT NULL,
	APPLIED_AT VARCHAR(25) NOT NULL,
	DESCRIPTION VARCHAR(255) NOT NULL,
	CONSTRAINT PK_${changelog} PRIMARY KEY (ID)
);

-- //@UNDO
DROP TABLE ${changelog};
