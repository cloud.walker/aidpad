-- // Assign roles to test users 2 - create temp procedure
CREATE PROCEDURE tmp() 
BEGIN

DECLARE test1_id BIGINT;
DECLARE test2_id BIGINT;
DECLARE test3_id BIGINT;
DECLARE test4_id BIGINT;

DECLARE benef_id BIGINT;
DECLARE admin_id BIGINT;
DECLARE finan_id BIGINT;
DECLARE assess_id BIGINT;

SELECT id FROM users WHERE uuid = '02886fe0-9cec-11ea-a061-20cf305ca7c0' INTO test1_id;
SELECT id FROM users WHERE uuid = '35f5a3c0-9cec-11ea-9b5d-20cf305ca7c0' INTO test2_id;
SELECT id FROM users WHERE uuid = '3653a39e-9cec-11ea-bf97-20cf305ca7c0' INTO test3_id;
SELECT id FROM users WHERE uuid = '7c5ed8f8-9ced-11ea-b6e0-20cf305ca7c0' INTO test4_id;

SELECT id FROM roles WHERE uuid = '12e609f2-9946-11ea-b6da-dcce74dcef1b' INTO benef_id;
SELECT id FROM roles WHERE uuid = '8bebb158-9945-11ea-b6da-dcce74dcef1b' INTO admin_id; 
SELECT id FROM roles WHERE uuid = 'f80a11f8-9945-11ea-b6da-dcce74dcef1b' INTO finan_id ;
SELECT id FROM roles WHERE uuid = '0a8d77f3-9946-11ea-b6da-dcce74dcef1b' INTO assess_id;

INSERT INTO user_x_roles
	(user_id, role_id)
VALUES
	(test1_id, benef_id),
	(test2_id, benef_id),
	(test3_id, admin_id),
	(test3_id, finan_id),
	(test4_id, assess_id);

END;

-- //@UNDO
-- SQL to undo the change goes here.
CALL tmp();
