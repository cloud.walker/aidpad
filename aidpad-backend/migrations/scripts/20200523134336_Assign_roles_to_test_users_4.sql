-- // Assign roles to test users 4 - drop temp procedure
DROP PROCEDURE IF EXISTS tmp;

-- //@UNDO
-- SQL to undo the change goes here.
-- @DELIMITER |
DROP PROCEDURE IF EXISTS tmp;
