-- // Create users-roles relation table
-- Migration SQL that makes the change goes here.
CREATE TABLE user_x_roles (
	id BIGINT auto_increment NOT NULL,
	user_id BIGINT NOT NULL,
	role_id BIGINT NOT NULL,
	CONSTRAINT user_roles_PK PRIMARY KEY (id),
	CONSTRAINT users_roles_FK_user FOREIGN KEY (user_id) REFERENCES aidpad.users(id) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT users_roles_FK_role FOREIGN KEY (role_id) REFERENCES aidpad.roles(id) ON DELETE CASCADE ON UPDATE CASCADE
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8mb4
COLLATE=utf8mb4_general_ci;

-- //@UNDO
-- SQL to undo the change goes here.
DROP TABLE user_x_roles; 
