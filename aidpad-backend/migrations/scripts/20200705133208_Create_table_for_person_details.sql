-- // Create table for person details
-- Migration SQL that makes the change goes here.
CREATE TABLE person_details (
	id BIGINT auto_increment NOT NULL,
	person_name varchar(100) NOT NULL,
	email varchar(50) NULL,
	user_id BIGINT NOT NULL,
	CONSTRAINT person_details_PK PRIMARY KEY (id),
	CONSTRAINT person_details_FK FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE ON UPDATE CASCADE
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8mb4
COLLATE=utf8mb4_general_ci;

-- //@UNDO
-- SQL to undo the change goes here.
DROP TABLE person_details;

