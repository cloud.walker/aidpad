# Local development

## Setting up the development environment
- install Java 11 JDK
- install maven 3+

## Setting up the database
- set up mariadb locally
- create an empty database named `aidpad`
- create a user named `aidpad` in mariadb
- grant it select, update, insert and delete rights on the `aidpad` database
- update `src/main/resources/application.yaml` to reflect your local settings
- _never commit or push the file `application.yaml` with your local settings in it_

## Before going on
- run `mvn clean install` in the root of this project

A full build plus installation into the local maven repo is required before importing projects into the IDE to avoid problems with inter-project dependencies not reflected by the POM files. 
