package com.aidpad;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import com.aidpad.config.AppProperties;

@SpringBootApplication
@EnableConfigurationProperties(AppProperties.class)
public class AidpadApplication {

	public static void main(String[] args) {
		SpringApplication.run(AidpadApplication.class, args);
	}
}
