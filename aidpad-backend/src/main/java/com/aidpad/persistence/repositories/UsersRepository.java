package com.aidpad.persistence.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.aidpad.persistence.entities.UserEntity;

public interface UsersRepository extends BaseRepository<UserEntity> {
	public UserEntity findByLoginName(String loginName);

	@Query(value =
			    "select " + 
			    "    users.* " + 
			    "from " + 
			    "    users " + 
			    "    left join user_x_roles on users.id = user_x_roles.user_id " + 
			    "where " + 
			    "    ( :startUuid is null or uuid >= :startUuid ) " + 
			    "    and ( :roleIds is null or user_x_roles.role_id in (:roleIds) ) " + 
			    "    and ( :userTypes is null or users.user_type in (:userTypes) ) " + 
			    "    and ( :authTypes is null or users.auth_type in (:authTypes) ) " + 
			    "    and ( :enabled is null or users.enabled = :enabled) " + 
			    "    and ( :mustChangePassword is null or users.must_change_password = :mustChangePassword ) " + 
			    "order by " + 
			    "    users.uuid asc " + 
			    "limit " + 
			    "     :count ", nativeQuery = true)
	public List<UserEntity> findPaged(
			@Param("startUuid") String startUuid, 
			@Param("count") int count, 
			@Param("roleIds") List<Long> roleIds,
			@Param("userTypes") List<String> userTypes,
			@Param("authTypes") List<String> authTypes,
			@Param("enabled") Boolean enabled,
			@Param("mustChangePassword") Boolean mustChangePassword);
	
	// used to build the prev link in users lists
	@Query(value =
		    "select " + 
		    "    users.uuid " + 
		    "from " + 
		    "    users " + 
		    "    left join user_x_roles on users.id = user_x_roles.user_id " + 
		    "where " + 
		    "    ( :startUuid is null or uuid < :startUuid ) " + 
		    "    and ( :roleIds is null or user_x_roles.role_id in (:roleIds) ) " + 
		    "    and ( :userTypes is null or users.user_type in (:userTypes) ) " + 
		    "    and ( :authTypes is null or users.auth_type in (:authTypes) ) " + 
		    "    and ( :enabled is null or users.enabled = :enabled) " + 
		    "    and ( :mustChangePassword is null or users.must_change_password = :mustChangePassword ) " + 
		    "order by " + 
		    "    users.uuid desc " + 
		    "limit " + 
		    "     :count ", nativeQuery = true)
	public List<String> findUidsReversed(
			@Param("startUuid") String startUuid, 
			@Param("count") int count, 
			@Param("roleIds") List<Long> roleIds,
			@Param("userTypes") List<String> userTypes,
			@Param("authTypes") List<String> authTypes,
			@Param("enabled") Boolean enabled,
			@Param("mustChangePassword") Boolean mustChangePassword);

	@Query(value =
		    "select " + 
		    "    users.uuid " + 
		    "from " + 
		    "    users " + 
		    "    left join user_x_roles on users.id = user_x_roles.user_id " + 
		    "where " + 
		    "    ( :startUuid is null or uuid > :startUuid ) " + 
		    "    and ( :roleIds is null or user_x_roles.role_id in (:roleIds) ) " + 
		    "    and ( :userTypes is null or users.user_type in (:userTypes) ) " + 
		    "    and ( :authTypes is null or users.auth_type in (:authTypes) ) " + 
		    "    and ( :enabled is null or users.enabled = :enabled) " + 
		    "    and ( :mustChangePassword is null or users.must_change_password = :mustChangePassword ) " + 
		    "order by " + 
		    "    users.uuid asc " + 
		    "limit 1", nativeQuery = true)
	public String findNextUuid(
			@Param("startUuid") String startUuid, 
			@Param("roleIds") List<Long> roleIds,
			@Param("userTypes") List<String> userTypes,
			@Param("authTypes") List<String> authTypes,
			@Param("enabled") Boolean enabled,
			@Param("mustChangePassword") Boolean mustChangePassword);

}
