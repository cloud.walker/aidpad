package com.aidpad.persistence.repositories;

import com.aidpad.persistence.entities.RoleEntity;

public interface RolesRepository extends BaseRepository<RoleEntity> {
	public RoleEntity findByName(String name);
}
