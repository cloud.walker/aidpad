package com.aidpad.persistence.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.aidpad.persistence.entities.PersonDetailsEntity;

public interface PersonDetailsRepository extends JpaRepository<PersonDetailsEntity, Long> {
	public PersonDetailsEntity findByUserId(long userId);
}
