package com.aidpad.persistence.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.aidpad.persistence.entities.GroupMemberDetailsEntity;

public interface GroupMembersRepository extends JpaRepository<GroupMemberDetailsEntity, Long>{
	public List<GroupMemberDetailsEntity> findByUserId(long userId);
}
