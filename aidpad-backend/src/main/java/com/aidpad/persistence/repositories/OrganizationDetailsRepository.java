package com.aidpad.persistence.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.aidpad.persistence.entities.OrganizationDetailsEntity;

public interface OrganizationDetailsRepository extends JpaRepository<OrganizationDetailsEntity, Long> {
	public OrganizationDetailsEntity findByUserId(long userId);
}
