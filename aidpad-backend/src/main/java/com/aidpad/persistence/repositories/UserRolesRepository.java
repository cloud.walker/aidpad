package com.aidpad.persistence.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.aidpad.persistence.entities.UserRoleEntity;

public interface UserRolesRepository extends JpaRepository<UserRoleEntity, Long> {
}
