package com.aidpad.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class RandomStringGenerator {

	public static final String ALLOWED_CHARACTERS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	
	private final int minLength;
	private final int maxLength;
	
	public RandomStringGenerator(@Value("${app.auth.tokenMinLength:15}") int minLength, @Value("${app.auth.tokenMaxLength:25}") int maxLength) {
		this.minLength = minLength;
		this.maxLength = maxLength;
	}

	public String generate() {
		int length = (int) (minLength + (maxLength - minLength) * Math.random());
		
		StringBuilder builder = new StringBuilder();
		for (int index = 0; index < length; index++) {
			builder.append(ALLOWED_CHARACTERS.charAt((int) (ALLOWED_CHARACTERS.length() * Math.random())));
		}		
		return builder.toString();
	}

}
