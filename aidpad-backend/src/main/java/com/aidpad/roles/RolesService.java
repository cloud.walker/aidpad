package com.aidpad.roles;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.aidpad.api.model.RoleTO;
import com.aidpad.api.model.RoleTOLinks;
import com.aidpad.api.model.RoleTOList;
import com.aidpad.api.model.RoleValueTO;
import com.aidpad.errorhandling.exceptions.BadRequestException;
import com.aidpad.errorhandling.exceptions.NotFoundException;
import com.aidpad.persistence.entities.RoleEntity;
import com.aidpad.persistence.repositories.RolesRepository;

@Service
public class RolesService {

	@Value("${openapi.aidpad.base-path}")
	private String basePath;
	
	public final static String LIST_PATH = "/roles";
	public final static String SINGLE_PATH = "/role/";
	
	private final RolesRepository rolesRepository;
	private final RolesProvider rolesProvider;
	
	@Autowired
	public RolesService(final RolesRepository rolesRepository, final RolesProvider rolesProvider) {
		this.rolesRepository = rolesRepository;
		this.rolesProvider = rolesProvider;
	}
	
	public RoleTO getRoleByUri(final String uri) {
		String uuid = rolesProvider.getUuidFromUri(uri);
		RoleEntity role = rolesRepository.findByUuid(uuid);
		return rolesProvider.buildResource(role);
	}
	
	public RoleTOList getRolesList() {
		return rolesProvider.buildResourceList(rolesRepository.findAll(), null);
	}
	
	public void updateRole(String uuid, RoleTO body) {
		if (uuid == null || !uuid.equals(body.getRole().getUuid())) {
			throw new BadRequestException("Invalid request");
		}
		String name = body.getRole().getName();
		if (name.isEmpty()) {
			throw new BadRequestException("Name and uuid cannot be empty");
		}
		RoleEntity role = rolesRepository.findByUuid(uuid);
		if (role == null) {
			throw new NotFoundException("No such role", "role", uuid);
		}
		role.setName(name);
		role.setUuid(uuid);
		rolesRepository.save(role);
	}
	
	public RoleTO createRole(RoleTO roleTO) {
		String uuid = roleTO.getRole().getUuid();
		String name = roleTO.getRole().getName();
		if (uuid == null || uuid.isEmpty() || name == null || name.isEmpty()) {
			throw new BadRequestException("Name and uuid cannot be empty");
		}
		if (rolesRepository.findByUuid(uuid) != null) {
			throw new BadRequestException("uuid is already used");
		}
		RoleEntity role = new RoleEntity();
		role.setName(name);
		role.setUuid(uuid);
		role = rolesRepository.save(role);
		return rolesProvider.buildResource(role);
	}

	public RoleTO getRoleByUuid(String uuid) {
		RoleEntity role = rolesRepository.findByUuid(uuid);
		if (role == null) {
			throw new NotFoundException("No such role.", "Role", uuid);
		}
		return rolesProvider.buildResource(role);
	}	
}
