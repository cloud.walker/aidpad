package com.aidpad.roles;

public enum Roles {
	ADMINISTRATOR,
	FINANCIER,
	ASSESSOR,
	BENEFICIARY;
}
