package com.aidpad.roles;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.aidpad.ResourceProvider;
import com.aidpad.api.model.RoleTO;
import com.aidpad.api.model.RoleTOLinks;
import com.aidpad.api.model.RoleTOList;
import com.aidpad.api.model.RoleValueTO;
import com.aidpad.errorhandling.exceptions.InvalidUriException;
import com.aidpad.persistence.entities.RoleEntity;


@Component
public class RolesProvider implements ResourceProvider<RoleTOList, RoleTO, RoleEntity> {

	private String basePath;
	
	public final static String LIST_PATH = "/roles";
	public final static String SINGLE_RESOURCE_PATH = "/role/";

	public RolesProvider(@Value("${openapi.aidpad.base-path}") String basePath) {
		this.basePath = basePath;
	}
	
	@Override
	public RoleTO buildResource(RoleEntity roleEntity) {
		RoleTO roleTO = new RoleTO();
		RoleValueTO roleBody = new RoleValueTO();
		roleBody.setName(roleEntity.getName());
		roleBody.setUuid(roleEntity.getUuid());
		roleTO.setRole(roleBody);
		
		String selfUrl = getTOUri(roleEntity.getUuid());
		
		roleTO.setLinks(new RoleTOLinks());
		roleTO.getLinks().setSelf(selfUrl);
		return roleTO;
	}

	@Override
	public RoleTOList buildResourceList(List<RoleEntity> roles, Map<String, String> queryParameters) {
		List<RoleTO> allRoles = new ArrayList<>();
		roles.forEach(roleEntity -> {
			allRoles.add(buildResource(roleEntity));
		});
		
		RoleTOList list = new RoleTOList();
		list.setRoles(allRoles);
		list.setLinks(new RoleTOLinks());
		list.getLinks().setSelf(getTOListUri(null));
		return list;
	}

	@Override
	public String getTOUri(String roleUuid) {
		return basePath + SINGLE_RESOURCE_PATH + roleUuid; 
	}
	
	@Override
	public String getTOListUri(Map<String, String> queryParameters) {
		return basePath + LIST_PATH; 
	}

	@Override
	public String getUuidFromUri(String uri) {
		if (!uri.startsWith(basePath + SINGLE_RESOURCE_PATH)) {
			throw new InvalidUriException(uri, RoleTO.class);
		}
		String withoutPath = uri.substring((basePath + SINGLE_RESOURCE_PATH).length());
		if (withoutPath.indexOf('?') >= 0) {
			withoutPath = withoutPath.substring(0, withoutPath.indexOf('?'));
		}
		try {
			UUID.fromString(withoutPath);
		} catch(IllegalArgumentException ex) {
			throw new InvalidUriException(uri, RoleTO.class, ex);
		}
		return withoutPath;
	}
}
