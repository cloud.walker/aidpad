package com.aidpad.roles;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.NativeWebRequest;

import com.aidpad.api.RoleApiDelegate;
import com.aidpad.api.RolesApiDelegate;
import com.aidpad.api.model.RoleTO;
import com.aidpad.api.model.RoleTOList;

@Component
public class RolesApiImpl implements RoleApiDelegate, RolesApiDelegate {

	private RolesService rolesService;
	
	@Autowired
	public RolesApiImpl(final RolesService rolesService) {
		this.rolesService = rolesService;
	}
	
	@Override
	public ResponseEntity<RoleTOList> getRoles() {
		RoleTOList list = rolesService.getRolesList();
		
		return new ResponseEntity<>(list, HttpStatus.OK);
	}
	
	@Override
	public ResponseEntity<Void> updateRole(String uuid, RoleTO body) {
		rolesService.updateRole(uuid, body);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@Override
	public ResponseEntity<RoleTO> createRole(RoleTO body) {
		return new ResponseEntity<>(rolesService.createRole(body), HttpStatus.OK);
	}
	
	@Override
	public ResponseEntity<RoleTO> getRole(String uuid) {
		RoleTO roleTO = rolesService.getRoleByUuid(uuid);
		return new ResponseEntity<>(roleTO, HttpStatus.OK);
	}

	@Override
	public Optional<NativeWebRequest> getRequest() {
		return Optional.empty();
	}
}
