package com.aidpad;

import java.util.List;
import java.util.Map;

/**
 * <p>Responsible for converting entities to transfer objects (TOs), building URLs corresponding to entities and anything related to URL parsing.</p>
 * 
 * <p>When a service needs to include in its own transfer objects resources of another type - like users needing
 * to include roles - the "another type" resource provider should be injected, not the "another type service.</p> 
 * 
 * @author Florin Jurcovici
 *
 * @param <TL> transfer object list type 
 * @param <T> transfer object single resource type
 * @param <E> entity type that gets converted
 */
public interface ResourceProvider<TL, T, E> {
	T buildResource(E entity) ;
	TL buildResourceList(List<E> entity, Map<String, String> queryParameters);
	String getTOUri(String entityUuid);
	String getTOListUri(Map<String, String> queryParameters);
	String getUuidFromUri(String uri);
}
