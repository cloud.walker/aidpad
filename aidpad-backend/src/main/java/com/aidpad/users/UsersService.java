package com.aidpad.users;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.aidpad.api.model.UserTO;
import com.aidpad.api.model.UserTOList;
import com.aidpad.errorhandling.exceptions.BadRequestException;
import com.aidpad.errorhandling.exceptions.NotFoundException;
import com.aidpad.persistence.entities.RoleEntity;
import com.aidpad.persistence.entities.UserEntity;
import com.aidpad.persistence.entities.UserRoleEntity;
import com.aidpad.persistence.repositories.RolesRepository;
import com.aidpad.persistence.repositories.UserRolesRepository;
import com.aidpad.persistence.repositories.UsersRepository;
import com.aidpad.roles.RolesProvider;

@Service
public class UsersService {

	public static final String INVALID_UNKNOWN_USER_TYPE = "Unknown user type: ";
	public static final String INVALID_UNKNOWN_AUTH_TYPE = "Unknown authentication type: ";
	public static final String INVALID_EMPTY_LOGIN_NAME = "Login name can not be empty.";
	public static final String INVALID_EMPTY_AUTH_TYPE = "Authentication type name can not be empty.";
	public static final String INVALID_EMPTY_USER_TYPE = "User type name can not be empty.";
	
	private final RolesProvider rolesProvider;
	private final UsersProvider usersProvider;
	private final UsersRepository usersRepository;
	private final RolesRepository rolesRepository;
	private final UserRolesRepository userRolesRepository;

	private final Map<String, UserDetailsService> detailsServices; 
	
	@Value("${query.list.maxSize:500}")
	public int maxListSize;

	@Autowired
	public UsersService(final RolesProvider rolesProvider, final UsersProvider usersProvider, 
			final UsersRepository usersRepository, final RolesRepository rolesRepository, 
			final UserRolesRepository userRolesRepository, final GroupDetailsService groupDetailsService,
			final OrganizationDetailsService organizationDetailsService, final PersonDetailsService personDetailsService) {
		super();
		this.rolesProvider = rolesProvider;
		this.usersProvider = usersProvider;
		this.usersRepository = usersRepository;
		this.rolesRepository = rolesRepository;
		this.userRolesRepository = userRolesRepository;
		
		Map<String, UserDetailsService> tmp = new HashMap<>();
		tmp.put(UserType.GROUP.name(), groupDetailsService);
		tmp.put(UserType.ORGANISATION.name(), organizationDetailsService);
		tmp.put(UserType.PERSON.name(), personDetailsService);
		
		detailsServices = Collections.unmodifiableMap(tmp);
	}
	
	@Transactional
	public UserTO createUser(UserTO toCreate) {
		UserEntity userEntity = usersRepository.findByUuid(toCreate.getUser().getUuid());
		if (userEntity != null) {
			throw new BadRequestException("Entity already exists.");
		}
		validateUserTO(toCreate);
		userEntity = new UserEntity(
				toCreate.getUser().getUuid(), 
				toCreate.getUser().getAuthType(), 
				toCreate.getUser().getUserType(), 
				toCreate.getUser().getLoginName(), 
				toCreate.getUser().getEnabled(), 
				toCreate.getUser().getMustChangePassword());
		userEntity = usersRepository.saveAndFlush(userEntity);
		
		detailsServices.get(userEntity.getUserType()).createDetails(toCreate, userEntity);
		
		updateUserRoles(toCreate, userEntity);
		
		return usersProvider.buildResource(usersRepository.findByUuid(userEntity.getUuid()));
	}

	@Transactional
	public UserTOList getUsersList(List<String> authTypes, List<String> userTypes, List<String> roleUris, String startUuid,
			Long count, Boolean enabled, Boolean mustChangePassword) throws UnsupportedEncodingException {
		
		final int recordsToIncludeInPage = recordsToRetrieve(count);
		final List<Long> roleIds = usersProvider.roleUrlsToIds(roleUris);
		List<UserEntity> users = usersRepository.findPaged(startUuid, recordsToIncludeInPage, roleIds, 
				userTypes == null || userTypes.isEmpty() ? null : userTypes, 
				authTypes == null || authTypes.isEmpty() ? null : authTypes, 
				enabled, mustChangePassword);
		
		Map<String, String> queryParameters = buildQueryParameters(authTypes, userTypes, roleUris, startUuid, count, enabled,
				mustChangePassword);
		
		return usersProvider.buildResourceList(users, queryParameters);
	}

	@Transactional
	public UserTO getUserByUuid(String uuid) {
		UserEntity userEntity = usersRepository.findByUuid(uuid);
		if (userEntity == null) {
			throw new NotFoundException("Not found." , "user", uuid);
		}
		return usersProvider.buildResource(userEntity);
	}

	@Transactional
	public void updateUser(String uuid, UserTO toUpdate) {
		if (uuid == null || !uuid.equals(toUpdate.getUser().getUuid())) {
			throw new BadRequestException("Invalid request");
		}
		validateUserTO(toUpdate);
		UserEntity userEntity = usersRepository.findByUuid(uuid);
		
		if (userEntity.getUserType() == null || !userEntity.getUserType().equals(toUpdate.getUser().getUserType())) {
			throw new BadRequestException("Once created, the user type cannot be changed.");
		}
		
		// never touch password-related stuff from here, this should be done from auth endpoints only
		userEntity.setAuthType(toUpdate.getUser().getAuthType());
		userEntity.setEnabled(toUpdate.getUser().getEnabled());
		userEntity.setLoginName(toUpdate.getUser().getLoginName());
		userEntity.setUserType(toUpdate.getUser().getUserType());
		userEntity = usersRepository.saveAndFlush(userEntity);
		
		detailsServices.get(userEntity.getUserType()).updateDetails(toUpdate, userEntity);
		updateUserRoles(toUpdate, userEntity);
	}

	@Transactional
	public UserTO getUserByUri(String uri) {
		String uuid = usersProvider.getUuidFromUri(uri);
		return usersProvider.buildResource(usersRepository.findByUuid(uuid));
	}
	
	private void validateUserTO(UserTO toUpdate) {
		List<String> validationErrors = new ArrayList<>();
		validateLoginName(toUpdate, validationErrors);
		validateAuthorizationType(toUpdate, validationErrors);
		validateUserType(toUpdate, validationErrors);
		
		if (toUpdate.getUser().getUserType() != null) {
			detailsServices.get(toUpdate.getUser().getUserType()).validateDetails(toUpdate, validationErrors);
		}
		
		if (validationErrors.size() > 0) {
			throw new BadRequestException(validationErrors.stream().collect(Collectors.joining(" ")));
		}
	}

	private void validateUserType(UserTO toUpdate, List<String> validationErrors) {
		if (toUpdate.getUser().getUserType() == null || toUpdate.getUser().getUserType().isEmpty()) {
			validationErrors.add(INVALID_EMPTY_USER_TYPE);
		} else {
			try {
				UserType.valueOf(toUpdate.getUser().getUserType());
			} catch (Exception ex) {
				validationErrors.add(INVALID_UNKNOWN_USER_TYPE + toUpdate.getUser().getUserType());
			}
			
		}
	}

	private void validateAuthorizationType(UserTO toUpdate, List<String> validationErrors) {
		if (toUpdate.getUser().getAuthType() == null || toUpdate.getUser().getAuthType().isEmpty()) {
			validationErrors.add(INVALID_EMPTY_AUTH_TYPE);
		} else {
			try {
				AuthenticationType.valueOf(toUpdate.getUser().getAuthType());
			} catch (Exception ex) {
				validationErrors.add(INVALID_UNKNOWN_AUTH_TYPE + toUpdate.getUser().getAuthType());
			}
		}
	}

	private void validateLoginName(UserTO toUpdate, List<String> validationErrors) {
		if (toUpdate.getUser().getLoginName() == null || toUpdate.getUser().getLoginName().isEmpty()) {
			validationErrors.add(INVALID_EMPTY_LOGIN_NAME);
		}
	}
	
	private int recordsToRetrieve(Long count) {
		int countToRetrieve = maxListSize + 1;
		if (count != null && count > 0) {
			countToRetrieve = (int) Math.min(1 + count, countToRetrieve);
		}
		return countToRetrieve;
	}

	private Map<String, String> buildQueryParameters(List<String> authtypes, List<String> usertypes, List<String> roles,
			String start, Long count, Boolean enabled, Boolean mustChangePassword) throws UnsupportedEncodingException {
		Map<String, String> queryParameters = new HashMap<>();
		if (start != null && !start.isEmpty()) {
			queryParameters.put(UsersProvider.QUERY_PARAM_START_UUID, start);
		}
		if (count != null && count > 0) {
			queryParameters.put(UsersProvider.QUERY_PARAM_COUNT, count.toString());
		}
		if (roles != null && !roles.isEmpty()) {
			// only roles need encoding, since they are actual URLs
			queryParameters.put(UsersProvider.QUERY_PARAM_AUTH_TYPES, URLEncoder.encode(roles.stream().collect(Collectors.joining(",")), "UTF-8"));
		}
		if (authtypes != null && !authtypes.isEmpty()) {
			queryParameters.put(UsersProvider.QUERY_PARAM_AUTH_TYPES, authtypes.stream().collect(Collectors.joining(",")));
		}
		if (usertypes != null && !usertypes.isEmpty()) {
			queryParameters.put(UsersProvider.QUERY_PARAM_USER_TYPES, usertypes.stream().collect(Collectors.joining(",")));
		}
		if (enabled != null) {
			queryParameters.put(UsersProvider.QUERY_PARAM_ENABLED, enabled.toString());
		}
		if (mustChangePassword != null) {
			queryParameters.put(UsersProvider.QUERY_PARAM_ENABLED, mustChangePassword.toString());
		}
		return queryParameters;
	}

	private void updateUserRoles(UserTO userTO, UserEntity userEntity) {
		// simplest strategy: delete all then create all
		// acceptable for a small number of roles overall and few user management operations over time
		if (userEntity != null && userEntity.getRoles() != null && !userEntity.getRoles().isEmpty()) {
			for (UserRoleEntity userRoleEntity : userEntity.getRoles()) {
				userRolesRepository.delete(userRoleEntity);
			}
		}
		
		if (userTO.getLinks() != null && userTO.getLinks().getRoles() != null) {
			List<String> roleUris = userTO.getLinks().getRoles();
			for (String roleUri : roleUris) {
				RoleEntity roleEntity = rolesRepository.findByUuid(rolesProvider.getUuidFromUri(roleUri));
				UserRoleEntity userRoleEntity = new UserRoleEntity(roleEntity, userEntity);
				userRolesRepository.save(userRoleEntity);
			}
		}
	}	
}
