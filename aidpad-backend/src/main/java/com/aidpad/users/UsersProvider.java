package com.aidpad.users;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.aidpad.ResourceProvider;
import com.aidpad.api.model.GroupDetailsTO;
import com.aidpad.api.model.GroupDetailsTOMembers;
import com.aidpad.api.model.OrganizationDetailsTO;
import com.aidpad.api.model.PersonDetailsTO;
import com.aidpad.api.model.UserTO;
import com.aidpad.api.model.UserTOLinks;
import com.aidpad.api.model.UserTOList;
import com.aidpad.api.model.UserTOListLinks;
import com.aidpad.api.model.UserValueTO;
import com.aidpad.errorhandling.exceptions.InvalidUriException;
import com.aidpad.persistence.entities.GroupMemberDetailsEntity;
import com.aidpad.persistence.entities.OrganizationDetailsEntity;
import com.aidpad.persistence.entities.PersonDetailsEntity;
import com.aidpad.persistence.entities.RoleEntity;
import com.aidpad.persistence.entities.UserEntity;
import com.aidpad.persistence.entities.UserRoleEntity;
import com.aidpad.persistence.repositories.RolesRepository;
import com.aidpad.persistence.repositories.UsersRepository;
import com.aidpad.roles.RolesProvider;

@Component
public class UsersProvider implements ResourceProvider<UserTOList, UserTO, UserEntity> {

	private String basePath;

	// These must be the same as the ones used in the api definition
	public static final String QUERY_PARAM_START_UUID = "start";
	public static final String QUERY_PARAM_ROLE_IDS = "roles";
	public static final String QUERY_PARAM_USER_TYPES = "usertypes";
	public static final String QUERY_PARAM_AUTH_TYPES = "authtypes";
	public static final String QUERY_PARAM_ENABLED = "enabled";
	public static final String QUERY_PARAM_MUST_CHANGE_PWD = "mustchangepassword";
	public static final String QUERY_PARAM_COUNT = "count";

	public final static String LIST_PATH = "/users";
	public final static String SINGLE_RESOURCE_PATH = "/user/";

	private final RolesProvider rolesProvider;

	private final UsersRepository usersRepository;
	
	private final RolesRepository rolesRepository;

	@Autowired
	public UsersProvider(@Value("${openapi.aidpad.base-path}") String basePath, final RolesProvider rolesProvider,
			final UsersRepository usersRepository, final RolesRepository rolesRepository) {
		this.basePath = basePath;
		this.rolesProvider = rolesProvider;
		this.usersRepository = usersRepository;
		this.rolesRepository = rolesRepository;
	}

	@Override
	public UserTO buildResource(UserEntity entity) {
		UserTO userTO = new UserTO();
		userTO.setUser(new UserValueTO());

		userTO.getUser().setAuthType(entity.getAuthType());
		userTO.getUser().setEnabled(entity.isEnabled());
		userTO.getUser().setLoginName(entity.getLoginName());
		userTO.getUser().setMustChangePassword(entity.isMustChangePassword());
		userTO.getUser().setName(entity.getLoginName());
		userTO.getUser().setPasswordHash("");
		userTO.getUser().setUserType(entity.getUserType());
		userTO.getUser().setUuid(entity.getUuid());

		userTO.setLinks(new UserTOLinks());
		userTO.getLinks().setSelf(getTOUri(entity.getUuid()));
		userTO.getLinks().setRoles(new ArrayList<String>());

		if (UserType.ORGANISATION.name().equals(entity.getUserType())) {
			addOrganizationDetails(entity, userTO);
		} else if (UserType.GROUP.name().equals(entity.getUserType())) {
			addGroupMembersDetails(entity, userTO);
		} else if (UserType.PERSON.name().equals(entity.getUserType())) {
			addPersonDetails(entity, userTO);
		}
		
		if (entity.getRoles() != null) {
			for (UserRoleEntity userRoleEntity : entity.getRoles()) {
				userTO.getLinks().getRoles().add(rolesProvider.getTOUri(userRoleEntity.getRole().getUuid()));
			}
		}
		return userTO;
	}

	private void addGroupMembersDetails(UserEntity entity, UserTO userTO) {
		List<GroupDetailsTOMembers> groupMembersDetailsTO = new ArrayList<>();
		GroupDetailsTO groupDetailsTO = new GroupDetailsTO();
		groupDetailsTO.setMembers(groupMembersDetailsTO);
		userTO.getUser().setGroupDetails(groupDetailsTO);
		if (entity.getGroupMembers() != null && !entity.getGroupMembers().isEmpty()) {
			for (GroupMemberDetailsEntity groupMemberDetailsEntity : entity.getGroupMembers()) {
				GroupDetailsTOMembers groupMemberDetailsTO = new GroupDetailsTOMembers();
				groupMemberDetailsTO.setEmail(groupMemberDetailsEntity.getEmail());
				groupMemberDetailsTO.setName(groupMemberDetailsEntity.getPersonName());
				groupMemberDetailsTO.setPhoneNumber(groupMemberDetailsEntity.getPhoneNumber());
				groupMembersDetailsTO.add(groupMemberDetailsTO);
			}
		}
	}

	private void addPersonDetails(UserEntity entity, UserTO userTO) {
		PersonDetailsTO personDetailsTO = new PersonDetailsTO();
		// assumes set has always exactly one element - quirk caused by limitations in hibernate tools
		// is also defensive - validation should not be done here
		if (entity.getPersonDetails().size() > 0) {
			PersonDetailsEntity personDetailsEntity = entity.getPersonDetails().iterator().next();
			personDetailsTO.setEmail(personDetailsEntity.getEmail());
			personDetailsTO.setName(personDetailsEntity.getPersonName());
		}
		userTO.getUser().setPersonDetails(personDetailsTO);
	}

	private void addOrganizationDetails(UserEntity entity, UserTO userTO) {
		OrganizationDetailsTO detailsTO = new OrganizationDetailsTO();
		// assumes set has always exactly one element - quirk caused by limitations in hibernate tools
		// is also defensive - validation should not be done here
		if (entity.getOrganizationDetails().size() > 0) {
			OrganizationDetailsEntity detailsEntity = entity.getOrganizationDetails().iterator().next();
			detailsTO.setContactEmail(detailsEntity.getContactEmail());
			detailsTO.setFacebookPage(detailsEntity.getFacebookPage());
			detailsTO.setFiscalCode(detailsEntity.getFiscalIdentificationCode());
			detailsTO.setName(detailsEntity.getName());
			detailsTO.setWebSite(detailsEntity.getWebSite());
		}
		userTO.getUser().setOrganizationDetails(detailsTO);
	}

	@Override
	public UserTOList buildResourceList(List<UserEntity> userEntities, Map<String, String> queryParameters) {
		UserTOList usersList = new UserTOList();
		usersList.setUsers(new ArrayList<>());
		userEntities.forEach(userEntity -> {
			usersList.getUsers().add(buildResource(userEntity));
		});
		updateLinks(usersList, userEntities, queryParameters);
		return usersList;
	}

	@Override
	public String getTOUri(String userUuid) {
		return basePath + SINGLE_RESOURCE_PATH + userUuid;
	}

	@Override
	public String getTOListUri(Map<String, String> queryParameters) {
		String queryPart = "";
		if (queryParameters != null && !queryParameters.isEmpty()) {
			queryPart = "?" + queryParameters.entrySet().stream().map(entry -> entry.getKey() + "=" + entry.getValue())
					.collect(Collectors.joining("&"));
		}
		return basePath + LIST_PATH + queryPart;
	}

	@Override
	public String getUuidFromUri(String uri) {
		if (!uri.startsWith(basePath + SINGLE_RESOURCE_PATH)) {
			throw new InvalidUriException(uri, UserTO.class);
		}
		String withoutPath = uri.substring((basePath + SINGLE_RESOURCE_PATH).length());
		if (withoutPath.indexOf('?') >= 0) {
			withoutPath = withoutPath.substring(0, withoutPath.indexOf('?'));
		}
		try {
			UUID.fromString(withoutPath);
		} catch (IllegalArgumentException ex) {
			throw new InvalidUriException(uri, UserTO.class, ex);
		}
		return withoutPath;
	}

	private void updateLinks(UserTOList usersList, List<UserEntity> userEntities, Map<String, String> queryParameters) {
		usersList.setLinks(new UserTOListLinks());
		usersList.getLinks().setSelf(getTOListUri(queryParameters));
		
		final List<Long> roleIds = queryParameters == null ? null : extractRoleIds(queryParameters.get(QUERY_PARAM_ROLE_IDS));
		final List<String> authTypes = queryParameters == null ? null : extractList(queryParameters.get(QUERY_PARAM_AUTH_TYPES));
		final List<String> userTypes = queryParameters == null ? null : extractList(queryParameters.get(QUERY_PARAM_USER_TYPES));
		final Boolean enabled = queryParameters == null ? null : extractBoolean(queryParameters.get(QUERY_PARAM_ENABLED));
		final Boolean mustChangePassword = queryParameters == null ? null : extractBoolean(queryParameters.get(QUERY_PARAM_MUST_CHANGE_PWD));
		final String startUuid = queryParameters == null ? null : queryParameters.get(QUERY_PARAM_START_UUID);
		
		List<String> prevPageUuids = usersRepository.findUidsReversed(
				startUuid, 
				userEntities.size() + 1, 
				roleIds,
				userTypes,
				authTypes,
				enabled,
				mustChangePassword);
		
		if (queryParameters == null) {
			queryParameters = new HashMap<>();
		}
		
		if (prevPageUuids != null && !prevPageUuids.isEmpty()) {
			queryParameters.put(QUERY_PARAM_START_UUID, prevPageUuids.get(prevPageUuids.size() - 1));
			usersList.getLinks().setPrev(getTOListUri(queryParameters));
		}
		
		String nextUuid = usersRepository.findNextUuid(
				userEntities == null || userEntities.isEmpty() ? null : userEntities.get(userEntities.size() - 1).getUuid(),
				roleIds,
				userTypes,
				authTypes,
				enabled,
				mustChangePassword);
		if (nextUuid != null && !nextUuid.isEmpty()) {
			queryParameters.put(QUERY_PARAM_START_UUID, nextUuid);
			usersList.getLinks().setNext(getTOListUri(queryParameters));
		}
	}

	private List<Long> extractRoleIds(String roleUrls) {
		List<Long> roleIds = null;
		if (roleUrls != null && !roleUrls.isEmpty()) {
			roleIds = roleUrlsToIds(Arrays.asList(roleUrls.split(",")));
		}
		return roleIds;
	}
	
	private List<String> extractList(String concatenated) {
		if (concatenated == null || concatenated.isEmpty()) {
			return null;
		}
		return Arrays.asList(concatenated.split("."));
	}
	
	private Boolean extractBoolean(String bool) {
		if (bool == null || bool.isEmpty()) {
			return null;
		}
		return Boolean.parseBoolean(bool);
	}
	
	List<Long> roleUrlsToIds(List<String> urls) {
		if (urls != null && !urls.isEmpty()) {
			return null;
		}
		List<RoleEntity> allRoles = rolesRepository.findAll();
		List<String> selectedRoleUuids = urls.stream().map(rolesProvider::getUuidFromUri).map(roleUrl -> {
			try {
				return URLDecoder.decode(roleUrl, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				// best effort attempt at decoding - return un-decoded if decoding fails
				return roleUrl;
			}
		}).collect(Collectors.toList());
		List<Long> selectedRoleIds = allRoles
				.stream()
				.filter(roleEntity -> selectedRoleUuids.contains(roleEntity.getUuid()))
				.map(RoleEntity::getId)
				.collect(Collectors.toList());
		return selectedRoleIds;
	}

}
