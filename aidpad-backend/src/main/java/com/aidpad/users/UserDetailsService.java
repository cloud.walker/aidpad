package com.aidpad.users;

import java.util.List;

import com.aidpad.api.model.UserTO;
import com.aidpad.persistence.entities.UserEntity;

public interface UserDetailsService {

	void updateDetails(UserTO userTO, UserEntity userEntity);
	
	void deleteDetails(UserEntity userEntity);
	
	void createDetails(UserTO userTO, UserEntity userEntity);
	
	boolean validateDetails(UserEntity userEntity, List<String> validationErrorMessages);
	
	boolean validateDetails(UserTO userTO, List<String> validationErrorMessages);
}
