package com.aidpad.users;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.NativeWebRequest;

import com.aidpad.api.UserApiDelegate;
import com.aidpad.api.UsersApiDelegate;
import com.aidpad.api.model.UserTO;
import com.aidpad.api.model.UserTOList;
import com.aidpad.errorhandling.exceptions.BadRequestException;

@Transactional
@Component
public class UsersApiImpl implements UsersApiDelegate, UserApiDelegate {

	private final UsersService usersService;
	
	@Autowired
	public UsersApiImpl(final UsersService usersService) {
		this.usersService = usersService;
	}

	@Override
	public ResponseEntity<UserTOList> getUsers(List<String> authtypes, List<String> usertypes, List<String> roles,
			String start, Long count, Boolean enabled, Boolean mustchangepassword) {
		try {
			return new ResponseEntity<>(usersService.getUsersList(authtypes, usertypes, roles, start, count, enabled, mustchangepassword), HttpStatus.OK);
		} catch (UnsupportedEncodingException e) {
			throw new BadRequestException("Could not parse parameters.");
		}
	}


	@Override
	public ResponseEntity<UserTO> createUser(UserTO userTO) {
		return new ResponseEntity<>(usersService.createUser(userTO), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<UserTO> getUser(String uuid) {
		return new ResponseEntity<>(usersService.getUserByUuid(uuid), HttpStatus.OK);	
	}

	@Override
	public ResponseEntity<Void> updateUser(String uuid, UserTO userTO) {
		usersService.updateUser(uuid, userTO);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@Override
	public Optional<NativeWebRequest> getRequest() {
		return Optional.empty();
	}
}
