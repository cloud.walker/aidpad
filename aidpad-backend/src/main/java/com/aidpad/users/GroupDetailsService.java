package com.aidpad.users;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aidpad.api.model.GroupDetailsTOMembers;
import com.aidpad.api.model.UserTO;
import com.aidpad.persistence.entities.GroupMemberDetailsEntity;
import com.aidpad.persistence.entities.UserEntity;
import com.aidpad.persistence.repositories.GroupMembersRepository;

@Service
public class GroupDetailsService implements UserDetailsService {

	private final GroupMembersRepository groupMembersRepository;

	public static final String INVALID_GROUP_MEMBER_HAS_NO_NAME = "Every member of a group needs to have a name set.";
	public static final String INVALID_GROUP_TOO_SMALL = "A group must contain at least three members.";
	public static final String INVALID_EMPTY_GROUP_MEMBER_DETAILS = "Must provide group member details for users of type group.";
	public static final String INVALID_GROUP_TOO_FEW_CONTACTS = "At least three members of a group must have contact information (email or phone number).";
	public static final String INVALID_EMPTY_GROUP_DETAILS = "Must provide group details for users of type group.";
	public static final String INVALID_FILLED_GROUP_DETAILS = "User of type other than group cannot have group details.";
	
	public static final int MIN_GROUP_MEMBERS = 3;
	public static final int MIN_CONTACT_INFO = 3;
	
	@Autowired
	public GroupDetailsService(final GroupMembersRepository groupMembersRepository) {
		this.groupMembersRepository = groupMembersRepository;
	}

	@Override
	public void updateDetails(UserTO userTO, UserEntity userEntity) {
		deleteDetails(userEntity);
		createDetails(userTO, userEntity);
	}

	@Override
	public void deleteDetails(UserEntity userEntity) {
		groupMembersRepository.deleteAll(userEntity.getGroupMembers());
	}

	@Override
	public void createDetails(UserTO userTO, UserEntity userEntity) {
		userEntity.getGroupMembers().clear();
		for (GroupDetailsTOMembers memberTO : userTO.getUser().getGroupDetails().getMembers()) {
			GroupMemberDetailsEntity newMemberEntity = new GroupMemberDetailsEntity(
					userEntity, memberTO.getName(), memberTO.getEmail(), memberTO.getPhoneNumber());
			userEntity.getGroupMembers().add(groupMembersRepository.save(newMemberEntity));
		}
	}

	@Override
	public boolean validateDetails(UserEntity userEntity, List<String> validationErrorMessages) {
		if (UserType.GROUP.name().equals(userEntity.getUserType())) {
			if (userEntity.getGroupMembers() == null || userEntity.getGroupMembers().isEmpty()) {
				validationErrorMessages.add(INVALID_EMPTY_GROUP_MEMBER_DETAILS);
			} else {
				if (userEntity.getGroupMembers().size() < MIN_GROUP_MEMBERS) {
					validationErrorMessages.add(INVALID_GROUP_TOO_SMALL);
				}
				int contactInfoCount = 0;
				boolean hasMemberWithNoName = false;
				for (GroupMemberDetailsEntity member : userEntity.getGroupMembers()) {
					if ((member.getPhoneNumber() != null && !member.getPhoneNumber().isEmpty())
						|| (member.getEmail() != null && !member.getEmail().isEmpty())) {
						contactInfoCount++;
					}
					if (member.getPersonName() == null || member.getPersonName().isEmpty()) {
						hasMemberWithNoName = true;
					}
				}
				if (contactInfoCount < MIN_CONTACT_INFO) {
					validationErrorMessages.add(INVALID_GROUP_TOO_FEW_CONTACTS);
				}
				if (hasMemberWithNoName) {
					validationErrorMessages.add(INVALID_GROUP_MEMBER_HAS_NO_NAME);
				}
			}
		} else {
			if (userEntity.getGroupMembers() != null && !userEntity.getGroupMembers().isEmpty()) {
				validationErrorMessages.add(INVALID_FILLED_GROUP_DETAILS);
			}
		}
		return validationErrorMessages.size() == 0;
	}

	@Override
	public boolean validateDetails(UserTO userTO, List<String> validationErrorMessages) {
		if (!UserType.GROUP.name().equals(userTO.getUser().getUserType())) {
			if (userTO.getUser().getGroupDetails() != null) {
				validationErrorMessages.add(INVALID_FILLED_GROUP_DETAILS);
			}
		} else {
			if (userTO.getUser().getGroupDetails() == null) {
				validationErrorMessages.add(INVALID_EMPTY_GROUP_MEMBER_DETAILS);
			} else if (userTO.getUser().getGroupDetails().getMembers() == null
				|| userTO.getUser().getGroupDetails().getMembers().isEmpty()) {
				validationErrorMessages.add(INVALID_EMPTY_GROUP_MEMBER_DETAILS);
			} else {
				if (userTO.getUser().getGroupDetails().getMembers().size() < MIN_GROUP_MEMBERS) {
					validationErrorMessages.add(INVALID_GROUP_TOO_SMALL);
				}
				int contactsCount = 0;
				boolean hasMemberWithNoName = false;
				for (GroupDetailsTOMembers member: userTO.getUser().getGroupDetails().getMembers()) {
					if ((member.getEmail() != null && !member.getEmail().isEmpty())
						|| (member.getPhoneNumber() != null && !member.getPhoneNumber().isEmpty())) {
						contactsCount++;
					}
					if (member.getName() == null || member.getName().isEmpty()) {
						hasMemberWithNoName = true;
					}
				}
				if (contactsCount < MIN_CONTACT_INFO) {
					validationErrorMessages.add(INVALID_GROUP_TOO_FEW_CONTACTS);
				}
				if (hasMemberWithNoName) {
					validationErrorMessages.add(INVALID_GROUP_MEMBER_HAS_NO_NAME);
				}
			}
		}
		return validationErrorMessages.size() == 0;
	}
	
}
