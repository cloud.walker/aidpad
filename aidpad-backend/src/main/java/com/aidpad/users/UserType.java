package com.aidpad.users;

public enum UserType {
	ORGANISATION,
	GROUP,
	PERSON;
}
