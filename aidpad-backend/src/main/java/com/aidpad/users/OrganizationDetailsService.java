package com.aidpad.users;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aidpad.api.model.OrganizationDetailsTO;
import com.aidpad.api.model.UserTO;
import com.aidpad.persistence.entities.OrganizationDetailsEntity;
import com.aidpad.persistence.entities.UserEntity;
import com.aidpad.persistence.repositories.OrganizationDetailsRepository;

@Service
public class OrganizationDetailsService implements UserDetailsService {

	private final OrganizationDetailsRepository organizationDetailsRepository;
	public static final String INVALID_ORGANIZATION_HAS_NO_CONTACT_EMAIL = "An organization needs a contact email.";
	public static final String INVALID_ORGANIZATION_HAS_NO_NAME = "An organization has to have a name.";
	public static final String INVALID_ORGANIZATION_HAS_NO_FISCAL_IDENTITY = "An organization needs a fiscal identification code.";
	public static final String INVALID_EMPTY_ORGANIZATION_DETAILS = "Must provide organization details for users of type organization."; 
	public static final String INVALID_FILLED_ORGANIZATION_DETAILS = "User of type other than organization cannot have organization details.";
	
	@Autowired
	public OrganizationDetailsService(final OrganizationDetailsRepository organizationDetailsRepository) {
		this.organizationDetailsRepository = organizationDetailsRepository;
	}

	@Override
	public void updateDetails(UserTO userTO, UserEntity userEntity) {
		deleteDetails(userEntity);
		createDetails(userTO, userEntity);
	}

	@Override
	public void deleteDetails(UserEntity userEntity) {
		organizationDetailsRepository.deleteAll(userEntity.getOrganizationDetails());
	}

	@Override
	public void createDetails(UserTO userTO, UserEntity userEntity) {
		OrganizationDetailsEntity organizationDetails = new OrganizationDetailsEntity(
				userEntity, 
				userTO.getUser().getOrganizationDetails().getName(),
				userTO.getUser().getOrganizationDetails().getFiscalCode(),
				userTO.getUser().getOrganizationDetails().getContactEmail(),
				userTO.getUser().getOrganizationDetails().getWebSite(),
				userTO.getUser().getOrganizationDetails().getFacebookPage());
		
		organizationDetails = organizationDetailsRepository.save(organizationDetails);
		
		userEntity.getOrganizationDetails().clear();
		userEntity.getOrganizationDetails().add(organizationDetails);
	}

	@Override
	public boolean validateDetails(UserEntity userEntity, List<String> validationErrorMessages) {
		if (UserType.ORGANISATION.name().equals(userEntity.getUserType())) {
			if (userEntity.getOrganizationDetails().size() == 0) {
				validationErrorMessages.add(INVALID_EMPTY_ORGANIZATION_DETAILS);
			} else {
				OrganizationDetailsEntity organizationDetails = userEntity.getOrganizationDetails().iterator().next();
				if (organizationDetails.getName() == null || organizationDetails.getName().isEmpty()) {
					validationErrorMessages.add(INVALID_ORGANIZATION_HAS_NO_NAME);
				}
				if (organizationDetails.getContactEmail() == null || organizationDetails.getContactEmail().isEmpty()) {
					validationErrorMessages.add(INVALID_ORGANIZATION_HAS_NO_CONTACT_EMAIL);
				}
				if (organizationDetails.getFiscalIdentificationCode() == null || organizationDetails.getFiscalIdentificationCode().isEmpty()) {
					validationErrorMessages.add(INVALID_ORGANIZATION_HAS_NO_FISCAL_IDENTITY);
				}
			}
			
		} else {
			if (userEntity.getOrganizationDetails().size() != 0) {
				validationErrorMessages.add(INVALID_FILLED_ORGANIZATION_DETAILS);
			}
		}
		return validationErrorMessages.size() == 0;
	}

	@Override
	public boolean validateDetails(UserTO userTO, List<String> validationErrorMessages) {
		if (!UserType.ORGANISATION.name().equals(userTO.getUser().getUserType())) {
			if (userTO.getUser().getOrganizationDetails() != null) {
				validationErrorMessages.add(INVALID_FILLED_ORGANIZATION_DETAILS);
			}
		} else {
			if (userTO.getUser().getOrganizationDetails() == null) {
				validationErrorMessages.add(INVALID_EMPTY_ORGANIZATION_DETAILS);
			} else {
				@Valid
				OrganizationDetailsTO organizationDetails = userTO.getUser().getOrganizationDetails();
				if (organizationDetails.getName() == null || organizationDetails.getName().isEmpty()) {
					validationErrorMessages.add(INVALID_ORGANIZATION_HAS_NO_NAME);
				}
				if (organizationDetails.getFiscalCode() == null || organizationDetails.getFiscalCode().isEmpty()) {
					validationErrorMessages.add(INVALID_ORGANIZATION_HAS_NO_FISCAL_IDENTITY);
				}
				if (organizationDetails.getContactEmail() == null || organizationDetails.getContactEmail().isEmpty()) {
					validationErrorMessages.add(INVALID_ORGANIZATION_HAS_NO_CONTACT_EMAIL);
				}
			}
		}
		return validationErrorMessages.size() == 0;
	}
	
}
