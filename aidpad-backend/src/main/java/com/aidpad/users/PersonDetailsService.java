package com.aidpad.users;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aidpad.api.model.UserTO;
import com.aidpad.persistence.entities.PersonDetailsEntity;
import com.aidpad.persistence.entities.UserEntity;
import com.aidpad.persistence.repositories.PersonDetailsRepository;

@Service
public class PersonDetailsService implements UserDetailsService {

	private final PersonDetailsRepository personDetailsRepository;
	public static final String INVALID_PERSON_HAS_NO_EMAIL = "A person has to have a contact email.";
	public static final String INVALID_PERSON_HAS_NO_NAME = "A person has to have a name.";
	public static final String INVALID_EMPTY_PERSON_DETAILS = "Must provide person details for users of type person."; 
	public static final String INVALID_FILLED_PERSON_DETAILS = "Person details should not be filled if user is not of type person."; 
	
	@Autowired
	public PersonDetailsService(final PersonDetailsRepository personDetailsRepository) {
		this.personDetailsRepository = personDetailsRepository;
	}

	@Override
	public void updateDetails(UserTO userTO, UserEntity userEntity) {
		deleteDetails(userEntity);
		createDetails(userTO, userEntity);
	}

	@Override
	public void deleteDetails(UserEntity userEntity) {
		personDetailsRepository.deleteAll(userEntity.getPersonDetails());
		userEntity.getPersonDetails().clear();
	}

	@Override
	public void createDetails(UserTO userTO, UserEntity userEntity) {
		PersonDetailsEntity personDetailsEntity = new PersonDetailsEntity(
				userEntity, 
				userTO.getUser().getPersonDetails().getName(),
				userTO.getUser().getPersonDetails().getEmail());
		
		personDetailsEntity = personDetailsRepository.save(personDetailsEntity);
		userEntity.getPersonDetails().clear();
		userEntity.getPersonDetails().add(personDetailsEntity);
	}

	@Override
	public boolean validateDetails(UserEntity userEntity, List<String> validationErrorMessages) {
		if (UserType.PERSON.name().equals(userEntity.getUserType())) {
			if (userEntity.getPersonDetails() == null || userEntity.getPersonDetails().isEmpty()) {
				validationErrorMessages.add(INVALID_EMPTY_PERSON_DETAILS);
			} else { 
				PersonDetailsEntity pesonDetails = userEntity.getPersonDetails().iterator().next();
				if (pesonDetails.getPersonName() == null || pesonDetails.getPersonName().isEmpty()) {
					validationErrorMessages.add(INVALID_PERSON_HAS_NO_NAME);
				}
				if (pesonDetails.getEmail() == null || pesonDetails.getEmail().isEmpty()) {
					validationErrorMessages.add(INVALID_PERSON_HAS_NO_EMAIL);
				}
			}
		} else {
			if (userEntity.getPersonDetails() != null && !userEntity.getPersonDetails().isEmpty()) {
				validationErrorMessages.add(INVALID_FILLED_PERSON_DETAILS);
			}
		}
		return validationErrorMessages.size() == 0;
	}

	@Override
	public boolean validateDetails(UserTO userTO, List<String> validationErrorMessages) {
		if (!UserType.PERSON.name().equals(userTO.getUser().getUserType())) {
			if (userTO.getUser().getPersonDetails() != null) {
				validationErrorMessages.add(INVALID_FILLED_PERSON_DETAILS);
			}
		} else {
			if (userTO.getUser().getPersonDetails() == null) {
				validationErrorMessages.add(INVALID_EMPTY_PERSON_DETAILS);
			} else {
				if (userTO.getUser().getPersonDetails().getName() == null || userTO.getUser().getPersonDetails().getName().isEmpty()) {
					validationErrorMessages.add(INVALID_PERSON_HAS_NO_NAME);
				}
				if (userTO.getUser().getPersonDetails().getEmail() == null || userTO.getUser().getPersonDetails().getEmail().isEmpty()) {
					validationErrorMessages.add(INVALID_PERSON_HAS_NO_EMAIL);
				}
			}
		}
		return validationErrorMessages.size() == 0;
	}
	
}
