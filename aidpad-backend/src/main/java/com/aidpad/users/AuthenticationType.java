package com.aidpad.users;

public enum AuthenticationType {
	PASSWORD,
	GOOGLE,
	FACEBOOK;
}
