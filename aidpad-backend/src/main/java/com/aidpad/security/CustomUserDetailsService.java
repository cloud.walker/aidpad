package com.aidpad.security;

import com.aidpad.errorhandling.exceptions.NotFoundException;
import com.aidpad.persistence.entities.UserEntity;
import com.aidpad.persistence.repositories.UsersRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CustomUserDetailsService implements UserDetailsService {

	@Autowired
	UsersRepository userRepository;

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
		UserEntity user = userRepository.findByLoginName(name);
		if (user == null)
			throw new NotFoundException("No such user", "user", name);

		return UserPrincipal.create(user);
	}

	@Transactional
	public UserDetails loadUserById(Long id) {
		UserEntity user = userRepository.findById(id)
				.orElseThrow(() -> new NotFoundException("User", "id", String.valueOf(id)));

		return UserPrincipal.create(user);
	}
}