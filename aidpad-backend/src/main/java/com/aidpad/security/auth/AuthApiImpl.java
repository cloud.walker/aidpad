package com.aidpad.security.auth;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.context.request.NativeWebRequest;

import com.aidpad.api.LoginApiDelegate;
import com.aidpad.api.RegisterApiDelegate;
import com.aidpad.api.model.LoginRequest;
import com.aidpad.api.model.RegisterRequest;
import com.aidpad.errorhandling.exceptions.AidpadAuthenticationException;
import com.aidpad.errorhandling.exceptions.BadRequestException;
import com.aidpad.errorhandling.exceptions.NotFoundException;
import com.aidpad.persistence.entities.RoleEntity;
import com.aidpad.persistence.entities.UserEntity;
import com.aidpad.persistence.entities.UserRoleEntity;
import com.aidpad.persistence.repositories.RolesRepository;
import com.aidpad.persistence.repositories.UserRolesRepository;
import com.aidpad.persistence.repositories.UsersRepository;
import com.aidpad.roles.RolesService;
import com.aidpad.security.TokenProvider;
import com.aidpad.users.AuthenticationType;

@Component
@Transactional
public class AuthApiImpl implements LoginApiDelegate, RegisterApiDelegate {

	@Autowired
	private AuthenticationManager authenticationManager;

	private final UsersRepository usersRepository;
	private final RolesRepository rolesRepository;
	private final RolesService rolesService;
	private final UserRolesRepository userRolesRepository;

	@Autowired
	public AuthApiImpl(final UsersRepository usersRepository, final RolesRepository rolesRepository,
			final RolesService rolesApiImpl, final UserRolesRepository userRolesRepository) {
		this.usersRepository = usersRepository;
		this.rolesRepository = rolesRepository;
		this.rolesService = rolesApiImpl;
		this.userRolesRepository = userRolesRepository;
	}

	@Autowired
	private TokenProvider tokenProvider;

	@Override
	public ResponseEntity<String> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

		UserEntity user = usersRepository.findByLoginName(loginRequest.getLoginName());
		if (user == null)
			throw new NotFoundException("No such user", "user", loginRequest.getLoginName());
		if (!user.isEnabled())
			throw new AidpadAuthenticationException("User is not enabled " + user.getLoginName());

		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getLoginName(), loginRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		String token = tokenProvider.createToken(authentication);
		return ResponseEntity.ok(token);
	}

	@Override
	public ResponseEntity<Void> registerUser(@Valid @RequestBody RegisterRequest registerRequest) {
		String uuid = registerRequest.getRequest().getUuid();
		String name = registerRequest.getRequest().getLoginName();
		Set<UserRoleEntity> userRoles = new HashSet<UserRoleEntity>();
		UserEntity user = new UserEntity();

		if (uuid == null || uuid.isEmpty() || name == null || name.isEmpty()) {
			throw new BadRequestException("Name and uuid cannot be empty");
		}
		if (usersRepository.findByUuid(uuid) != null) {
			throw new BadRequestException("uuid is already used");
		}

		UserEntity userEntity = usersRepository.findByLoginName(registerRequest.getRequest().getLoginName());
		if (userEntity != null) {
			throw new BadRequestException("Username address already in use.");
		}

		userEntity = new UserEntity(registerRequest.getRequest().getUuid(), AuthenticationType.PASSWORD.name(),
				registerRequest.getRequest().getUserType(), registerRequest.getRequest().getLoginName(), false, true);

		user = usersRepository.saveAndFlush(user);

		if (registerRequest.getLinks().getRoles() != null) {
			List<String> roleUris = registerRequest.getLinks().getRoles();
			for (String roleUri : roleUris) {
				RoleEntity roleEntity = rolesRepository
						.findByUuid(rolesService.getRoleByUri(roleUri).getRole().getUuid());
				UserRoleEntity userRoleEntity = new UserRoleEntity(roleEntity, userEntity);
				userRolesRepository.save(userRoleEntity);
			}
		} else {
			UserRoleEntity userRoleEntity = new UserRoleEntity(rolesRepository.findByName("BENEFICIARY"), user);
			userRolesRepository.save(userRoleEntity);

		}

		// send email

		 return new ResponseEntity<>(HttpStatus.OK);
	}

	@Override
	public Optional<NativeWebRequest> getRequest() {
		return LoginApiDelegate.super.getRequest();
	}

}
