package com.aidpad.security.oauth2;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.client.userinfo.DefaultOAuth2UserService;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aidpad.errorhandling.exceptions.OAuth2AuthenticationProcessingException;
import com.aidpad.persistence.entities.RoleEntity;
import com.aidpad.persistence.entities.UserEntity;
import com.aidpad.persistence.entities.UserRoleEntity;
import com.aidpad.persistence.repositories.UsersRepository;
import com.aidpad.security.UserPrincipal;
import com.aidpad.security.oauth2.user.OAuth2UserInfo;
import com.aidpad.security.oauth2.user.OAuth2UserInfoFactory;
import com.aidpad.users.AuthenticationType;

@Service
public class CustomOAuth2UserService extends DefaultOAuth2UserService {

    @Autowired
    private UsersRepository userRepository;

    @Override
    public OAuth2User loadUser(OAuth2UserRequest oAuth2UserRequest) throws OAuth2AuthenticationException {
        OAuth2User oAuth2User = super.loadUser(oAuth2UserRequest);

        try {
            return processOAuth2User(oAuth2UserRequest, oAuth2User);
        } catch (AuthenticationException ex) {
            throw ex;
        } catch (Exception ex) {
            // Throwing an instance of AuthenticationException will trigger the OAuth2AuthenticationFailureHandler
            throw new InternalAuthenticationServiceException(ex.getMessage(), ex.getCause());
        }
    }

    //pt partea de register si login cu google/facebook intra aici de debug-uit ce se intampla si cum putem adauga celelalte campuri
    private OAuth2User processOAuth2User(OAuth2UserRequest oAuth2UserRequest, OAuth2User oAuth2User) {
        OAuth2UserInfo oAuth2UserInfo = OAuth2UserInfoFactory.getOAuth2UserInfo(oAuth2UserRequest.getClientRegistration().getRegistrationId(), oAuth2User.getAttributes());
        if(StringUtils.isEmpty(oAuth2UserInfo.getEmail())) {
            throw new OAuth2AuthenticationProcessingException("Email not found from OAuth2 provider");
        }

        UserEntity user = userRepository.findByLoginName(oAuth2UserInfo.getEmail());
        if(user!=null) {
            if(!user.getAuthType().equals(AuthenticationType.valueOf(oAuth2UserRequest.getClientRegistration().getRegistrationId()))) {
                throw new OAuth2AuthenticationProcessingException("Looks like you're signed up with " +
                        user.getAuthType() + " account. Please use your " + user.getAuthType() +
                        " account to login.");
            }
            user = updateExistingUser(user, oAuth2UserInfo);
        } else {
            user = registerNewUser(oAuth2UserRequest, oAuth2UserInfo);
        }

        return UserPrincipal.create(user, oAuth2User.getAttributes());
    }

    private UserEntity registerNewUser(OAuth2UserRequest oAuth2UserRequest, OAuth2UserInfo oAuth2UserInfo) {
    	UserEntity user = new UserEntity();

        user.setAuthType(AuthenticationType.valueOf(oAuth2UserRequest.getClientRegistration().getRegistrationId()).toString());
        user.setLoginName(oAuth2UserInfo.getEmail());
        user.setUserType(oAuth2UserInfo.getType());
        RoleEntity role = new RoleEntity();
        UserRoleEntity userRoles = new UserRoleEntity();
        role.setName("BENEFICIARY");
       
        user.setEnabled(true);
        user.setMustChangePassword(false);
        Set<UserRoleEntity> us =new HashSet<UserRoleEntity>();
        us.add(userRoles);
        user.setRoles(us);
        return userRepository.save(user);
    }

    private UserEntity updateExistingUser(UserEntity existingUser, OAuth2UserInfo oAuth2UserInfo) {
    	existingUser.setLoginName(oAuth2UserInfo.getEmail());
        return userRepository.save(existingUser);
    }

}
