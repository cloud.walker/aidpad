package com.aidpad.errorhandling;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringEscapeUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.aidpad.errorhandling.exceptions.AidpadAuthenticationException;
import com.aidpad.errorhandling.exceptions.BadRequestException;
import com.aidpad.errorhandling.exceptions.NotAuthorizedException;
import com.aidpad.errorhandling.exceptions.NotFoundException;

@ControllerAdvice
public class GlobalControllerExceptionHandler {

	@ExceptionHandler(BadRequestException.class)
	public ResponseEntity<String> onValidationException(HttpServletRequest request, Exception ex) {
		String json = "{\"message\":\"" + StringEscapeUtils.escapeJavaScript(ex.getMessage()) + "\"}";
			return new ResponseEntity<>(json, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(NotAuthorizedException.class)
	public ResponseEntity<String> onAuthorizationException(HttpServletRequest request, Exception ex) {
		String json = "{\"message\":\"" + StringEscapeUtils.escapeJavaScript(ex.getMessage()) + "\"}";
			return new ResponseEntity<>(json, HttpStatus.FORBIDDEN);
	}
	
	@ExceptionHandler(NotFoundException.class)
	public ResponseEntity<String> onNotFoundException(HttpServletRequest request, Exception ex) {
		String json = "{\"message\":\"" + StringEscapeUtils.escapeJavaScript(ex.getMessage()) + "\"}";
		return new ResponseEntity<>(json, HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(AidpadAuthenticationException.class)
	public ResponseEntity<String> onAidpadAuthenticationException(HttpServletRequest request, Exception ex) {
		String json = "{\"message\":\"" + StringEscapeUtils.escapeJavaScript(ex.getMessage()) + "\"}";
		return new ResponseEntity<>(json, HttpStatus.INTERNAL_SERVER_ERROR);
	}
}

