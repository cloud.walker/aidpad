package com.aidpad.errorhandling.exceptions;

public class InvalidUriException extends RuntimeException {
	private static final long serialVersionUID = 6215811393138214109L;

	public <T> InvalidUriException(String uri, Class<?> type, Throwable cause) {
		super("URI " + uri + " is not a valid uri for resources of type " + type.getName() + ": " + cause.getMessage());
	}

	public <T> InvalidUriException(String uri, Class<?> type) {
		super("URI " + uri + " is not a valid uri for resources of type " + type.getName());
	}
}
