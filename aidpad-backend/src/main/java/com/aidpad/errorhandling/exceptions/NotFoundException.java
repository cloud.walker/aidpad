package com.aidpad.errorhandling.exceptions;

public class NotFoundException extends RuntimeException {

	private static final long serialVersionUID = 8083991378899733563L;

	public NotFoundException(Throwable cause, String resourceName, String uuid) {
		super(resourceName + " " + uuid + " not found: " + cause.getMessage());
	}

	public NotFoundException(String message, String resourceName, String uuid) {
		super(resourceName + " " + uuid + " not found: " + message);
	}
}
