package com.aidpad.errorhandling.exceptions;

public class BadRequestException extends RuntimeException {
	private static final long serialVersionUID = 6215811393138214109L;

	public <T> BadRequestException(Throwable cause) {
		super("Request failed. " + cause.getMessage());
	}

	public <T> BadRequestException(String message) {
		super("Request failed: " + message);
	}
}
