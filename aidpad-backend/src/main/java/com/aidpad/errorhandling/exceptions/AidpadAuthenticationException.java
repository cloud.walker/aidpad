package com.aidpad.errorhandling.exceptions;

public class AidpadAuthenticationException extends RuntimeException {

	private static final long serialVersionUID = -6744133163258330680L;

	public AidpadAuthenticationException(String msg, Throwable t) {
		super(msg, t);
	}

	public AidpadAuthenticationException(String msg) {
		super(msg);
	}
}
