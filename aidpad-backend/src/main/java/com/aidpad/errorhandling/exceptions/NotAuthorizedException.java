package com.aidpad.errorhandling.exceptions;

public class NotAuthorizedException extends RuntimeException {

	private static final long serialVersionUID = 8083991378899733563L;

	public NotAuthorizedException(Throwable cause, String url, String method) {
		super("Not authorized to " + method + " " + url + ": " + cause.getMessage());
	}

	public <T> NotAuthorizedException(String message, String url, String method) {
		super("Not authorized to " + method + " " + url + ": " + message);
	}
}
