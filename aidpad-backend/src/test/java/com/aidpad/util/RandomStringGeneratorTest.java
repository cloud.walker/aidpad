package com.aidpad.util;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.lessThanOrEqualTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.RepeatedTest;

class RandomStringGeneratorTest {

	private static final int TEST_MIN_LENGTH = 45;
	private static final int TEST_MAX_LENGTH = 55;
	
	private RandomStringGenerator generator = new RandomStringGenerator(TEST_MIN_LENGTH, TEST_MAX_LENGTH); 
	
	@RepeatedTest(100)
	void generatesWithinLengthRange() {
		String generated = generator.generate();
		assertThat(generated, notNullValue());
		assertThat(generated.length(), lessThanOrEqualTo(TEST_MAX_LENGTH));
		assertThat(generated.length(), greaterThanOrEqualTo(TEST_MIN_LENGTH));
	}
	
	@RepeatedTest(100)
	void doesNotGenerateManyRepeatedCharacters() {
		int repetitionsEncountered = 0;
		for (int counter = 0; counter < 1000; counter++) {
			String generated = generator.generate();
			int repetitionEncountered = 0;
			for (int index = 0; index < generated.length() - 1; index++) {
				if (generated.charAt(index) == generated.charAt(index + 1)) {
					repetitionEncountered++;
					break;
				}
			}
			if (repetitionEncountered > 1) {
				repetitionsEncountered++;
			}
		}
		assertThat(repetitionsEncountered, lessThanOrEqualTo(10));
	}
	
	@RepeatedTest(100)
	void usesOnlyAllowedCharacters() {
		String generated = generator.generate();
		for (int index = 0; index < generated.length() - 1; index++) {
			assertThat(RandomStringGenerator.ALLOWED_CHARACTERS, containsString(generated.substring(index, index + 1)));
		}
	}

	@RepeatedTest(10)
	void rarelyRepeatsGeneratedString() {
		List<String> generateds = new ArrayList<>();
		int collisions = 0;
		for (int counter = 0; counter < 10000; counter++) {
			String generated = generator.generate();
			boolean collision = false;
			for (int index = 0; index < generateds.size(); index++) {
				if (generateds.get(index).equals(generated)) {
					collision = true;
					break;
				}
			}
			if (collision) {
				collisions++;
			} else {
				generateds.add(generated);
			}
		}
		// 10 collisions in 1000 strings means 1%
		assertThat(collisions, lessThanOrEqualTo(10));
	}
}
