package com.aidpad.roles;


import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.junit.Test;

import com.aidpad.api.model.RoleTO;
import com.aidpad.api.model.RoleTOList;
import com.aidpad.errorhandling.exceptions.InvalidUriException;
import com.aidpad.persistence.entities.RoleEntity;

public class RolesProviderTest {

	private static final String TEST_BASE_PATH = "/base/path";
	private RolesProvider rolesProvider = new RolesProvider(TEST_BASE_PATH);
	
	@Test
	public void buildsSingleResourceUrlCorrectly() {
		String roleUuid = UUID.randomUUID().toString();
		String roleUrl = rolesProvider.getTOUri(roleUuid);
		assertThat(roleUrl, equalTo(TEST_BASE_PATH + RolesProvider.SINGLE_RESOURCE_PATH + roleUuid));
	}
	
	@Test
	public void buildsListUrlCorrectly() {
		String roleListUrl = rolesProvider.getTOListUri(null);
		assertThat(roleListUrl, equalTo(TEST_BASE_PATH + RolesProvider.LIST_PATH));
	}
	
	@Test
	public void buildsSingleRoleTOWithAllFields() {
		RoleEntity roleEntity = new RoleEntity(UUID.randomUUID().toString(), "test role", null);
		RoleTO roleTO = rolesProvider.buildResource(roleEntity);
		assertThat(roleTO.getRole(), notNullValue());
		assertThat(roleTO.getRole().getUuid(), equalTo(roleEntity.getUuid()));
		assertThat(roleTO.getRole().getName(), equalTo(roleEntity.getName()));
		assertThat(roleTO.getLinks(), notNullValue());
		assertThat(roleTO.getLinks().getSelf(), equalTo(TEST_BASE_PATH + RolesProvider.SINGLE_RESOURCE_PATH + roleEntity.getUuid()));
	}
	
	@Test
	public void returnsListWithAllAttributes() {
		List<RoleEntity> roles = Arrays.<RoleEntity>asList(
				new RoleEntity(UUID.randomUUID().toString(), "test role 1", null),
				new RoleEntity(UUID.randomUUID().toString(), "test role 2", null));
		
		RoleTOList roleTOList = rolesProvider.buildResourceList(roles, null);
		
		assertThat(roleTOList.getRoles(), notNullValue());
		assertThat(roleTOList.getRoles(), hasSize(roles.size()));
		
		assertThat(roleTOList.getRoles().stream().map(role -> role.getRole().getUuid()).collect(Collectors.toList()),
				containsInAnyOrder(roles.get(0).getUuid(), roles.get(1).getUuid()));

		assertThat(roleTOList.getRoles().stream().map(role -> role.getRole().getName()).collect(Collectors.toList()),
				containsInAnyOrder(roles.get(0).getName(), roles.get(1).getName()));

		assertThat(roleTOList.getLinks(), notNullValue());
		assertThat(roleTOList.getLinks().getSelf(), equalTo(TEST_BASE_PATH + RolesProvider.LIST_PATH));
	}
	
	@Test
	public void returnsEmptyListIfRolesListEmpty() {
		List<RoleEntity> roles = new ArrayList<>();
		
		RoleTOList roleTOList = rolesProvider.buildResourceList(roles, null);
		
		assertThat(roleTOList.getRoles(), notNullValue());
		assertThat(roleTOList.getRoles(), hasSize(roles.size()));
		
		assertThat(roleTOList.getLinks(), notNullValue());
		assertThat(roleTOList.getLinks().getSelf(), equalTo(TEST_BASE_PATH + RolesProvider.LIST_PATH));
	}
	
	@Test
	public void extractsUuidFromRoleUri() {
		String testUuid = UUID.randomUUID().toString();
		String roleUri = TEST_BASE_PATH + RolesProvider.SINGLE_RESOURCE_PATH + testUuid;
		assertThat(rolesProvider.getUuidFromUri(roleUri), equalTo(testUuid));
	}
	
	@Test(expected = InvalidUriException.class)
	public void failsUriParsingOnInvalidPath() {
		String testUuid = UUID.randomUUID().toString();
		String roleUri = TEST_BASE_PATH + RolesProvider.SINGLE_RESOURCE_PATH + "/garbage/" + testUuid;
		rolesProvider.getUuidFromUri(roleUri);
	}

	@Test(expected = InvalidUriException.class)
	public void failsUriParsingOnInvalidUuid() {
		String testUuid = "rubbish-uuid";
		String roleUri = TEST_BASE_PATH + RolesProvider.SINGLE_RESOURCE_PATH + "/garbage/" + testUuid;
		rolesProvider.getUuidFromUri(roleUri);
	}
}
