package com.aidpad.roles;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.junit.Assert.assertThat;

import java.util.UUID;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.aidpad.api.model.RoleTO;
import com.aidpad.api.model.RoleValueTO;
import com.aidpad.errorhandling.exceptions.BadRequestException;
import com.aidpad.errorhandling.exceptions.NotFoundException;
import com.aidpad.persistence.entities.RoleEntity;
import com.aidpad.persistence.repositories.RolesRepository;

@RunWith(MockitoJUnitRunner.Silent.class)
public class RolesServiceTest {

	@Mock
	private RolesRepository rolesRepository;
	
	@Mock
	private RolesProvider rolesProvider; 
	
	@InjectMocks
	private RolesService rolesService; 
	
	@Test(expected = NotFoundException.class)
	public void throwsNotFoundWhenRoleDoesNotExist() {
		String roleUuid = UUID.randomUUID().toString();
		when(rolesRepository.findByUuid(any())).thenReturn(null);
		rolesService.getRoleByUuid(roleUuid);
	}

	@Test
	public void returnsRoleTOForExistingUuid() {
		String roleUuid = UUID.randomUUID().toString();
		RoleEntity roleEntity = new RoleEntity(roleUuid, "test role", null);
		when(rolesRepository.findByUuid(any())).thenReturn(roleEntity);
		RoleTO expected = new RoleTO();
		when(rolesProvider.buildResource(roleEntity)).thenReturn(expected);
		assertThat(rolesService.getRoleByUuid(roleUuid), is(expected));
	}
	
	@Test
	public void savesNewTOOnCreation() {
		String roleUuid = UUID.randomUUID().toString();
		RoleEntity roleEntity = new RoleEntity(roleUuid, "test role", null);
		
		when(rolesRepository.findByUuid(roleUuid)).thenReturn(null).thenReturn(roleEntity);
		when(rolesRepository.save(any())).thenAnswer(new Answer<RoleEntity>() {
			@Override
			public RoleEntity answer(InvocationOnMock invocation) throws Throwable {
				RoleEntity toSave = invocation.getArgument(0);
				return toSave.getUuid().equals(roleUuid) ? roleEntity : null;
			}
		});
		RoleTO roleTO = new RoleTO();
		when(rolesProvider.buildResource(roleEntity)).thenReturn(roleTO);
		
		RoleTO toSave = new RoleTO();
		toSave.setRole(new RoleValueTO());
		toSave.getRole().setUuid(roleUuid);
		toSave.getRole().setName("test role");
		
		RoleTO created = rolesService.createRole(toSave);
		assertThat(created, is(roleTO));
		
		ArgumentCaptor<RoleEntity> savedCaptor = ArgumentCaptor.forClass(RoleEntity.class);
		verify(rolesRepository).save(savedCaptor.capture());
		assertThat(savedCaptor.getValue().getUuid(), equalTo(roleUuid));
		assertThat(savedCaptor.getValue().getName(), equalTo(roleEntity.getName()));
	}
	
	@Test(expected = BadRequestException.class)
	public void barfsWhenRoleToCreateAlreadtExists() {
		String roleUuid = UUID.randomUUID().toString();
		RoleEntity roleEntity = new RoleEntity(roleUuid, "test role", null);
		when(rolesRepository.findByUuid(roleUuid)).thenReturn(roleEntity);
		
		RoleTO toSave = new RoleTO();
		toSave.setRole(new RoleValueTO());
		toSave.getRole().setUuid(roleUuid);
		toSave.getRole().setName("test role");
		
		rolesService.createRole(toSave);
		
	}
	
	@Test
	public void updatesEntityOnUpdate() {
		String roleUuid = UUID.randomUUID().toString();
		RoleEntity roleEntity = new RoleEntity(roleUuid, "test role", null);
		when(rolesRepository.findByUuid(roleUuid)).thenReturn(roleEntity);
		
		RoleTO toSave = new RoleTO();
		toSave.setRole(new RoleValueTO());
		toSave.getRole().setUuid(roleUuid);
		toSave.getRole().setName("test role modified");
		
		rolesService.updateRole(roleUuid, toSave);
		
		ArgumentCaptor<RoleEntity> savedCaptor = ArgumentCaptor.forClass(RoleEntity.class);
		verify(rolesRepository).save(savedCaptor.capture());
		assertThat(savedCaptor.getValue().getUuid(), equalTo(roleUuid));
		assertThat(savedCaptor.getValue().getName(), equalTo(toSave.getRole().getName()));
	}
	
	@Test(expected = NotFoundException.class)
	public void throwsNotFoundWhenUpdateNonExistingRole()  {

		String roleUuid = UUID.randomUUID().toString();
		when(rolesRepository.findByUuid(roleUuid)).thenReturn(null);
		
		RoleTO toSave = new RoleTO();
		toSave.setRole(new RoleValueTO());
		toSave.getRole().setUuid(roleUuid);
		toSave.getRole().setName("test role");
		
		rolesService.updateRole(roleUuid, toSave);

	}
	
	@Test
	public void findsRoleByRoleURI() {
		String roleUuid = UUID.randomUUID().toString();
		String uri = "/base/path/role/" + roleUuid;
		
		RoleEntity roleEntity = new RoleEntity();
		RoleTO roleTO = new RoleTO();
		
		when(rolesProvider.getUuidFromUri(uri)).thenReturn(roleUuid);
		when(rolesRepository.findByUuid(roleUuid)).thenReturn(roleEntity);
		when(rolesProvider.buildResource(roleEntity)).thenReturn(roleTO);
		
		assertThat(rolesService.getRoleByUri(uri), equalTo(roleTO ));
	}
}
