package com.aidpad.aidpad;

import org.junit.jupiter.api.Test;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;

import com.aidpad.config.AppProperties;


@SpringBootTest
@EnableConfigurationProperties(AppProperties.class)
class AidpadApplicationTests {

	@Test
	void contextLoads() {
	}

}
