package com.aidpad.users;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.aidpad.api.model.PersonDetailsTO;
import com.aidpad.api.model.UserTO;
import com.aidpad.api.model.UserValueTO;
import com.aidpad.persistence.entities.PersonDetailsEntity;
import com.aidpad.persistence.entities.UserEntity;
import com.aidpad.persistence.repositories.PersonDetailsRepository;

import static org.mockito.ArgumentMatchers.any;

@RunWith(MockitoJUnitRunner.Silent.class)
public class PersonDetailsServiceTest {

	private static final String TEST_PERSON_EMAIL = "a@b.c";
	private static final String TEST_PERSON_NAME = "test-person-name";
	private static final String TEST_PERSON_EMAIL_MODIFIED = "a-modified@b.c";
	private static final String TEST_PERSON_NAME_MODIFIED = "test-person-name-modified";

	@Mock
	private PersonDetailsRepository personDetailsRepository;
	
	@InjectMocks
	private PersonDetailsService personDetailsService;
	
	@Test
	public void allowsFullyValidUser() {
		UserTO user = createPersonUserTO(TEST_PERSON_NAME, TEST_PERSON_EMAIL);
		
		List<String> validationErrors = new ArrayList<>();
		assertThat(personDetailsService.validateDetails(user, validationErrors), is(true));
		
		assertThat(validationErrors, hasSize(0));
	}

	@Test
	public void disallowsEmptyUserDetailsIfUserTypeIsPerson() {
		UserTO user = createPersonUserTO(null, null);
		user.getUser().setPersonDetails(null);
		
		List<String> validationErrors = new ArrayList<>();
		assertThat(personDetailsService.validateDetails(user, validationErrors), is(false));
		
		assertThat(validationErrors, hasSize(1));
		assertThat(validationErrors.get(0), containsString(PersonDetailsService.INVALID_EMPTY_PERSON_DETAILS));
	}

	@Test
	public void disallowsFilledPersonDetailsForNonPersonUser() {
		UserTO user = createPersonUserTO(TEST_PERSON_NAME, TEST_PERSON_EMAIL);
		user.getUser().setUserType(UserType.ORGANISATION.name());
		
		List<String> validationErrors = new ArrayList<>();
		assertThat(personDetailsService.validateDetails(user, validationErrors), is(false));
		
		assertThat(validationErrors, hasSize(1));
		assertThat(validationErrors.get(0), containsString(PersonDetailsService.INVALID_FILLED_PERSON_DETAILS));
	}
	
	@Test
	public void disallowsPersonWithNoName() {
		UserTO user = createPersonUserTO(null, TEST_PERSON_EMAIL);
		
		List<String> validationErrors = new ArrayList<>();
		assertThat(personDetailsService.validateDetails(user, validationErrors), is(false));
		
		assertThat(validationErrors, hasSize(1));
		assertThat(validationErrors.get(0), containsString(PersonDetailsService.INVALID_PERSON_HAS_NO_NAME));
	}

	@Test
	public void disallowsPersonWithNoEmail() {
		UserTO user = createPersonUserTO(TEST_PERSON_NAME, null);
		
		List<String> validationErrors = new ArrayList<>();
		assertThat(personDetailsService.validateDetails(user, validationErrors), is(false));
		
		assertThat(validationErrors, hasSize(1));
		assertThat(validationErrors.get(0), containsString(PersonDetailsService.INVALID_PERSON_HAS_NO_EMAIL));
	}

	@Test
	public void allowsFullyValidUserEntity() {
		UserEntity user = createValidUserEntity(TEST_PERSON_NAME, TEST_PERSON_EMAIL);
		
		List<String> validationErrors = new ArrayList<>();
		assertThat(personDetailsService.validateDetails(user, validationErrors), is(true));
		
		assertThat(validationErrors, hasSize(0));
	}

	@Test
	public void disallowsEmptyUserDetailsIfUserEntityTypeIsPerson() {
		UserEntity user = createValidUserEntity(null, null);
		user.getPersonDetails().clear();
		
		List<String> validationErrors = new ArrayList<>();
		assertThat(personDetailsService.validateDetails(user, validationErrors), is(false));
		
		assertThat(validationErrors, hasSize(1));
		assertThat(validationErrors.get(0), containsString(PersonDetailsService.INVALID_EMPTY_PERSON_DETAILS));
	}

	@Test
	public void disallowsFilledPersonDetailsEntityForNonPersonUser() {
		UserEntity user = createValidUserEntity(TEST_PERSON_NAME, TEST_PERSON_EMAIL);
		user.setUserType(UserType.ORGANISATION.name());
		
		List<String> validationErrors = new ArrayList<>();
		assertThat(personDetailsService.validateDetails(user, validationErrors), is(false));
		
		assertThat(validationErrors, hasSize(1));
		assertThat(validationErrors.get(0), containsString(PersonDetailsService.INVALID_FILLED_PERSON_DETAILS));
	}

	@Test
	public void disallowsPersonDetailsEntityWithNoName() {
		UserEntity user = createValidUserEntity(null, TEST_PERSON_EMAIL);
		
		List<String> validationErrors = new ArrayList<>();
		assertThat(personDetailsService.validateDetails(user, validationErrors), is(false));
		
		assertThat(validationErrors, hasSize(1));
		assertThat(validationErrors.get(0), containsString(PersonDetailsService.INVALID_PERSON_HAS_NO_NAME));
	}
	
	@Test
	public void disallowsPersonDetailsEntityWithNoEmail() {
		UserEntity user = createValidUserEntity(TEST_PERSON_NAME, null);
		
		List<String> validationErrors = new ArrayList<>();
		assertThat(personDetailsService.validateDetails(user, validationErrors), is(false));
		
		assertThat(validationErrors, hasSize(1));
		assertThat(validationErrors.get(0), containsString(PersonDetailsService.INVALID_PERSON_HAS_NO_EMAIL));
	}

	@Test
	public void persistsPersonDetailsOnSave() {
		UserTO userTO = createPersonUserTO(TEST_PERSON_NAME, TEST_PERSON_EMAIL);
		UserEntity userEntity = createValidUserEntity(TEST_PERSON_EMAIL, TEST_PERSON_EMAIL);
		userEntity.getPersonDetails().clear();
		
		when(personDetailsRepository.save(any(PersonDetailsEntity.class))).thenAnswer(new Answer<PersonDetailsEntity>() {
			@Override
			public PersonDetailsEntity answer(InvocationOnMock invocation) throws Throwable {
				return invocation.getArgument(0);
			}
		});
		
		personDetailsService.createDetails(userTO, userEntity);
		
		ArgumentCaptor<PersonDetailsEntity> saveArgumentCaptor = ArgumentCaptor.forClass(PersonDetailsEntity.class);
		verify(personDetailsRepository).save(saveArgumentCaptor.capture());
		
		PersonDetailsEntity savedDetails = saveArgumentCaptor.getValue();
		assertThat(savedDetails.getPersonName(), equalTo(userTO.getUser().getPersonDetails().getName()));
		assertThat(savedDetails.getEmail(), equalTo(userTO.getUser().getPersonDetails().getEmail()));
		assertThat(userEntity.getPersonDetails().iterator().next(), is(savedDetails));
	}
	
	@Test
	public void deletesPersonDetailsOnDelete() {
		UserEntity userEntity = createValidUserEntity(TEST_PERSON_EMAIL, TEST_PERSON_EMAIL);
		personDetailsService.deleteDetails(userEntity);
		
		Set<PersonDetailsEntity> personDetailsSet = userEntity.getPersonDetails(); 
		
		verify(personDetailsRepository).deleteAll(personDetailsSet);
		assertThat(userEntity.getPersonDetails(), hasSize(0));
	}

	@Test
	public void deletesAndCreatesPersonDetailsOnUpdate() {
		UserTO userTO = createPersonUserTO(TEST_PERSON_NAME_MODIFIED, TEST_PERSON_EMAIL_MODIFIED);
		UserEntity userEntity = createValidUserEntity(TEST_PERSON_EMAIL, TEST_PERSON_EMAIL);
		
		Set<PersonDetailsEntity> personDetailsSet = userEntity.getPersonDetails();
		PersonDetailsEntity personDetails = personDetailsSet.iterator().next();
		
		when(personDetailsRepository.save(any(PersonDetailsEntity.class))).thenAnswer(new Answer<PersonDetailsEntity>() {
			@Override
			public PersonDetailsEntity answer(InvocationOnMock invocation) throws Throwable {
				return invocation.getArgument(0);
			}
		});
		
		personDetailsService.updateDetails(userTO, userEntity);
		
		verify(personDetailsRepository).deleteAll(personDetailsSet);
		
		ArgumentCaptor<PersonDetailsEntity> saveArgumentCaptor = ArgumentCaptor.forClass(PersonDetailsEntity.class);
		verify(personDetailsRepository).save(saveArgumentCaptor.capture());
		
		PersonDetailsEntity savedDetails = saveArgumentCaptor.getValue();
		assertThat(savedDetails.getPersonName(), equalTo(userTO.getUser().getPersonDetails().getName()));
		assertThat(savedDetails.getEmail(), equalTo(userTO.getUser().getPersonDetails().getEmail()));
		assertThat(userEntity.getPersonDetails().iterator().next(), is(savedDetails));
		
		assertThat(savedDetails, not(is(personDetails)));
	}

	
	private UserTO createPersonUserTO(String personName, String personEmail) {
		UserTO user = new UserTO();
		user.setUser(new UserValueTO());
		user.getUser().setUserType(UserType.PERSON.name());
		PersonDetailsTO personDetails = createValidPersonDetailsTO(personName, personEmail);
		user.getUser().setPersonDetails(personDetails);
		return user;
	}

	private PersonDetailsTO createValidPersonDetailsTO(String personName, String personEmail) {
		PersonDetailsTO personDetails = new PersonDetailsTO();
		personDetails.setName(personName);
		personDetails.setEmail(personEmail);
		return personDetails;
	}
	
	private UserEntity createValidUserEntity(String personName, String personEmail) {
		UserEntity user = new UserEntity();
		user.setUserType(UserType.PERSON.name());
		user.setPersonDetails(new HashSet<>());
		PersonDetailsEntity personDetails = createValidPersonDetailsEntity(personName, personEmail);
		user.getPersonDetails().add(personDetails);
		return user;
	}

	private PersonDetailsEntity createValidPersonDetailsEntity(String personName, String personEmail) {
		PersonDetailsEntity personDetails = new PersonDetailsEntity();
		personDetails.setPersonName(personName);
		personDetails.setEmail(personEmail);
		return personDetails;
	}
}
