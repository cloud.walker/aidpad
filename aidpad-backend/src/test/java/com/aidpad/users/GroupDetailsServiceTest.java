package com.aidpad.users;

import static com.aidpad.users.GroupDetailsService.MIN_CONTACT_INFO;
import static com.aidpad.users.GroupDetailsService.MIN_GROUP_MEMBERS;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.aidpad.api.model.GroupDetailsTO;
import com.aidpad.api.model.GroupDetailsTOMembers;
import com.aidpad.api.model.UserTO;
import com.aidpad.api.model.UserValueTO;
import com.aidpad.persistence.entities.GroupMemberDetailsEntity;
import com.aidpad.persistence.entities.UserEntity;
import com.aidpad.persistence.repositories.GroupMembersRepository;

@RunWith(MockitoJUnitRunner.Silent.class)
public class GroupDetailsServiceTest {

	private static final String TEST_GROUP_MEMBER_NAME_TEMPLATE = "test-group-member-name-%d";
	private static final String TEST_GROUP_MEMBER_EMAIL_TEMPLATE = "test-email-%d@example.com";
	private static final String TEST_GROUP_MEMBER_PHONE_TEMPLATE = "123-456-789-%d";
	
	public static final class OnGroupMemberNameMatcher extends TypeSafeMatcher<GroupMemberDetailsEntity> {

		private final String memberName;
		
		public OnGroupMemberNameMatcher(String memberName) {
			this.memberName = memberName;
		}

		@Override
		public boolean matchesSafely(GroupMemberDetailsEntity member) {
			if ((member == null && memberName == null)) {
				return true;
			}
			if ((member == null && memberName != null)) {
				return false;
			}
			if (!GroupMemberDetailsEntity.class.equals(member.getClass())) {
				return false;
			}
			return memberName.equals(member.getPersonName());
		}

		@Override
		public void describeTo(Description description) {
			description.appendText("Group member does not have name \"" + memberName + "\"");
		}
	}
	
	@Mock
	private GroupMembersRepository groupMembersRepository;
	
	@InjectMocks
	private GroupDetailsService groupDetailsService;
	
	@Test
	public void validGroupUserTOIsValid() {
		
		List<GroupDetailsTOMembers> members = new ArrayList<>();
		for (int index = 0; index < MIN_GROUP_MEMBERS; index++) {
			members.add(createGroupMemberTO(
					String.format(TEST_GROUP_MEMBER_NAME_TEMPLATE, index + 1), 
					String.format(TEST_GROUP_MEMBER_EMAIL_TEMPLATE, index + 1), 
					String.format(TEST_GROUP_MEMBER_PHONE_TEMPLATE, index + 1)));
		}
		UserTO userTO = createGroupUserTO(members);
		
		List<String> validationMessages = new ArrayList<>();
		assertThat(groupDetailsService.validateDetails(userTO, validationMessages), is(true));
		
		assertThat(validationMessages, hasSize(0));
	}

	
	@Test
	public void disallowsGroupUserTOWithoutGroupMembers() {
		
		List<GroupDetailsTOMembers> members = new ArrayList<>();
		UserTO userTO = createGroupUserTO(members);
		
		List<String> validationMessages = new ArrayList<>();
		assertThat(groupDetailsService.validateDetails(userTO, validationMessages), is(false));
		
		assertThat(validationMessages, hasSize(1));
		assertThat(validationMessages.get(0), equalTo(GroupDetailsService.INVALID_EMPTY_GROUP_MEMBER_DETAILS));
	}

	@Test
	public void disallowsGroupUserTOWithTooFewGroupMembers() {
		
		List<GroupDetailsTOMembers> members = new ArrayList<>();
		for (int index = 0; index < MIN_GROUP_MEMBERS - 1; index++) {
			members.add(createGroupMemberTO(
					String.format(TEST_GROUP_MEMBER_NAME_TEMPLATE, index + 1), 
					String.format(TEST_GROUP_MEMBER_EMAIL_TEMPLATE, index + 1), 
					String.format(TEST_GROUP_MEMBER_PHONE_TEMPLATE, index + 1)));
		}
		UserTO userTO = createGroupUserTO(members);
		
		List<String> validationMessages = new ArrayList<>();
		assertThat(groupDetailsService.validateDetails(userTO, validationMessages), is(false));
		
		// will also trigger the too few contacts check, therefore no size check here
		assertThat(validationMessages, hasItem(GroupDetailsService.INVALID_GROUP_TOO_SMALL));
	}

	@Test
	public void disallowsGroupUserTOWithTooFewContactDetails() {
		
		List<GroupDetailsTOMembers> members = new ArrayList<>();
		for (int index = 0; index < MIN_CONTACT_INFO - 1; index++) {
			members.add(createGroupMemberTO(
					String.format(TEST_GROUP_MEMBER_NAME_TEMPLATE, index + 1), 
					String.format(TEST_GROUP_MEMBER_EMAIL_TEMPLATE, index + 1), 
					String.format(TEST_GROUP_MEMBER_PHONE_TEMPLATE, index + 1)));
		}
		for (int index = MIN_CONTACT_INFO - 1; index < MIN_GROUP_MEMBERS + 1; index++) {
		members.add(createGroupMemberTO(
				String.format(TEST_GROUP_MEMBER_NAME_TEMPLATE, MIN_GROUP_MEMBERS - 1), 
				null, 
				null));
		}
		UserTO userTO = createGroupUserTO(members);
		
		List<String> validationMessages = new ArrayList<>();
		assertThat(groupDetailsService.validateDetails(userTO, validationMessages), is(false));

		assertThat(validationMessages, hasSize(1));
		assertThat(validationMessages.get(0), equalTo(GroupDetailsService.INVALID_GROUP_TOO_FEW_CONTACTS));
	}
	
	@Test
	public void disallowsGroupMemberTOWithNoName() {
		
		List<GroupDetailsTOMembers> members = new ArrayList<>();
		for (int index = 0; index < MIN_CONTACT_INFO - 1; index++) {
			members.add(createGroupMemberTO(
					String.format(TEST_GROUP_MEMBER_NAME_TEMPLATE, index + 1), 
					String.format(TEST_GROUP_MEMBER_EMAIL_TEMPLATE, index + 1), 
					String.format(TEST_GROUP_MEMBER_PHONE_TEMPLATE, index + 1)));
		}
		members.add(createGroupMemberTO(
				null, 
				String.format(TEST_GROUP_MEMBER_EMAIL_TEMPLATE, MIN_CONTACT_INFO - 1), 
				String.format(TEST_GROUP_MEMBER_PHONE_TEMPLATE, MIN_CONTACT_INFO - 1)));
		UserTO userTO = createGroupUserTO(members);
		
		List<String> validationMessages = new ArrayList<>();
		assertThat(groupDetailsService.validateDetails(userTO, validationMessages), is(false));

		assertThat(validationMessages, hasSize(1));
		assertThat(validationMessages.get(0), equalTo(GroupDetailsService.INVALID_GROUP_MEMBER_HAS_NO_NAME));
	}
	
	@Test
	public void disallowsGroupUserTOWithNoGroupDetails() {
		UserTO userTO = createGroupUserTO(null);
		
		List<String> validationMessages = new ArrayList<>();
		assertThat(groupDetailsService.validateDetails(userTO, validationMessages), is(false));

		assertThat(validationMessages, hasSize(1));
		assertThat(validationMessages.get(0), equalTo(GroupDetailsService.INVALID_EMPTY_GROUP_MEMBER_DETAILS));
	}

	@Test
	public void disallowsNonGroupUserTOWithGroupDetails() {
		UserTO userTO = createGroupUserTO(null);
		userTO.getUser().setUserType(UserType.ORGANISATION.name());
		
		List<String> validationMessages = new ArrayList<>();
		assertThat(groupDetailsService.validateDetails(userTO, validationMessages), is(false));

		assertThat(validationMessages, hasSize(1));
		assertThat(validationMessages.get(0), equalTo(GroupDetailsService.INVALID_FILLED_GROUP_DETAILS));
	}
	
	@Test
	public void validGroupUserEntityIsValid() {
		
		String uuid = UUID.randomUUID().toString();
		long userId = 14L;
		
		Set<GroupMemberDetailsEntity> groupMembers = new HashSet<>();
		for (int index = 0; index < MIN_GROUP_MEMBERS; index++) {
			groupMembers.add(createGroupMemberEntity(
					String.format(TEST_GROUP_MEMBER_NAME_TEMPLATE, index + 1), 
					String.format(TEST_GROUP_MEMBER_EMAIL_TEMPLATE, index + 1), 
					String.format(TEST_GROUP_MEMBER_PHONE_TEMPLATE, index + 1)));
		}
		UserEntity userEntity = createGroupUserEntity(userId, uuid, groupMembers);

		List<String> validationMessages = new ArrayList<>();
		assertThat(groupDetailsService.validateDetails(userEntity, validationMessages), is(true));
		
		assertThat(validationMessages, hasSize(0));		
	}


	@Test
	public void disallowsGroupUserEntityWithoutGroupMembers() {
		String uuid = UUID.randomUUID().toString();
		long userId = 14L;
		
		Set<GroupMemberDetailsEntity> groupMembers = new HashSet<>();

		UserEntity userEntity = createGroupUserEntity(userId, uuid, groupMembers);

		List<String> validationMessages = new ArrayList<>();
		assertThat(groupDetailsService.validateDetails(userEntity, validationMessages), is(false));
		
		assertThat(validationMessages, hasSize(1));
		assertThat(validationMessages.get(0), equalTo(GroupDetailsService.INVALID_EMPTY_GROUP_MEMBER_DETAILS));
	}
	
	@Test
	public void disallowsGroupUserEntityWithTooFewGroupMembers() {
		String uuid = UUID.randomUUID().toString();
		long userId = 14L;
		
		Set<GroupMemberDetailsEntity> groupMembers = new HashSet<>();
		for (int index = 0; index < MIN_GROUP_MEMBERS - 1; index++) {
			groupMembers.add(createGroupMemberEntity(
					String.format(TEST_GROUP_MEMBER_NAME_TEMPLATE, index + 1), 
					String.format(TEST_GROUP_MEMBER_EMAIL_TEMPLATE, index + 1), 
					String.format(TEST_GROUP_MEMBER_PHONE_TEMPLATE, index + 1)));
		}
		UserEntity userEntity = createGroupUserEntity(userId, uuid, groupMembers);

		List<String> validationMessages = new ArrayList<>();
		assertThat(groupDetailsService.validateDetails(userEntity, validationMessages), is(false));
		
		// no size check here because too little contact info will also be triggered 
		assertThat(validationMessages.get(0), equalTo(GroupDetailsService.INVALID_GROUP_TOO_SMALL));
	}
	
	@Test
	public void disallowsGroupUserEntityWithTooFewContactDetails() {
		String uuid = UUID.randomUUID().toString();
		long userId = 14L;
		
		Set<GroupMemberDetailsEntity> groupMembers = new HashSet<>();
		for (int index = 0; index < MIN_CONTACT_INFO - 1; index++) {
			groupMembers.add(createGroupMemberEntity(
					String.format(TEST_GROUP_MEMBER_NAME_TEMPLATE, index + 1), 
					String.format(TEST_GROUP_MEMBER_EMAIL_TEMPLATE, index + 1), 
					String.format(TEST_GROUP_MEMBER_PHONE_TEMPLATE, index + 1)));
		}
		for (int index = MIN_CONTACT_INFO - 1; index < MIN_GROUP_MEMBERS + 1; index++) {
			groupMembers.add(createGroupMemberEntity(
					String.format(TEST_GROUP_MEMBER_NAME_TEMPLATE, index + 1), 
					null, 
					null));
		}
		UserEntity userEntity = createGroupUserEntity(userId, uuid, groupMembers);

		List<String> validationMessages = new ArrayList<>();
		assertThat(groupDetailsService.validateDetails(userEntity, validationMessages), is(false));
		
		assertThat(validationMessages, hasSize(1));
		assertThat(validationMessages.get(0), equalTo(GroupDetailsService.INVALID_GROUP_TOO_FEW_CONTACTS));
	}
	
	@Test
	public void disallowsGroupMemberEntityWithNoName() {
		String uuid = UUID.randomUUID().toString();
		long userId = 14L;
		
		Set<GroupMemberDetailsEntity> groupMembers = new HashSet<>();
		for (int index = 0; index < MIN_GROUP_MEMBERS - 1; index++) {
			groupMembers.add(createGroupMemberEntity(
					String.format(TEST_GROUP_MEMBER_NAME_TEMPLATE, index + 1), 
					String.format(TEST_GROUP_MEMBER_EMAIL_TEMPLATE, index + 1), 
					String.format(TEST_GROUP_MEMBER_PHONE_TEMPLATE, index + 1)));
		}
		groupMembers.add(createGroupMemberEntity(
				null, 
				String.format(TEST_GROUP_MEMBER_EMAIL_TEMPLATE, MIN_GROUP_MEMBERS - 1), 
				String.format(TEST_GROUP_MEMBER_PHONE_TEMPLATE, MIN_GROUP_MEMBERS - 1)));
		UserEntity userEntity = createGroupUserEntity(userId, uuid, groupMembers);

		List<String> validationMessages = new ArrayList<>();
		assertThat(groupDetailsService.validateDetails(userEntity, validationMessages), is(false));
		
		assertThat(validationMessages, hasSize(1));
		assertThat(validationMessages.get(0), equalTo(GroupDetailsService.INVALID_GROUP_MEMBER_HAS_NO_NAME));
	}
	
	@Test
	public void disallowsNonGroupUserEntityWithGroupMembers() {
		String uuid = UUID.randomUUID().toString();
		long userId = 14L;
		
		Set<GroupMemberDetailsEntity> groupMembers = new HashSet<>();
		for (int index = 0; index < MIN_GROUP_MEMBERS; index++) {
			groupMembers.add(createGroupMemberEntity(
					String.format(TEST_GROUP_MEMBER_NAME_TEMPLATE, index + 1), 
					String.format(TEST_GROUP_MEMBER_EMAIL_TEMPLATE, index + 1), 
					String.format(TEST_GROUP_MEMBER_PHONE_TEMPLATE, index + 1)));
		}
		UserEntity userEntity = createGroupUserEntity(userId, uuid, groupMembers);
		userEntity.setUserType(UserType.ORGANISATION.name());

		List<String> validationMessages = new ArrayList<>();
		assertThat(groupDetailsService.validateDetails(userEntity, validationMessages), is(false));
		
		assertThat(validationMessages, hasSize(1));
		assertThat(validationMessages.get(0), equalTo(GroupDetailsService.INVALID_FILLED_GROUP_DETAILS));
	}

	@Test
	public void deletesAllGroupMembersOnDelete() {
		String uuid = UUID.randomUUID().toString();
		long userId = 14L;
		
		Set<GroupMemberDetailsEntity> groupMembers = new HashSet<>();
		for (int index = 0; index < MIN_GROUP_MEMBERS; index++) {
			groupMembers.add(createGroupMemberEntity(
					String.format(TEST_GROUP_MEMBER_NAME_TEMPLATE, index + 1), 
					String.format(TEST_GROUP_MEMBER_EMAIL_TEMPLATE, index + 1), 
					String.format(TEST_GROUP_MEMBER_PHONE_TEMPLATE, index + 1)));
		}
		UserEntity userEntity = createGroupUserEntity(userId, uuid, groupMembers);

		groupDetailsService.deleteDetails(userEntity);
		
		verify(groupMembersRepository).deleteAll(groupMembers);
	}

	@Test
	public void createsAllGroupMembersOnDelete() {
		String uuid = UUID.randomUUID().toString();
		long userId = 14L;
		
		List<GroupDetailsTOMembers> members = new ArrayList<>();
		for (int index = 0; index < MIN_GROUP_MEMBERS; index++) {
			members.add(createGroupMemberTO(
					String.format(TEST_GROUP_MEMBER_NAME_TEMPLATE, index + 1), 
					String.format(TEST_GROUP_MEMBER_EMAIL_TEMPLATE, index + 1), 
					String.format(TEST_GROUP_MEMBER_PHONE_TEMPLATE, index + 1)));
		}
		UserTO userTO = createGroupUserTO(members);
		userTO.getUser().setUuid(uuid);
		
		UserEntity userEntity = createGroupUserEntity(userId, uuid, new HashSet<>());
		
		when(groupMembersRepository.save(any(GroupMemberDetailsEntity.class))).thenAnswer(new Answer<GroupMemberDetailsEntity>() {
			@Override
			public GroupMemberDetailsEntity answer(InvocationOnMock invocation) throws Throwable {
				return invocation.getArgument(0);
			}
		});
		
		groupDetailsService.createDetails(userTO, userEntity);
		
		ArgumentCaptor<GroupMemberDetailsEntity> membersCaptor = ArgumentCaptor.forClass(GroupMemberDetailsEntity.class);
		verify(groupMembersRepository, times(MIN_GROUP_MEMBERS)).save(membersCaptor.capture());
		
		List<GroupMemberDetailsEntity> allSavedMembers = membersCaptor.getAllValues();
		assertThat(allSavedMembers, hasSize(MIN_GROUP_MEMBERS));
		for (int index = 0; index < MIN_GROUP_MEMBERS; index++) {
			GroupDetailsTOMembers member = members.get(index);
			assertThat(allSavedMembers, hasItem(matchesName(member.getName())));
		}
	}
	
	@Test
	public void firstDeleteThenCreatesMembersOnUpdate() {
		String uuid = UUID.randomUUID().toString();
		long userId = 14L;
		
		List<GroupDetailsTOMembers> groupMemberTOs = new ArrayList<>();
		for (int index = 0; index < MIN_GROUP_MEMBERS + 1 ; index++) {
			groupMemberTOs.add(createGroupMemberTO(
					String.format(TEST_GROUP_MEMBER_NAME_TEMPLATE, index + 1), 
					String.format(TEST_GROUP_MEMBER_EMAIL_TEMPLATE, index + 1), 
					String.format(TEST_GROUP_MEMBER_PHONE_TEMPLATE, index + 1)));
		}
		UserTO userTO = createGroupUserTO(groupMemberTOs);
		userTO.getUser().setUuid(uuid);

		Set<GroupMemberDetailsEntity> groupMemberEntities = new HashSet<>();
		for (int index = 0; index < MIN_GROUP_MEMBERS + 4; index++) {
			groupMemberEntities.add(createGroupMemberEntity(
					String.format(TEST_GROUP_MEMBER_NAME_TEMPLATE, index + 1), 
					String.format(TEST_GROUP_MEMBER_EMAIL_TEMPLATE, index + 1), 
					String.format(TEST_GROUP_MEMBER_PHONE_TEMPLATE, index + 1)));
		}
		UserEntity userEntity = createGroupUserEntity(userId, uuid, groupMemberEntities);
		
		groupDetailsService.updateDetails(userTO, userEntity);

		verify(groupMembersRepository).deleteAll(groupMemberEntities);
		
		verify(groupMembersRepository, times(MIN_GROUP_MEMBERS + 1)).save(any());
	}
	
	/*
	 * first deletes, then creates on update
	 * */
	
	private UserTO createGroupUserTO(List<GroupDetailsTOMembers> members) {
		UserTO userTO = new UserTO();
		userTO.setUser(new UserValueTO());
		userTO.getUser().setEnabled(false);
		userTO.getUser().setUserType(UserType.GROUP.name());
		
		userTO.getUser().setGroupDetails(new GroupDetailsTO());
		userTO.getUser().getGroupDetails().setMembers(members);
		return userTO;
	}

	private GroupDetailsTOMembers createGroupMemberTO(String name, String email, String phoneNumber) {
		GroupDetailsTOMembers groupMember = new GroupDetailsTOMembers();
		groupMember.setName(name);
		groupMember.setEmail(email);
		groupMember.setPhoneNumber(phoneNumber);
		return groupMember;
	}
	
	private UserEntity createGroupUserEntity(long id, String uuid, Set<GroupMemberDetailsEntity> groupMembers) {
		UserEntity userEntity = new UserEntity();
		userEntity.setId(id);
		userEntity.setUuid(uuid);
		userEntity.setUserType(UserType.GROUP.name());
		groupMembers.forEach(member -> member.setUser(userEntity));
		userEntity.setGroupMembers(groupMembers);
		return userEntity;
	}


	private GroupMemberDetailsEntity createGroupMemberEntity(String name, String email, String phoneNumber) {
		return new GroupMemberDetailsEntity(null, name, email, phoneNumber);
	}
	
	public static Matcher<GroupMemberDetailsEntity> matchesName(String name) {
		return new OnGroupMemberNameMatcher(name);
	}
}
