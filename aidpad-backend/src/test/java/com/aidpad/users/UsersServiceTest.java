package com.aidpad.users;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatcher;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.springframework.test.util.ReflectionTestUtils;

import com.aidpad.api.model.PersonDetailsTO;
import com.aidpad.api.model.UserTO;
import com.aidpad.api.model.UserTOLinks;
import com.aidpad.api.model.UserValueTO;
import com.aidpad.errorhandling.exceptions.BadRequestException;
import com.aidpad.errorhandling.exceptions.NotFoundException;
import com.aidpad.persistence.entities.RoleEntity;
import com.aidpad.persistence.entities.UserEntity;
import com.aidpad.persistence.entities.UserRoleEntity;
import com.aidpad.persistence.repositories.RolesRepository;
import com.aidpad.persistence.repositories.UserRolesRepository;
import com.aidpad.persistence.repositories.UsersRepository;
import com.aidpad.roles.RolesProvider;

@RunWith(MockitoJUnitRunner.Silent.class)
public class UsersServiceTest {

	@Mock
	private RolesProvider rolesProvider;
	
	@Mock
	private UsersProvider usersProvider;
	
	@Mock
	private UsersRepository usersRepository;

	@Mock
	private RolesRepository rolesRepository;
	
	@Mock
	private UserRolesRepository userRolesRepository;
		
	@Mock
	private GroupDetailsService groupDetailsService;
	
	@Mock
	private OrganizationDetailsService organizationDetailsService;
	
	@Mock
	private PersonDetailsService personDetailsService;
	
	@InjectMocks
	private UsersService usersService;

	@Test(expected = NotFoundException.class)
	public void barfsWhenUserNotFound() {
		String userUuid = UUID.randomUUID().toString();
		when(usersRepository.findByUuid(userUuid)).thenReturn(null);
		usersService.getUserByUuid(userUuid);
	}

	@Test(expected = BadRequestException.class)
	public void barfsWhenUserTypeChanged() {
		String userUuid = UUID.randomUUID().toString();
		UserEntity userEntity = new UserEntity(userUuid, AuthenticationType.GOOGLE.name(), 
				UserType.PERSON.name(), null, "test-user", false, true, new HashSet<>(), new HashSet<>(), new HashSet<>(), new HashSet<>());
		when(usersRepository.findByUuid(userUuid)).thenReturn(userEntity);
		
		UserTO toUpdate = makeUserTO(userUuid, false, "test-user", 
				UserType.ORGANISATION.name(), AuthenticationType.GOOGLE.name(), 
				stringList());
		
		usersService.updateUser(userUuid, toUpdate);
	}

	@Test
	public void reportsDetailsValidationMessagesOnCreate() {
		
		final String VALIDATION_ERROR_MESSAGE = "test-generated validation error message."; 
		
		when(personDetailsService.validateDetails(any(UserTO.class), anyList())).thenAnswer(new Answer<Boolean>() {
			@SuppressWarnings("unchecked")
			@Override
			public Boolean answer(InvocationOnMock invocation) throws Throwable {
				invocation.getArgument(1, List.class).add(VALIDATION_ERROR_MESSAGE);
				return false;
			}
		});
		
		String userUuid = UUID.randomUUID().toString();
		UserEntity userEntity = new UserEntity(userUuid, AuthenticationType.FACEBOOK.name(), UserType.PERSON.name(), "valid-login-name", false, true);
		when(usersRepository.findByUuid(userUuid)).thenReturn(null);
		when(usersRepository.saveAndFlush(any(UserEntity.class))).thenAnswer(new Answer<UserEntity>() {
			@Override
			public UserEntity answer(InvocationOnMock invocation) throws Throwable {
				return invocation.getArgument(0);
			}
		});
		
		UserTO toCreate = this.makeFromEntity(userEntity);
		
		toCreate.getUser().setPersonDetails(new PersonDetailsTO());
		
		try {
			usersService.createUser(toCreate);
			fail("Should not succeed creating user with invalid details.");
		} catch (BadRequestException ex) {
			assertThat(ex.getMessage(), containsString(VALIDATION_ERROR_MESSAGE));
		}
	}

	@Test
	public void returnsUserTOForValidUuid() {
		String userUuid = UUID.randomUUID().toString();
		UserEntity userEntity = new UserEntity(userUuid, AuthenticationType.GOOGLE.name(), 
				UserType.PERSON.name(), null, "test-user", false, true, new HashSet<>(), new HashSet<>(), new HashSet<>(), new HashSet<>());
		when(usersRepository.findByUuid(userUuid)).thenReturn(userEntity);
		
		UserTO expected = new UserTO();
		when(usersProvider.buildResource(userEntity)).thenReturn(expected);
		
		UserTO userTO = usersService.getUserByUuid(userUuid);
		
		assertThat(userTO, equalTo(expected));
	}
	
	@Test
	public void savesUserAndUserRolesOnCreate() {
		boolean enabled = false;
		String roleUuid1 = "role-1-uuid";
		String roleUuid2 = "role-2-uuid";
		UserTO toCreate = makeUserTO(UUID.randomUUID().toString(), enabled, "test-user", 
				UserType.PERSON.name(), AuthenticationType.GOOGLE.name(), 
				stringList("http://example.com/roles/" + roleUuid1, "http://example.com/roles/" + roleUuid2));
		
		toCreate.getUser().setPersonDetails(new PersonDetailsTO());
		toCreate.getUser().getPersonDetails().setName("test--person-details");
		toCreate.getUser().getPersonDetails().setEmail("a@b.c");
		
		UserEntity saved = makeFromTO(toCreate);
		UserTO createdResult = new UserTO();
		when(usersProvider.buildResource(saved)).thenReturn(createdResult);
		
		UserTO savedTO = usersService.createUser(toCreate);
		
		ArgumentCaptor<UserEntity> userEntityCaptor = ArgumentCaptor.forClass(UserEntity.class); 
		
		verify(usersRepository).saveAndFlush(userEntityCaptor.capture());
		
		UserEntity savedEntity = userEntityCaptor.getValue();
		assertThat(savedEntity.getLoginName(), equalTo(toCreate.getUser().getLoginName()));
		assertThat(savedEntity.getAuthType(), equalTo(toCreate.getUser().getAuthType()));
		assertThat(savedEntity.getUserType(), equalTo(toCreate.getUser().getUserType()));
		assertThat(savedEntity.isEnabled(), equalTo(toCreate.getUser().getEnabled()));
		assertThat(savedEntity.getUuid(), equalTo(toCreate.getUser().getUuid()));

		assertThat(savedTO, is(createdResult));
		verify(userRolesRepository, times(2)).save(any());
	}
	
	@Test(expected = BadRequestException.class)
	public void barfsOnCreateUserWithExistingUuid() {
		UserEntity userEntity = new UserEntity();
		userEntity.setUuid(UUID.randomUUID().toString());
		when(usersRepository.findByUuid(userEntity.getUuid())).thenReturn(userEntity);
		
		UserTO toCreate = makeFromEntity(userEntity);
		
		usersService.createUser(toCreate);
	}
	
	@Test
	public void savesUserChangesOnUpdate() {
		boolean enabled = false;
		String uuid = UUID.randomUUID().toString();
		UserEntity beforeSave = new UserEntity(uuid, AuthenticationType.GOOGLE.name(), 
				UserType.PERSON.name(), null, "test-user", enabled, true, null, null, null, null);
		beforeSave.setId(14L);
		
		UserTO updated = makeFromEntity(beforeSave);
		updated.getUser().setLoginName("test-user-changed");
		updated.getUser().setAuthType(AuthenticationType.FACEBOOK.name());
		
		updated.getUser().setPersonDetails(new PersonDetailsTO());
		updated.getUser().getPersonDetails().setEmail("a@b.c");
		updated.getUser().getPersonDetails().setName("test-person-name");
		
		UserEntity afterSave = makeFromTO(updated);
		afterSave.setId(beforeSave.getId());
		
		when(usersRepository.findByUuid(uuid)).thenReturn(beforeSave);
		when(usersRepository.save(matchesOnUuid(uuid))).thenReturn(afterSave);
		when(usersRepository.saveAndFlush(matchesOnUuid(uuid))).thenReturn(afterSave);
		
		usersService.updateUser(uuid, updated);
		
		ArgumentCaptor<UserEntity> userEntityCaptor = ArgumentCaptor.forClass(UserEntity.class); 
		
		verify(usersRepository).saveAndFlush(userEntityCaptor.capture());
		
		assertThat(userEntityCaptor.getValue().getUuid(), equalTo(updated.getUser().getUuid()));
		assertThat(userEntityCaptor.getValue().getLoginName(), equalTo(updated.getUser().getLoginName()));
		assertThat(userEntityCaptor.getValue().getUserType(), equalTo(updated.getUser().getUserType()));
		assertThat(userEntityCaptor.getValue().getAuthType(), equalTo(updated.getUser().getAuthType()));
	}

	@Test
	public void savesDetailsOnCreate() {
		when(personDetailsService.validateDetails(any(UserTO.class), anyList())).thenReturn(true);
		
		String userUuid = UUID.randomUUID().toString();
		UserEntity userEntity = new UserEntity(userUuid, AuthenticationType.FACEBOOK.name(), UserType.PERSON.name(), "valid-login-name", false, true);
		when(usersRepository.findByUuid(userUuid)).thenReturn(null);
		when(usersRepository.saveAndFlush(any(UserEntity.class))).thenAnswer(new Answer<UserEntity>() {
			@Override
			public UserEntity answer(InvocationOnMock invocation) throws Throwable {
				return invocation.getArgument(0);
			}
		});
		
		UserTO toCreate = this.makeFromEntity(userEntity);
		
		toCreate.getUser().setPersonDetails(new PersonDetailsTO());

		usersService.createUser(toCreate);
		
		verify(personDetailsService).createDetails(eq(toCreate), any(UserEntity.class));
	}
	
	@Test
	public void savesDetailsOnUpdate() {
		when(personDetailsService.validateDetails(any(UserTO.class), anyList())).thenReturn(true);
		
		String userUuid = UUID.randomUUID().toString();
		UserEntity userEntity = new UserEntity(userUuid, AuthenticationType.FACEBOOK.name(), UserType.PERSON.name(), "valid-login-name", false, true);
		when(usersRepository.findByUuid(userUuid)).thenReturn(userEntity);
		when(usersRepository.saveAndFlush(any(UserEntity.class))).thenAnswer(new Answer<UserEntity>() {
			@Override
			public UserEntity answer(InvocationOnMock invocation) throws Throwable {
				return invocation.getArgument(0);
			}
		});
		
		UserTO toUpdate = this.makeFromEntity(userEntity);
		
		toUpdate.getUser().setPersonDetails(new PersonDetailsTO());

		usersService.updateUser(userUuid, toUpdate);
		
		verify(personDetailsService).updateDetails(eq(toUpdate), any(UserEntity.class));
	}
	
	@Test
	public void savesUserRoleChangesOnUpdate() {
		boolean enabled = false;
		String uuid = UUID.randomUUID().toString();
		
		String roleUuid1 = "role-1-uuid";
		String roleUuid2 = "role-2-uuid";
		String roleUuid3 = "role-3-uuid";
		String roleUuid4 = "role-4-uuid";
		
		RoleEntity role1 = makeUserRole(roleUuid1);
		RoleEntity role2 = makeUserRole(roleUuid2);
		RoleEntity role3 = makeUserRole(roleUuid3);
		RoleEntity role4 = makeUserRole(roleUuid4);
		
		Set<UserRoleEntity> userRoles = new HashSet<>();
		userRoles.add(mockUserRole(role1));
		userRoles.add(mockUserRole(role2));
		
		UserEntity userEntity = new UserEntity(uuid, AuthenticationType.GOOGLE.name(), 
				UserType.PERSON.name(), null, "test-user", enabled, true, new HashSet<>(), new HashSet<>(), new HashSet<>(), userRoles);
		userEntity.setId(14L);
	
		UserTO updated = makeFromEntity(userEntity);
		updated.getUser().setPersonDetails(new PersonDetailsTO());
		updated.getUser().getPersonDetails().setName("test-person-name-modified");
		updated.getUser().getPersonDetails().setEmail("a@b.c");
		
		updated.getLinks().getRoles().clear();
		updated.getLinks().setRoles(stringList("http://example.com/role/" + roleUuid3, "http://example.com/role/" + roleUuid4));
		
		when(usersRepository.findByUuid(uuid)).thenReturn(userEntity);
		when(usersRepository.save(matchesOnUuid(uuid))).thenReturn(userEntity);
		when(usersRepository.saveAndFlush(matchesOnUuid(uuid))).thenReturn(userEntity);

		usersService.updateUser(uuid, updated);
		
		for (UserRoleEntity oldUserRole : userRoles) {
			verify(userRolesRepository).delete(oldUserRole);
		}
		
		ArgumentCaptor<UserRoleEntity> userRolesCaptor = ArgumentCaptor.forClass(UserRoleEntity.class);  
		verify(userRolesRepository, times(2)).save(userRolesCaptor.capture());
		
		assertThat(userRolesCaptor.getAllValues().stream().map(UserRoleEntity::getRole).collect(Collectors.toList()), containsInAnyOrder(role3, role4));
	}

	@Test(expected = BadRequestException.class)
	public void barfsOnUpdateIfUserDoesNotExist() {
		String uuid = UUID.randomUUID().toString();
		when(usersRepository.findByUuid(uuid)).thenReturn(null);
		
		UserTO toUpdate = new UserTO();
		toUpdate.setUser(new UserValueTO());
		toUpdate.getUser().setUuid(uuid);
		
		usersService.updateUser(uuid, toUpdate);
	}
	
	@Test
	public void findsUserByUri() {
		String uuid = UUID.randomUUID().toString();
		UserEntity userEntity = new UserEntity();
		userEntity.setUuid(uuid);
		
		when(usersRepository.findByUuid(uuid)).thenReturn(userEntity);
		
		String userUrl = "http://example.com/user/" + uuid;
		when(usersProvider.getUuidFromUri(userUrl)).thenReturn(uuid);
		UserTO userTO = new UserTO();
		when(usersProvider.buildResource(userEntity)).thenReturn(userTO);
		
		assertThat(usersService.getUserByUri(userUrl), equalTo(userTO));
	}

	@Test
	public void reportsAllInvalidFieldsOnCreate() {
		String uuid = UUID.randomUUID().toString();
		UserTO userTO = new UserTO();
		userTO.setUser(new UserValueTO());
		userTO.getUser().setUuid(uuid);
		
		try {
			usersService.createUser(userTO);
			fail("Update user with invalid field values should have failed.");
		} catch (BadRequestException ex) {
			String message = ex.getMessage();
			assertThat(message, containsString(UsersService.INVALID_EMPTY_AUTH_TYPE));
			assertThat(message, containsString(UsersService.INVALID_EMPTY_USER_TYPE));
			assertThat(message, containsString(UsersService.INVALID_EMPTY_LOGIN_NAME));
		}
	}

	@Test
	public void reportsAllInvalidFieldsOnUpdate() {
		String uuid = UUID.randomUUID().toString();
		UserTO userTO = new UserTO();
		userTO.setUser(new UserValueTO());
		userTO.getUser().setUuid(uuid);
		
		try {
			usersService.updateUser(uuid, userTO);
			fail("Update user with invalid field values should have failed.");
		} catch (BadRequestException ex) {
			String message = ex.getMessage();
			assertThat(message, containsString(UsersService.INVALID_EMPTY_AUTH_TYPE));
			assertThat(message, containsString(UsersService.INVALID_EMPTY_USER_TYPE));
			assertThat(message, containsString(UsersService.INVALID_EMPTY_LOGIN_NAME));
		}
	}
	
	@Test
	public void reportsDetailsValidationMessagesOnUpdate() {
		boolean enabled = false;
		String roleUuid1 = "role-1-uuid";
		String roleUuid2 = "role-2-uuid";
		String userUuid = UUID.randomUUID().toString();
		UserTO toCreate = makeUserTO(userUuid, enabled, "test-user", 
				UserType.PERSON.name(), AuthenticationType.GOOGLE.name(), 
				stringList("http://example.com/roles/" + roleUuid1, "http://example.com/roles/" + roleUuid2));
		
		UserEntity userEntity = new UserEntity(userUuid, AuthenticationType.FACEBOOK.name(), UserType.PERSON.name(), null, enabled, true);
		when(usersRepository.findByUuid(userUuid)).thenReturn(userEntity);
		
		toCreate.getUser().setPersonDetails(new PersonDetailsTO());
		toCreate.getUser().getPersonDetails().setName("test--person-details");
		toCreate.getUser().getPersonDetails().setEmail("a@b.c");

		final String TEST_VALIDATION_ERROR_MESSAGE = "Test-validation-message";
		when(personDetailsService.validateDetails(any(UserTO.class), anyList())).thenAnswer(new Answer<Boolean>() {
			@SuppressWarnings("unchecked")
			@Override
			public Boolean answer(InvocationOnMock invocation) throws Throwable {
				((List<String>) invocation.getArgument(1)).add(TEST_VALIDATION_ERROR_MESSAGE);
				return false;
			}
		});
		
		try {
			usersService.updateUser(userUuid, toCreate);
			fail("Update user with invalid field values should have failed.");
		} catch (BadRequestException ex) {
			String message = ex.getMessage();
			assertThat(message, containsString(TEST_VALIDATION_ERROR_MESSAGE));
		}
	}
	
	@Test
	public void leavesAuthFieldsUnchangedOnUpdate() {
		boolean enabled = false;
		String uuid = UUID.randomUUID().toString();
		UserEntity beforeSave = new UserEntity(uuid, AuthenticationType.GOOGLE.name(), 
				UserType.PERSON.name(), null, "test-user", enabled, true, null, null, null, null);
		String initialPasswordHash = "unchanged-pwd-hash";
		beforeSave.setPasswordHash(initialPasswordHash);
		beforeSave.setId(14L);
		
		UserTO updated = makeFromEntity(beforeSave);
		updated.getUser().setPasswordHash("changed-pwd-hash");
		
		updated.getUser().setPersonDetails(new PersonDetailsTO());
		updated.getUser().getPersonDetails().setEmail("a@b.c");
		updated.getUser().getPersonDetails().setName("test-person-name");		
		
		UserEntity afterSave = makeFromTO(updated);
		afterSave.setId(beforeSave.getId());
		when(usersRepository.findByUuid(uuid)).thenReturn(beforeSave);
		when(usersRepository.save(matchesOnUuid(uuid))).thenReturn(afterSave);
		when(usersRepository.saveAndFlush(matchesOnUuid(uuid))).thenReturn(afterSave);
		
		usersService.updateUser(uuid, updated);
		
		ArgumentCaptor<UserEntity> userEntityCaptor = ArgumentCaptor.forClass(UserEntity.class); 
		verify(usersRepository).saveAndFlush(userEntityCaptor.capture());
		assertThat(userEntityCaptor.getValue().getPasswordHash(), equalTo(initialPasswordHash));
	}

	@Test
	public void passesParametersToRepositoryForListRetrieval() throws UnsupportedEncodingException {
		
		List<String> authTypes = Arrays.asList(AuthenticationType.GOOGLE.name(), AuthenticationType.FACEBOOK.name());
		List<String> userTypes = Arrays.asList(UserType.PERSON.name(), UserType.GROUP.name());
		List<String> roleUris = Arrays.asList("http://example.com/role/role1", "http://example.com/role/role2");
		List<Long> roleIds = Arrays.asList(14L, 42L);
		
		ArrayList<RoleEntity> allRoles = new ArrayList<>();
		allRoles.add(new RoleEntity("role1"));
		allRoles.get(0).setId(14L);
		allRoles.add(new RoleEntity("role2"));
		allRoles.get(1).setId(42L);
		
		when(usersProvider.roleUrlsToIds(roleUris)).thenReturn(roleIds);
		
		when(rolesRepository.findAll()).thenReturn(allRoles);
		
		String startUuid = "user-uuid-001";
		long count = 10L;
		boolean enabled = false;
		boolean mustChangePassword = true;
		
		ReflectionTestUtils.setField(usersService, "maxListSize", 100);
		
		usersService.getUsersList(authTypes, userTypes, roleUris, startUuid, count, enabled, mustChangePassword);
		
		// always retrieves one record more, to build the link to the next chunk/page
		verify(usersRepository).findPaged(startUuid, (int) count + 1, roleIds, userTypes, authTypes, enabled, mustChangePassword);
	}
	
	@Test
	public void usesMaxListSizeValueWhenCountTooBig() throws UnsupportedEncodingException {
		List<String> authTypes = Arrays.asList(AuthenticationType.GOOGLE.name(), AuthenticationType.FACEBOOK.name());
		List<String> userTypes = Arrays.asList(UserType.PERSON.name(), UserType.GROUP.name());
		List<String> roleUris = Arrays.asList("http://example.com/role/role1", "http://example.com/role/role2");
		List<Long> roleIds = Arrays.asList(14L, 42L);
		
		ArrayList<RoleEntity> allRoles = new ArrayList<>();
		allRoles.add(new RoleEntity("role1"));
		allRoles.get(0).setId(14L);
		allRoles.add(new RoleEntity("role2"));
		allRoles.get(1).setId(42L);
		
		when(usersProvider.roleUrlsToIds(roleUris)).thenReturn(roleIds);
		
		when(rolesRepository.findAll()).thenReturn(allRoles);
		
		final int MAX_LIST_SIZE = 10;
		String startUuid = "user-uuid-001";
		long count = MAX_LIST_SIZE + 100L;
		boolean enabled = false;
		boolean mustChangePassword = true;
		
		ReflectionTestUtils.setField(usersService, "maxListSize", MAX_LIST_SIZE);
		
		usersService.getUsersList(authTypes, userTypes, roleUris, startUuid, count, enabled, mustChangePassword);
		
		// always retrieves one record more, to build the link to the next chunk/page
		verify(usersRepository).findPaged(startUuid, MAX_LIST_SIZE + 1, roleIds, userTypes, authTypes, enabled, mustChangePassword);
	}

	@Test
	public void usesMaxListSizeValueWhenCountIsNull() throws UnsupportedEncodingException {
		List<String> authTypes = Arrays.asList(AuthenticationType.GOOGLE.name(), AuthenticationType.FACEBOOK.name());
		List<String> userTypes = Arrays.asList(UserType.PERSON.name(), UserType.GROUP.name());
		List<String> roleUris = Arrays.asList("http://example.com/role/role1", "http://example.com/role/role2");
		List<Long> roleIds = Arrays.asList(14L, 42L);
		
		ArrayList<RoleEntity> allRoles = new ArrayList<>();
		allRoles.add(new RoleEntity("role1"));
		allRoles.get(0).setId(14L);
		allRoles.add(new RoleEntity("role2"));
		allRoles.get(1).setId(42L);
		
		when(usersProvider.roleUrlsToIds(roleUris)).thenReturn(roleIds);
		
		when(rolesRepository.findAll()).thenReturn(allRoles);
		
		String startUuid = "user-uuid-001";
		boolean enabled = false;
		boolean mustChangePassword = true;
		
		final int MAX_LIST_SIZE = 10;
		ReflectionTestUtils.setField(usersService, "maxListSize", MAX_LIST_SIZE);
		
		usersService.getUsersList(authTypes, userTypes, roleUris, startUuid, null, enabled, mustChangePassword);
		
		// always retrieves one record more, to build the link to the next chunk/page
		verify(usersRepository).findPaged(startUuid, MAX_LIST_SIZE + 1, roleIds, userTypes, authTypes, enabled, mustChangePassword);
	}

	private RoleEntity makeUserRole(String roleUuid) {
		RoleEntity role1 = new RoleEntity();
		role1.setUuid(roleUuid);
		when(rolesRepository.findByUuid(roleUuid)).thenReturn(role1);
		when(rolesProvider.getUuidFromUri("http://example.com/role/" + roleUuid)).thenReturn(roleUuid);
		return role1;
	}

	private UserRoleEntity mockUserRole(RoleEntity role) {
		UserRoleEntity userRole = mock(UserRoleEntity.class, RETURNS_DEEP_STUBS);
		when(userRole.getRole()).thenReturn(role);
		return userRole;
	}	
	
	public static final class OnUuidMatcher<T> implements ArgumentMatcher<T> {

		private String[] toMatch;
		
		public OnUuidMatcher(String... toMatch) {
			this.toMatch = toMatch;
		}

		@Override
		public boolean matches(T argument) {
			if (argument == null) {
				return false;
			}
			Method getUuid;
			try {
				getUuid = argument.getClass().getMethod("getUuid");
				String uuid = (String) getUuid.invoke(argument);
				for (String expectedUuid : toMatch) {
					if (expectedUuid.equals(uuid)) {
						return true;
					}
				}
				return false;
			} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				return false;
			}
		}
	}
	
	private static <T> T matchesOnUuid(String... uuid) {
		return argThat(new OnUuidMatcher<T>(uuid));
	}
	
	private UserTO makeFromEntity(UserEntity userEntity) {
		List<String> roleLinks = new ArrayList<>();
		if (userEntity.getRoles() != null && !userEntity.getRoles().isEmpty()) {
			for (UserRoleEntity userRoleEntity : userEntity.getRoles()) {
				roleLinks.add("http://example.com/roles/" + userRoleEntity.getRole().getUuid());
			}
		}
		UserTO toCreate = makeUserTO(userEntity.getUuid(), userEntity.isEnabled(), userEntity.getLoginName(), userEntity.getUserType(), userEntity.getAuthType(), roleLinks);
		toCreate.getUser().setMustChangePassword(userEntity.isMustChangePassword());
		return toCreate;
	}
	
	private UserEntity makeFromTO(UserTO toCreate) {
		UserEntity saved = new UserEntity(toCreate.getUser().getUuid(), toCreate.getUser().getAuthType(), toCreate.getUser().getUserType(), toCreate.getUser().getLoginName(), toCreate.getUser().getEnabled(), true);
		when(usersRepository.findByUuid(saved.getUuid())).thenReturn(null).thenReturn(saved);
		when(usersRepository.save(matchesOnUuid(saved.getUuid()))).thenReturn(saved);
		when(usersRepository.saveAndFlush(matchesOnUuid(saved.getUuid()))).thenReturn(saved);
		return saved;
	}

	private UserTO makeUserTO(String uuid, boolean enabled, String loginName, String userType, String authType,
			List<String> userRoles) {
		UserTO userTO = new UserTO();
		userTO.setUser(new UserValueTO());
		userTO.getUser().setLoginName(loginName);
		userTO.getUser().setUuid(uuid);
		userTO.getUser().setEnabled(enabled);
		userTO.getUser().setMustChangePassword(true);
		userTO.getUser().setUserType(userType);
		userTO.getUser().setAuthType(authType);
		userTO.setLinks(new UserTOLinks());
		if (userRoles != null) {
			userTO.getLinks().setRoles(userRoles);
		}
		return userTO;
	}

	private List<String> stringList(String... roles) {
		List<String> userRoles = new ArrayList<>();
		for (String role : roles) {
			userRoles.add(role);
		}
		return userRoles;
	}
}
