package com.aidpad.users;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.aidpad.api.model.GroupDetailsTO;
import com.aidpad.api.model.GroupDetailsTOMembers;
import com.aidpad.api.model.OrganizationDetailsTO;
import com.aidpad.api.model.PersonDetailsTO;
import com.aidpad.api.model.UserTO;
import com.aidpad.api.model.UserTOList;
import com.aidpad.api.model.UserTOListLinks;
import com.aidpad.errorhandling.exceptions.InvalidUriException;
import com.aidpad.persistence.entities.GroupMemberDetailsEntity;
import com.aidpad.persistence.entities.OrganizationDetailsEntity;
import com.aidpad.persistence.entities.PersonDetailsEntity;
import com.aidpad.persistence.entities.RoleEntity;
import com.aidpad.persistence.entities.UserEntity;
import com.aidpad.persistence.entities.UserRoleEntity;
import com.aidpad.persistence.repositories.RolesRepository;
import com.aidpad.persistence.repositories.UsersRepository;
import com.aidpad.roles.RolesProvider;

@RunWith(MockitoJUnitRunner.Silent.class)
public class UsersProviderTest {

	private static final String TEST_BASE_PATH = "/base/path";

	@Mock
	private RolesProvider rolesProvider;

	@Mock
	private UsersRepository usersRepository;
	
	@Mock
	private RolesRepository rolesRepository;

	private UsersProvider usersProvider;

	@Before
	public void initMocking() {
		usersProvider = new UsersProvider(TEST_BASE_PATH, rolesProvider, usersRepository, rolesRepository);
	}

	@Test
	public void buildsSingleResourceUrlCorrectly() {
		String userUuid = UUID.randomUUID().toString();
		assertThat(usersProvider.getTOUri(userUuid),
				equalTo(TEST_BASE_PATH + UsersProvider.SINGLE_RESOURCE_PATH + userUuid));
	}

	@Test
	public void buildsListUrlCorrectly() {
		assertThat(usersProvider.getTOListUri(null), equalTo(TEST_BASE_PATH + UsersProvider.LIST_PATH));
	}

	@Test
	public void buildsListUrlCorrectlyWithQueryParameters() {
		Map<String, String> queryParameters = new HashMap<>();
		queryParameters.put(UsersProvider.QUERY_PARAM_AUTH_TYPES, AuthenticationType.GOOGLE.name());
		queryParameters.put(UsersProvider.QUERY_PARAM_USER_TYPES, UserType.ORGANISATION.name());

		String listUrl = usersProvider.getTOListUri(queryParameters);

		assertThat(listUrl, startsWith(TEST_BASE_PATH + UsersProvider.LIST_PATH));
		assertThat(listUrl,
				containsString(UsersProvider.QUERY_PARAM_AUTH_TYPES + "=" + AuthenticationType.GOOGLE.name()));
		assertThat(listUrl, containsString(UsersProvider.QUERY_PARAM_USER_TYPES + "=" + UserType.ORGANISATION.name()));
		assertThat(listUrl, containsString("?"));
		assertThat(listUrl, containsString("&"));
	}

	@Test
	public void buildsSingleUserToWithAllFields() {
		String userUuid = UUID.randomUUID().toString();
		UserEntity userEntity = new UserEntity(userUuid, AuthenticationType.GOOGLE.name(), UserType.GROUP.name(),
				"pwd-hash", "login-name", false, true, null, null, null, null);

		List<RoleEntity> roles = Arrays.asList(new RoleEntity(UUID.randomUUID().toString(), "test-role-1", null),
				new RoleEntity(UUID.randomUUID().toString(), "test-role-2", null));
		Set<UserRoleEntity> rolesSet = new HashSet<>();
		roles.forEach(roleEntity -> {
			rolesSet.add(new UserRoleEntity(roleEntity, userEntity));
			when(rolesProvider.getTOUri(roleEntity.getUuid()))
					.thenReturn(TEST_BASE_PATH + RolesProvider.SINGLE_RESOURCE_PATH + roleEntity.getUuid());
		});
		userEntity.setRoles(rolesSet);

		UserTO built = usersProvider.buildResource(userEntity);

		assertThat(built.getUser().getUuid(), equalTo(userEntity.getUuid()));
		assertThat(built.getUser().getAuthType(), equalTo(userEntity.getAuthType()));
		assertThat(built.getUser().getEnabled(), equalTo(userEntity.isEnabled()));
		assertThat(built.getUser().getLoginName(), equalTo(userEntity.getLoginName()));
		assertThat(built.getUser().getMustChangePassword(), equalTo(userEntity.isMustChangePassword()));
		assertThat(built.getUser().getName(), equalTo(userEntity.getLoginName()));
		assertThat(built.getUser().getPasswordHash(), equalTo(""));
		assertThat(built.getUser().getUserType(), equalTo(userEntity.getUserType()));

		assertThat(built.getLinks().getRoles(), hasSize(2));
		assertThat(built.getLinks().getRoles(),
				containsInAnyOrder(endsWith(roles.get(0).getUuid()), endsWith(roles.get(1).getUuid())));

		assertThat(built.getLinks().getSelf(),
				equalTo(TEST_BASE_PATH + UsersProvider.SINGLE_RESOURCE_PATH + userEntity.getUuid()));
	}

	@Test
	public void addsAllOrganizationDetails() {

		String userUuid = UUID.randomUUID().toString();
		UserEntity userEntity = new UserEntity(userUuid, AuthenticationType.GOOGLE.name(), UserType.ORGANISATION.name(),
				"pwd-hash", "login-name", false, true, null, null, null, null);

		OrganizationDetailsEntity organizationDetailsEntity = new OrganizationDetailsEntity(userEntity, "test-org", 
				"fiscal-code", "contact@org.domain", "http://web.site", "http://facebook.com/org.name");
		userEntity.setOrganizationDetails(new HashSet<OrganizationDetailsEntity>());
		userEntity.getOrganizationDetails().add(organizationDetailsEntity);
		
		// add something that shouldn't be there to make sure it's ignored by the conversion
		List<GroupMemberDetailsEntity> groupMemberDetailsEntities = Arrays.asList(
				new GroupMemberDetailsEntity(userEntity, "group-member-1"),
				new GroupMemberDetailsEntity(userEntity, "group-member-2"));
		userEntity.setGroupMembers(new HashSet<GroupMemberDetailsEntity>());
		userEntity.getGroupMembers().addAll(groupMemberDetailsEntities);
		
		PersonDetailsEntity personDetailsEntity = new PersonDetailsEntity(userEntity, "test-person-name");
		userEntity.setPersonDetails(new HashSet<PersonDetailsEntity>());
		userEntity.getPersonDetails().add(personDetailsEntity);
		
		UserTO built = usersProvider.buildResource(userEntity);
		
		assertThat(built.getUser().getUserType(), equalTo(UserType.ORGANISATION.name()));

		assertThat(built.getUser().getPersonDetails(), nullValue());
		assertThat(built.getUser().getGroupDetails(), nullValue());
		
		OrganizationDetailsTO organizationDetailsTO = built.getUser().getOrganizationDetails();
		assertThat(organizationDetailsTO, notNullValue());
		assertThat(organizationDetailsTO.getContactEmail(), equalTo(organizationDetailsEntity.getContactEmail()));
		assertThat(organizationDetailsTO.getFacebookPage(), equalTo(organizationDetailsEntity.getFacebookPage()));
		assertThat(organizationDetailsTO.getFiscalCode(), equalTo(organizationDetailsEntity.getFiscalIdentificationCode()));
		assertThat(organizationDetailsTO.getName(), equalTo(organizationDetailsEntity.getName()));
		assertThat(organizationDetailsTO.getWebSite(), equalTo(organizationDetailsEntity.getWebSite()));
	}
	
	@Test
	public void addsAllPersonDetails() {
		String userUuid = UUID.randomUUID().toString();
		UserEntity userEntity = new UserEntity(userUuid, AuthenticationType.GOOGLE.name(), UserType.PERSON.name(),
				"pwd-hash", "login-name", false, true, null, null, null, null);

		OrganizationDetailsEntity organizationDetailsEntity = new OrganizationDetailsEntity(userEntity, "test-org", 
				"fiscal-code", "contact@org.domain", "http://web.site", "http://facebook.com/org.name");
		userEntity.setOrganizationDetails(new HashSet<OrganizationDetailsEntity>());
		userEntity.getOrganizationDetails().add(organizationDetailsEntity);
		
		// add something that shouldn't be there to make sure it's ignored by the conversion
		List<GroupMemberDetailsEntity> groupMemberDetailsEntities = Arrays.asList(
				new GroupMemberDetailsEntity(userEntity, "group-member-1"),
				new GroupMemberDetailsEntity(userEntity, "group-member-2"));
		userEntity.setGroupMembers(new HashSet<GroupMemberDetailsEntity>());
		userEntity.getGroupMembers().addAll(groupMemberDetailsEntities);
		
		PersonDetailsEntity personDetailsEntity = new PersonDetailsEntity(userEntity, "test-person-name", "user-email");
		userEntity.setPersonDetails(new HashSet<PersonDetailsEntity>());
		userEntity.getPersonDetails().add(personDetailsEntity);
		
		UserTO built = usersProvider.buildResource(userEntity);
		
		assertThat(built.getUser().getUserType(), equalTo(UserType.PERSON.name()));

		assertThat(built.getUser().getOrganizationDetails(), nullValue());
		assertThat(built.getUser().getGroupDetails(), nullValue());
		
		PersonDetailsTO personDetailsTO = built.getUser().getPersonDetails();
		assertThat(personDetailsTO, notNullValue());
		
		assertThat(personDetailsTO.getEmail(), equalTo(personDetailsEntity.getEmail()));
		assertThat(personDetailsTO.getName(), equalTo(personDetailsEntity.getPersonName()));
	}
	
	@Test
	public void addsAllGroupDetails() {
		String userUuid = UUID.randomUUID().toString();
		UserEntity userEntity = new UserEntity(userUuid, AuthenticationType.GOOGLE.name(), UserType.GROUP.name(),
				"pwd-hash", "login-name", false, true, null, null, null, null);

		OrganizationDetailsEntity organizationDetailsEntity = new OrganizationDetailsEntity(userEntity, "test-org", 
				"fiscal-code", "contact@org.domain", "http://web.site", "http://facebook.com/org.name");
		userEntity.setOrganizationDetails(new HashSet<OrganizationDetailsEntity>());
		userEntity.getOrganizationDetails().add(organizationDetailsEntity);
		
		// add something that shouldn't be there to make sure it's ignored by the conversion
		List<GroupMemberDetailsEntity> groupMemberDetailsEntities = Arrays.asList(
				new GroupMemberDetailsEntity(userEntity, "group-member-1"),
				new GroupMemberDetailsEntity(userEntity, "group-member-2"));
		userEntity.setGroupMembers(new HashSet<GroupMemberDetailsEntity>());
		userEntity.getGroupMembers().addAll(groupMemberDetailsEntities);
		
		PersonDetailsEntity personDetailsEntity = new PersonDetailsEntity(userEntity, "test-person-name", "user-email");
		userEntity.setPersonDetails(new HashSet<PersonDetailsEntity>());
		userEntity.getPersonDetails().add(personDetailsEntity);
		
		UserTO built = usersProvider.buildResource(userEntity);
		
		assertThat(built.getUser().getUserType(), equalTo(UserType.GROUP.name()));

		assertThat(built.getUser().getOrganizationDetails(), nullValue());
		assertThat(built.getUser().getPersonDetails(), nullValue());
		
		GroupDetailsTO groupDetailsTO = built.getUser().getGroupDetails();
		assertThat(groupDetailsTO, notNullValue());
		
		assertThat(groupDetailsTO.getMembers(), hasSize(groupMemberDetailsEntities.size()));
		
		assertThat(groupDetailsTO.getMembers()
				.stream()
				.map(GroupDetailsTOMembers::getName)
				.collect(Collectors.toList()), 
			containsInAnyOrder("group-member-1", "group-member-2"));
	}
	
	@Test
	public void returnsUsersListWithAllAttributes() {
		List<UserEntity> users = Arrays.asList(
				new UserEntity(UUID.randomUUID().toString(), AuthenticationType.GOOGLE.name(), UserType.GROUP.name(),
						"test-user-1", false, true),
				new UserEntity(UUID.randomUUID().toString(), AuthenticationType.FACEBOOK.name(),
						UserType.ORGANISATION.name(), "test-user-1", false, true));

		Map<String, String> queryParameters = new HashMap<>();
		queryParameters.put(UsersProvider.QUERY_PARAM_USER_TYPES,
				UserType.GROUP.name() + "," + UserType.ORGANISATION.name());

		UserTOList usersResource = usersProvider.buildResourceList(users, queryParameters);

		assertThat(usersResource.getUsers(), hasSize(2));

		assertThat(usersResource.getUsers().stream().map(user -> user.getUser().getUuid()).collect(Collectors.toList()),
				containsInAnyOrder(users.get(0).getUuid(), users.get(1).getUuid()));
		assertThat(
				usersResource.getUsers().stream().map(user -> user.getUser().getLoginName())
						.collect(Collectors.toList()),
				containsInAnyOrder(users.get(0).getLoginName(), users.get(1).getLoginName()));

		assertThat(usersResource.getLinks().getSelf(), startsWith(TEST_BASE_PATH + UsersProvider.LIST_PATH));
		assertThat(usersResource.getLinks().getSelf(), containsString(UsersProvider.QUERY_PARAM_USER_TYPES + "="
				+ UserType.GROUP.name() + "," + UserType.ORGANISATION.name()));
	}

	@Test
	public void returnsEmptyListIfUserListEmpty() {
		List<UserEntity> users = new ArrayList<>();

		UserTOList usersResource = usersProvider.buildResourceList(users, null);

		assertThat(usersResource.getUsers(), hasSize(0));

		assertThat(usersResource.getLinks().getSelf(), not(containsString("?")));
	}

	@Test
	public void extractsUuidFromUserUri() {
		String userUuid = UUID.randomUUID().toString();
		String userUri = TEST_BASE_PATH + UsersProvider.SINGLE_RESOURCE_PATH + userUuid + "?query=param";
		assertThat(usersProvider.getUuidFromUri(userUri), equalTo(userUuid));
	}

	@Test(expected = InvalidUriException.class)
	public void failsUriParsingOnInvalidPath() {
		String userUuid = UUID.randomUUID().toString();
		String userUri = TEST_BASE_PATH + UsersProvider.SINGLE_RESOURCE_PATH + "/garbage/" + userUuid;
		assertThat(usersProvider.getUuidFromUri(userUri), equalTo(userUuid));
	}

	@Test(expected = InvalidUriException.class)
	public void failsUriParsingOnInvalidUuid() {
		String userUuid = "invalid-uuid";
		String userUri = TEST_BASE_PATH + UsersProvider.SINGLE_RESOURCE_PATH + userUuid;
		assertThat(usersProvider.getUuidFromUri(userUri), equalTo(userUuid));
	}

	@Test
	public void addsPrevAndNextUrlsCorrectly() {
		List<String> testUuids = new ArrayList<>(
				Arrays.asList(UUID.randomUUID().toString(), UUID.randomUUID().toString(), UUID.randomUUID().toString(),
						UUID.randomUUID().toString(), UUID.randomUUID().toString()));
		Collections.sort(testUuids);

		List<UserEntity> testUsers = testUuids
					.stream()
					.map(uuid -> new UserEntity(uuid, AuthenticationType.GOOGLE.name(), UserType.PERSON.name(), "user " + uuid, false, true))
					.collect(Collectors.toList());

		for (int index = 0; index < testUuids.size(); index++) {
			when(usersRepository.findByUuid(testUuids.get(index))).thenReturn(testUsers.get(index));
		}

		List<UserEntity> forPage = Arrays.asList(testUsers.get(2), testUsers.get(3));
		
		when(usersRepository.findUidsReversed(
				any(), anyInt(), any(), any(), any(), any(), any())).thenReturn(Arrays.asList(
				testUuids.get(1), testUuids.get(0)));
		
		when(usersRepository.findNextUuid(any(), any(), any(), any(), any(), any())).thenReturn(testUuids.get(4));

		// page of size 2, page start at testUuids[2], prev must be testUuids[0], next
		// must be testUuids[4], page must contain testUuids[2] and testUuids[3]
		
		Map<String, String> queryParameters = new HashMap<>();
		queryParameters.put(UsersProvider.QUERY_PARAM_START_UUID, testUuids.get(2));
		queryParameters.put(UsersProvider.QUERY_PARAM_COUNT, "2");
		UserTOList usersResource = usersProvider.buildResourceList(forPage, queryParameters);
		
		UserTOListLinks links = usersResource.getLinks();
		
		// This is a bit of cheating - map does not provide ordering guarantees, so query params might get reversed
		assertThat(links.getNext(), equalTo(TEST_BASE_PATH + UsersProvider.LIST_PATH + "?start=" + testUuids.get(4) + "&count=2"));
		assertThat(links.getPrev(), equalTo(TEST_BASE_PATH + UsersProvider.LIST_PATH + "?start=" + testUuids.get(0) + "&count=2"));
		assertThat(links.getSelf(), equalTo(TEST_BASE_PATH + UsersProvider.LIST_PATH + "?start=" + testUuids.get(2) + "&count=2"));
	}
}
