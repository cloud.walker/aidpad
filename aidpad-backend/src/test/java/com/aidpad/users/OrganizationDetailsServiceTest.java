package com.aidpad.users;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.aidpad.api.model.OrganizationDetailsTO;
import com.aidpad.api.model.UserTO;
import com.aidpad.api.model.UserValueTO;
import com.aidpad.persistence.entities.OrganizationDetailsEntity;
import com.aidpad.persistence.entities.UserEntity;
import com.aidpad.persistence.repositories.OrganizationDetailsRepository;

@RunWith(MockitoJUnitRunner.Silent.class)
public class OrganizationDetailsServiceTest {

	private static final String TEST_ORGANIZATION_NAME = "test-organization-name";
	private static final String TEST_FISCAL_CODE = "123456789";
	private static final String TEST_ORGANIZATION_EMAIL = "a@b.c";
	private static final String TEST_ORGANIZATION_NAME_MODIFIED = "test-organization-name-modified";
	private static final String TEST_FISCAL_CODE_MODIFIED = "123456789-modified";
	private static final String TEST_ORGANIZATION_EMAIL_MODIFIED = "a-modified@b.c";

	@Mock
	private OrganizationDetailsRepository organizationDetailsRepository;
	
	@InjectMocks
	private OrganizationDetailsService organizationDetailsService;

	@Test
	public void validOrganizationDetailsTOIsValidated() {
		UserTO userTO = createOrganizationUser(TEST_ORGANIZATION_EMAIL, TEST_FISCAL_CODE, TEST_ORGANIZATION_NAME);
		
		List<String> validationErrors = new ArrayList<>();
		assertThat(organizationDetailsService.validateDetails(userTO, validationErrors), is(true));
		assertThat(validationErrors, hasSize(0));
	}

	@Test
	public void disallowOrganizationUserWithNoOrganization() {
		UserTO userTO = createOrganizationUser(TEST_ORGANIZATION_EMAIL, TEST_FISCAL_CODE, TEST_ORGANIZATION_NAME);
		userTO.getUser().setOrganizationDetails(null);
		
		List<String> validationErrors = new ArrayList<>();
		assertThat(organizationDetailsService.validateDetails(userTO, validationErrors), is(false));

		assertThat(validationErrors, hasSize(1));
		assertThat(validationErrors.get(0), containsString(OrganizationDetailsService.INVALID_EMPTY_ORGANIZATION_DETAILS));
	}
	
	@Test
	public void disallowNonOrganizationUserWithOrganization() {
		UserTO userTO = createOrganizationUser(TEST_ORGANIZATION_EMAIL, TEST_FISCAL_CODE, TEST_ORGANIZATION_NAME);
		userTO.getUser().setUserType(UserType.PERSON.name());
		
		List<String> validationErrors = new ArrayList<>();
		assertThat(organizationDetailsService.validateDetails(userTO, validationErrors), is(false));

		assertThat(validationErrors, hasSize(1));
		assertThat(validationErrors.get(0), containsString(OrganizationDetailsService.INVALID_FILLED_ORGANIZATION_DETAILS));
	}

	@Test
	public void disallowOrganizationUserWithNoOrganizationName() {
		UserTO userTO = createOrganizationUser(TEST_ORGANIZATION_EMAIL, TEST_FISCAL_CODE, null);
		
		List<String> validationErrors = new ArrayList<>();
		assertThat(organizationDetailsService.validateDetails(userTO, validationErrors), is(false));

		assertThat(validationErrors, hasSize(1));
		assertThat(validationErrors.get(0), containsString(OrganizationDetailsService.INVALID_ORGANIZATION_HAS_NO_NAME));
	}

	@Test
	public void disallowOrganizationUserWithNoFiscalCode() {
		UserTO userTO = createOrganizationUser(TEST_ORGANIZATION_EMAIL, null, TEST_ORGANIZATION_NAME);
		
		List<String> validationErrors = new ArrayList<>();
		assertThat(organizationDetailsService.validateDetails(userTO, validationErrors), is(false));

		assertThat(validationErrors, hasSize(1));
		assertThat(validationErrors.get(0), containsString(OrganizationDetailsService.INVALID_ORGANIZATION_HAS_NO_FISCAL_IDENTITY));
	}
	
	@Test
	public void disallowOrganizationUserWithNoEmail() {
		UserTO userTO = createOrganizationUser(null, TEST_FISCAL_CODE, TEST_ORGANIZATION_NAME);
		
		List<String> validationErrors = new ArrayList<>();
		assertThat(organizationDetailsService.validateDetails(userTO, validationErrors), is(false));

		assertThat(validationErrors, hasSize(1));
		assertThat(validationErrors.get(0), containsString(OrganizationDetailsService.INVALID_ORGANIZATION_HAS_NO_CONTACT_EMAIL));
	}
	
	@Test
	public void validatesValidOrganizationDetailsEntity() {
		UserEntity userEntity = createOrganizationUserEntity(TEST_ORGANIZATION_NAME, TEST_ORGANIZATION_EMAIL,
				TEST_FISCAL_CODE);
		
		List<String> validationErrors = new ArrayList<>();
		assertThat(organizationDetailsService.validateDetails(userEntity, validationErrors), is(true));
		assertThat(validationErrors, hasSize(0));
	}

	@Test
	public void disallowsOrganizationUserEntityWithOrganizationDetails() {
		UserEntity userEntity = createOrganizationUserEntity(null, null, null);
		userEntity.getOrganizationDetails().clear();
		
		List<String> validationErrors = new ArrayList<>();
		assertThat(organizationDetailsService.validateDetails(userEntity, validationErrors), is(false));
		
		assertThat(validationErrors, hasSize(1));
		assertThat(validationErrors.get(0), containsString(OrganizationDetailsService.INVALID_EMPTY_ORGANIZATION_DETAILS));
	}

	@Test
	public void disallowsNonOrganizationUserEntityWithNoOrganizationDetails() {
		UserEntity userEntity = createOrganizationUserEntity(null, null, null);
		userEntity.setUserType(UserType.PERSON.name());
		
		List<String> validationErrors = new ArrayList<>();
		assertThat(organizationDetailsService.validateDetails(userEntity, validationErrors), is(false));
		
		assertThat(validationErrors, hasSize(1));
		assertThat(validationErrors.get(0), containsString(OrganizationDetailsService.INVALID_FILLED_ORGANIZATION_DETAILS));
	}

	@Test
	public void disallowsOrganizationUserEntityWithNoOrganizationName() {
		UserEntity userEntity = createOrganizationUserEntity(null, TEST_ORGANIZATION_EMAIL, TEST_FISCAL_CODE);
		
		List<String> validationErrors = new ArrayList<>();
		assertThat(organizationDetailsService.validateDetails(userEntity, validationErrors), is(false));
		
		assertThat(validationErrors, hasSize(1));
		assertThat(validationErrors.get(0), containsString(OrganizationDetailsService.INVALID_ORGANIZATION_HAS_NO_NAME));
	}
	
	@Test
	public void disallowsOrganizationUserEntityWithNoEmail() {
		UserEntity userEntity = createOrganizationUserEntity(TEST_ORGANIZATION_NAME, null, TEST_FISCAL_CODE);
		
		List<String> validationErrors = new ArrayList<>();
		assertThat(organizationDetailsService.validateDetails(userEntity, validationErrors), is(false));
		
		assertThat(validationErrors, hasSize(1));
		assertThat(validationErrors.get(0), containsString(OrganizationDetailsService.INVALID_ORGANIZATION_HAS_NO_CONTACT_EMAIL));
	}
	
	@Test
	public void disallowsOrganizationUserEntityWithNoFiscalCode() {
		UserEntity userEntity = createOrganizationUserEntity(TEST_ORGANIZATION_NAME, TEST_ORGANIZATION_EMAIL, null);
		
		List<String> validationErrors = new ArrayList<>();
		assertThat(organizationDetailsService.validateDetails(userEntity, validationErrors), is(false));
		
		assertThat(validationErrors, hasSize(1));
		assertThat(validationErrors.get(0), containsString(OrganizationDetailsService.INVALID_ORGANIZATION_HAS_NO_FISCAL_IDENTITY));
	}
	
	@Test
	public void savesOrganizationDetailsOnCreate() {
		UserTO userTO = createOrganizationUser(TEST_ORGANIZATION_EMAIL, TEST_FISCAL_CODE, TEST_ORGANIZATION_NAME);
		UserEntity userEntity = createOrganizationUserEntity(TEST_ORGANIZATION_NAME, TEST_ORGANIZATION_EMAIL, TEST_FISCAL_CODE);
		userEntity.getOrganizationDetails().clear();
		
		when(organizationDetailsRepository.save(any(OrganizationDetailsEntity.class))).then(new Answer<OrganizationDetailsEntity>() {
			@Override
			public OrganizationDetailsEntity answer(InvocationOnMock invocation) throws Throwable {
				return invocation.getArgument(0);
			}
		});
		
		organizationDetailsService.createDetails(userTO, userEntity);
		
		ArgumentCaptor<OrganizationDetailsEntity> saveArgumentCaptor = ArgumentCaptor.forClass(OrganizationDetailsEntity.class);
		verify(organizationDetailsRepository).save(saveArgumentCaptor.capture());
		
		OrganizationDetailsEntity savedDetails = saveArgumentCaptor.getValue();
		assertThat(savedDetails.getName(), equalTo(userTO.getUser().getOrganizationDetails().getName()));
		assertThat(savedDetails.getContactEmail(), equalTo(userTO.getUser().getOrganizationDetails().getContactEmail()));
		assertThat(userEntity.getOrganizationDetails().iterator().next(), is(savedDetails));		
	}
	
	@Test
	public void deletesOrganizationDetailsOnDelete() {
		UserEntity userEntity = createOrganizationUserEntity(TEST_ORGANIZATION_NAME, TEST_ORGANIZATION_EMAIL, TEST_FISCAL_CODE);
		
		Set<OrganizationDetailsEntity> organizationDetailsSet = userEntity.getOrganizationDetails();
		
		organizationDetailsService.deleteDetails(userEntity);
		
		verify(organizationDetailsRepository).deleteAll(organizationDetailsSet);
	}
	
	@Test
	public void deletesThenCreatesOrganizationDetailsOnUpdate() {
		UserTO userTO = createOrganizationUser(TEST_ORGANIZATION_EMAIL_MODIFIED, TEST_FISCAL_CODE_MODIFIED, TEST_ORGANIZATION_NAME_MODIFIED);
		UserEntity userEntity = createOrganizationUserEntity(TEST_ORGANIZATION_NAME, TEST_ORGANIZATION_EMAIL, TEST_FISCAL_CODE);

		Set<OrganizationDetailsEntity> organizationDetailsSet = userEntity.getOrganizationDetails();
		OrganizationDetailsEntity oldOrganizationDetails = organizationDetailsSet.iterator().next();

		when(organizationDetailsRepository.save(any(OrganizationDetailsEntity.class))).then(new Answer<OrganizationDetailsEntity>() {
			@Override
			public OrganizationDetailsEntity answer(InvocationOnMock invocation) throws Throwable {
				return invocation.getArgument(0);
			}
		});

		organizationDetailsService.updateDetails(userTO, userEntity);
		
		verify(organizationDetailsRepository).deleteAll(organizationDetailsSet);
		
		ArgumentCaptor<OrganizationDetailsEntity> saveArgumentCaptor = ArgumentCaptor.forClass(OrganizationDetailsEntity.class);
		verify(organizationDetailsRepository).save(saveArgumentCaptor.capture());
		
		OrganizationDetailsEntity savedDetails = saveArgumentCaptor.getValue();
		assertThat(savedDetails.getName(), equalTo(userTO.getUser().getOrganizationDetails().getName()));
		assertThat(savedDetails.getContactEmail(), equalTo(userTO.getUser().getOrganizationDetails().getContactEmail()));
		assertThat(userEntity.getOrganizationDetails().iterator().next(), not(is(oldOrganizationDetails)));		
	}
	
	private UserTO createOrganizationUser(String contactEmail, String fiscalCode, String name) {
		UserTO userTO = new UserTO();
		userTO.setUser(new UserValueTO());
		userTO.getUser().setUserType(UserType.ORGANISATION.name());
		OrganizationDetailsTO organizationDetails = createOrganizationDetailsTO(contactEmail, fiscalCode, name);
		userTO.getUser().setOrganizationDetails(organizationDetails);
		return userTO;
	}

	private OrganizationDetailsTO createOrganizationDetailsTO(String contactEmail, String fiscalCode, String name) {
		OrganizationDetailsTO organizationDetails = new OrganizationDetailsTO();
		organizationDetails.setContactEmail(contactEmail);
		organizationDetails.setFiscalCode(fiscalCode);
		organizationDetails.setName(name);
		return organizationDetails;
	}
	
	private UserEntity createOrganizationUserEntity(String testOrganizationName, String testOrganizationEmail,
			String testFiscalCode) {
		UserEntity userEntity = new UserEntity();
		userEntity.setUserType(UserType.ORGANISATION.name());
		userEntity.setOrganizationDetails(new HashSet<>());
		OrganizationDetailsEntity organizationDetailsEntity = createOrganizationDetailsEntity(testOrganizationName,
				testOrganizationEmail, testFiscalCode);
		userEntity.getOrganizationDetails().add(organizationDetailsEntity);
		return userEntity;
	}

	private OrganizationDetailsEntity createOrganizationDetailsEntity(String testOrganizationName,
			String testOrganizationEmail, String testFiscalCode) {
		OrganizationDetailsEntity organizationDetailsEntity = new OrganizationDetailsEntity();
		organizationDetailsEntity.setContactEmail(testOrganizationEmail);
		organizationDetailsEntity.setFiscalIdentificationCode(testFiscalCode);
		organizationDetailsEntity.setName(testOrganizationName);
		return organizationDetailsEntity;
	}
}
