# AidPad

## What it's all about

A bunch of programmers gathered to build a platform for a local NGO/charity which gathers donations and sponsors various local activities to make life better - this [one](https://www.fundatiacomunitaratimisoara.ro/ro/).

What their stated mission is (translated from Romanian): contribute substantially to the mobilization of local financial resource for the benefit of the community.

What they lacked was a computer based platform where to manage all the funds they collect and all the projects they decide to sponsor. We initially set out to build one for them. Then we thought we'd do things so other organizations with similar needs can pick this up and use it too.

What the supported process is envisioned to be - that's how the organization works:
- someone called a financier creates and publishes a request for projects
- would-be beneficiaries - that's the ones getting money for doing the projects, not the beneficiaries of the projects - apply for finanncing
- each application is evaluated by a set of evaluators, and whatever is deemed useful and can be financed, is approved
- a contract is signed, lots of documents are uploaded, payments are made - outside of the platform, the platform is used only for uploading documents in electronic format in this stage
- finished projects are archived

**Note!** The application is in an early stage of implementation, and we're quite busy otherwise, so don't look for much functionality so far.

## How it's set up

There are two parts with a strong separation: frontend and backend.

Each one uses different technology, and can be deployed separately. Where they are coupled is the API. The project takes an API-first approach, with the API being defined in the sub-directory aidpad-api. Both the fronted and the backend use the openapi definitions in there to generate client and server code at build time.

The API provided by the backend/consumed by the frontend is meant to be fully restful. (We still have some work to do there.) This should make it easily consumable by additional clients.

This structure makes it easy for additional clients, besides the frontend, to integrate with the backend.

Besides those two components, meant for production, there are a lot of test modules, each one with its own sub-directory. We're a small team, and as such have to rely heavily on testing, if we want to ensure a stable and solid platform. A small, non-IT-heavy organization has no use from a platform which continuously needs intervention from administrators.

The entire build and deployment is managed via a gitlab CI pipeline.

## Technicalities - more details

The frontend is a simple angular client. The backend is a Java 11 application done with Spring boot.

In designing the solution, in line with our intention to make the application usable for whoever is interested, we rely on no external API. All that we require is a web server to serve the static resources which compose the fronted, a mysql or mariadb database (or something compatible), and a place where to run Java 11.

One maybe quirky aspect is the database versioning mechanism. We want to have everything version controlled - including the database. There are many well known tools out there, for this purpose, most of them with two major drawbacks:
- they don't allow rollback to a previous version
- they only version schema, not operations on data

Using a tool with those two drawbacks would cause a lot of costs with migrations for our would-be customer - migrating data, when schema changes, or rolling back to a previous version, in case a new version is found to have problems, is tedious and sometimes complex manual work, for the database administrator.

Therefore, we use [mybatis migrations](http://mybatis.org/migrations/) for database versioning. This mechanism maintains the database version via SQL scripts. Each version change has to be defined in a migration script - a text file containing two SQL snippets, one for migrating the database - including existing data - to a new version, and one for reverting the changes. This allows for fully automated database management, in the face of version changes.

To ensure the database and the backend code work well together, entities are not written by hand, neither are they defined in mapping files maintained by hand, but are generated at build time from the database schema itself. The database schema being maintained by mybatis migrations, we have both a single source of truth for the backend's data model and the database itself.

## Responsibilities of components

The frontend is where it's at. When defining a request for projects, a form structured according to the project needs is defined dynamically. This definition is then presented to beneficiaries. Based on a user's access rights, the frontend also has to show different parts of the application to different users. This highly dynamic presentation layer requirements are where the challenge lays, with the frontend.

The backend serves merely as a data store for the frontend. However, it does implement some non-trivial functionality of its own. Access to various operations is dependent on the state of a request for project or a project, the role of the user requesting the operations, the user's relation to the request for projects or or project the operation is to be applied to and so on. The authorization logic, therefore, is quite complex. That part is what the backend enforces.

The backend's authorization implementation relies on [Spring Security](https://spring.io/projects/spring-security). The frontend-backend interaction uses a [JWT](https://jwt.io/). That JWT is usable by the server to decide whether a request is legitimate and authorized. In principle, the JWT would also be usable by the frontend to decide what parts of the application to make available to users. In practice, however, the frontend retrieves the user's full details from the backend to make these decisions.

To speed things up, some validations are performed both in the frontend and in the backend. Most time spent for a roundtrip to the server is spent on the network. Doing all checks and validations on the backend is unavoidable, since otherwise alternative clients (like [curl](https://curl.haxx.se/) or [postman](https://www.postman.com/downloads/), for example) could circumvent restrictions. Spending a few nanoseconds for redundant validtions on some requests in the frontend more than makes up for unnecessary roundtrips to the backend, of which a single one can take tens of milliseconds.

The application supports three kinds of authorization, for the time being: facebook, google and username and password.

## Functionality realms

Broadly speaking, there's two of them: user management and management of requests for projects and projects.

The management of requests and projects further splits into separate realms based on the different phases through which a request for projects and a project can go.

This split is reflected in the backend's implementation in the package structure.

## Finally

Each sub-component, where needed, has additional documentation in its own sub-directory.
